/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.security;

import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.util.I18NManager;
import com.sun.grid.grm.util.Printer;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.security.KeyStore;
import java.security.Principal;
import java.security.cert.CertificateException;
import java.security.cert.PKIXBuilderParameters;
import java.security.cert.X509CertSelector;
import java.security.cert.X509Certificate;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;
import javax.net.ssl.CertPathTrustManagerParameters;
import javax.net.ssl.ManagerFactoryParameters;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.ConfirmationCallback;
import javax.security.auth.callback.UnsupportedCallbackException;

/**
 * Instances of the class delegate validation of certificate chains to
 * a delegate <code>X509TrustManager</code>. If the delegate do not trust
 * the certificate, the user is asked wether he trusts the certificate.
 *
 * Instances of this class can be used for CLI clients.
 *
 * <H3>Example</H3>
 *
 * <pre>
 *   class SampleClient {
 *
 *      public static void main(String [] args) {
 *
 *       X509TrustManager tm = ...;
 *       TrustManager [] tms = new TrustManager [] {
 *           new AskingX509TrustManager(tm)
 *       };
 *       SSLHelper.init(null, tms, null);
 *
 *      }
 *   }
 * </pre>
 *
 * @todo    i18n
 */
public class AskingX509TrustManager implements X509TrustManager {

    private static final String BUNDLE = "com.sun.grid.grm.security.security";
    private final Logger logger =
            Logger.getLogger(AskingX509TrustManager.class.getName(), BUNDLE);

    private CallbackHandler callbackHandler;
    private final ExecutionEnv env;
    private final List<X509TrustManager> trustManagers = new LinkedList<X509TrustManager>();
    
    private KeyStore certificateKeyStore;
    
    /**
     * Create a new instanceof <code>AskingX509TrustManager</code>.
     *
     * @param env the execution enviroment
     * @param delegate the delegate <code>X509TrustManager</code>
     *                 which validates the certificate chains.
     * @param callbackHandler the callback handler
     *
     */
    public AskingX509TrustManager(ExecutionEnv env, X509TrustManager delegate, CallbackHandler callbackHandler) {
        this.env = env;
        this.callbackHandler = callbackHandler;
        trustManagers.add(delegate);
    }
    
    private void addCertToCache(X509Certificate cert) {
        
        
        try {
            if(certificateKeyStore == null) {
                certificateKeyStore  = KeyStore.getInstance(SecurityConstants.KEYSTORE_ALGORITHM);
                certificateKeyStore.load(null, new char[0]);
                

                certificateKeyStore.setCertificateEntry("0", cert);
                
                PKIXBuilderParameters pkixParams = 
                        new PKIXBuilderParameters(certificateKeyStore, new X509CertSelector());
                
                
                pkixParams.setRevocationEnabled(false);
                
                // Wrap them as trust manager parameters
                ManagerFactoryParameters trustParams =
                        new CertPathTrustManagerParameters(pkixParams);
                TrustManagerFactory fac = TrustManagerFactory.getInstance(SecurityConstants.TRUSTMANAGER_ALGORITHM);
                
                fac.init(trustParams);
                for (TrustManager tm: fac.getTrustManagers()) {
                    if (tm instanceof X509TrustManager) {
                        trustManagers.add((X509TrustManager)tm);
                    }
                }
            } else {
                certificateKeyStore.setCertificateEntry(Integer.toString(certificateKeyStore.size()), cert);
            }
            
        } catch(Exception e) {
            throw new IllegalStateException("Can not setup trusted cert cache", e);
        }
    }

    private boolean trustCeritificate(final X509Certificate[] chain) {
        
        StringWriter sw = new StringWriter();
        Printer pw = new Printer(new PrintWriter(sw));
        X509Certificate cert = chain[chain.length -1];
        
        try {
            pw.println(I18NManager.formatMessage("AskingX509TrustManager.certNotTrusted", BUNDLE));
            printCertificate(cert, pw);

            pw.print(I18NManager.formatMessage("AskingX509TrustManager.trustCert", BUNDLE));
            pw.print(' ');
        } finally {
            pw.close();
        }
        
        ConfirmationCallback [] callbacks = new ConfirmationCallback [] {
              new ConfirmationCallback(sw.getBuffer().toString(),
                                       ConfirmationCallback.INFORMATION,
                                       ConfirmationCallback.YES_NO_OPTION,
                                       ConfirmationCallback.YES)
        };
        try {
            callbackHandler.handle(callbacks);
        } catch (IOException ex) {
            return false;
        } catch (UnsupportedCallbackException ex) {
            return false;
        }
        
        if(callbacks[0].getSelectedIndex() ==  ConfirmationCallback.YES) {
            addCertToCache(cert);
            return true;
        } else {
            return false;
        }
    }

    private static void printDN(Principal principal, Printer pw, int maxCol) {
        pw.indent();
        String [] dn = principal.getName().split(",");
        int col = 0;
        boolean first = true;
        for(String d: dn) {
            d = d.trim();
            if (first) {
                first = false;
            } else {
                pw.print(", ");
                col += 2;
            }
            if(col + d.length() > maxCol) {
                pw.println();
                col = 0;
            }
            pw.print(d);
            col += d.length();
        }
        pw.println();
        pw.deindent();
        
    }
    /**
     * Print the certificate into a printer
     * @param cert the certificate
     * @param pw   the printer
     */
    public static void printCertificate(final X509Certificate cert,
                                  final Printer pw) {

        String issuedTo = I18NManager.formatMessage("AskingX509TrustManager.issuedTo", BUNDLE);
        String serialNo = I18NManager.formatMessage("AskingX509TrustManager.serialNo", BUNDLE);
        String issuedBy = I18NManager.formatMessage("AskingX509TrustManager.issuedBy", BUNDLE);
        String validity = I18NManager.formatMessage("AskingX509TrustManager.validity", BUNDLE);
        String fingerPrints = I18NManager.formatMessage("AskingX509TrustManager.fingerprints", BUNDLE);
        
        int len = issuedTo.length();
        len = Math.max(len, serialNo.length());
        len = Math.max(len, issuedBy.length());
        len = Math.max(len, validity.length());
        len = Math.max(len, fingerPrints.length());
        
        String headerFormat = "%" + len + "s ";
        int maxCol = 100;
        pw.setDepth(len+1);
        pw.printf(headerFormat, issuedTo);
        printDN(cert.getSubjectDN(), pw, maxCol);
        pw.printf(headerFormat, serialNo);
        pw.println(Long.toHexString(cert.getSerialNumber().longValue()));
        
        pw.printf(headerFormat, issuedBy);
        printDN(cert.getIssuerDN(), pw, maxCol);
        pw.printf(headerFormat, validity);
        pw.indent();
        String from = I18NManager.formatMessage("AskingX509TrustManager.from", BUNDLE);
        String to = I18NManager.formatMessage("AskingX509TrustManager.to", BUNDLE);
        String df = "%" + Math.max(from.length(), to.length()) + "s ";
        pw.printf(df, from);
        pw.println(cert.getNotBefore().toString());
        pw.printf(df, to);
        pw.println(cert.getNotAfter().toString());
        pw.deindent();
        pw.printf(headerFormat, fingerPrints);
        pw.println(cert.getSigAlgName());
        pw.indent();
        printHexString(cert.getSignature(), pw, maxCol);
        pw.println();
        pw.deindent();
    }


    /*
     * Inherited from super class.
     *
     * @param chain     the ceriticate chain
     * @param authType  authentification type
     * @throws java.security.cert.CertificateException if the certificate chain
     *                                      is not trusted by this TrustManager
     * @see javax.net.ssl.X509TrustManager#checkClientTrusted
     */
    public final void checkClientTrusted(final X509Certificate[] chain,
                                         final String authType)
    throws CertificateException {
        logger.fine("AskingX509TrustManager.verifyClient");
        boolean notTrusted = true;
        
        for(X509TrustManager tm: trustManagers) {
            try {
                tm.checkClientTrusted(chain, authType);
                notTrusted = false;
                break;
            } catch(CertificateException ex) {
                // Ignore
            }
        }
        if(notTrusted) {
            if(!trustCeritificate(chain)) {
                throw new CertificateException("Not trusted");
            }
        }
    }

    /*
     * Inherited from super class.
     *
     * @param chain     the certificate chain
     * @param authType  the authentication type
     * @throws java.security.cert.CertificateException if the certificate chain
     *                                      is not trusted by this TrustManager
     * @see javax.net.ssl.X509TrustManager#checkServerTrusted
     */
    public final void checkServerTrusted(final X509Certificate[] chain,
                                         final String authType)
    throws CertificateException {
        logger.fine("AskingX509TrustManager.verifyServer");
        boolean notTrusted = true;        
        for(X509TrustManager tm: trustManagers) {
            try {
                tm.checkServerTrusted(chain, authType);
                notTrusted = false;
                break;
            } catch(CertificateException ex) {
                // Ignore
            }
        }
        if(notTrusted) {
            if(!trustCeritificate(chain)) {
                throw new CertificateException("Not trusted");
            }
        }
    }


    /*
     * Inherited from super class.
     *
     * @return the accepted issuers
     * @see javax.net.ssl.X509TrustManager#getAcceptedIssuers
     */
    public final X509Certificate[] getAcceptedIssuers() {
        // we only accept Issuers from the delegate
        return trustManagers.get(0).getAcceptedIssuers();
    }

    private static void printHexString(final byte [] bytes, Printer pw, int maxCol) {
        StringBuilder buf = new StringBuilder();
        boolean first = true;

        int bytesPrinted = 0;
        for (byte b : bytes) {
            if(bytesPrinted >= maxCol) {
                pw.println();
                bytesPrinted = 0;
                first = true;
            }
            if (first) {
                first = false;
            } else {
                pw.print(':');
                bytesPrinted++;
            }

            String s = Integer.toHexString(b & 0x00FF);

            if (s.length() == 1) {
                pw.print('0');
            }
            pw.print(s);
            bytesPrinted += 2;
        }
    }
}

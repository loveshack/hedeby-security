/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.security;

import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.SystemUtil;
import com.sun.grid.grm.security.ca.GrmCATrustManager;
import java.io.File;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStore.PrivateKeyEntry;
import java.security.cert.X509Certificate;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Logger;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import javax.net.ssl.X509TrustManager;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.callback.UnsupportedCallbackException;

/**
 * Default implements for a client security context.
 *
 * <p>This security context tries to get the default keystore and ca certificate file
 * for this user and registers it in the <code>SSLHelper</code>. Once it is registered
 * the user can communicate without password.</p>
 *
 * <p>If the try was not succesfull the <code>callbackHandler</code> is used to get the
 * required information. The following <code>Callback</code>s are used:</p>
 *
 * <table border="1">
 *  <tr>
 *     <th>Callback</th><th>Parameters</th><th>Description</th>
 *  </tr>
 *  <tr>
 *     <td><code>{@link com.sun.grid.grm.security.FileCallback 
 *          com.sun.grid.grm.security.FileCallback}</code></td>
 *     <td><code>type=SecurityConstants.KEYSTORE_FILE_CALLBACK</code></td>
 *     <td>
 *        Queries the path to the keystore file.
 *     </td>
 *  </tr>
 *  <tr>
 *     <td><code>javax.security.auth.callback.NameCallback</code></td>
 *     <td>None</td>
 *     <td>
 *        Queries the username.
 *     </td>
 *  </tr>
 *  <tr>
 *     <td><code>javax.security.auth.callback.PasswordCallback</code></td>
 *     <td>None</td>
 *     <td>
 *        Queries the password if the user.
 *     </td>
 *  </tr>
 * </table>
 *
 */
public final class DefaultClientSecurityContext implements ClientSecurityContext {
    
    private static final String BUNDLE = "com.sun.grid.grm.security.security";
    private final static Logger log = Logger.getLogger(DefaultClientSecurityContext.class.getName(), BUNDLE);
    private final boolean sslDisabled;
    private final CallbackHandler  callbackHandler;
    private final String username;
    private final KeyStore keyManager;
    private final ExecutionEnv env;
    private String cachedUsername;
    private char [] cachedPassword;
    private final X509Certificate [] cachedCert;
    private final SSLHelper sslHelper;
    private long lastCertValidationTimestamp = 0;
    
    /**
     * Create an instance of the DefaultClientSecurityContext.
     * @param env the execution env
     * @param username the name of the user
     * @param callbackHandler callback handler for getting unspecified informations
     * @throws com.sun.grid.grm.security.GrmSecurityException If the security context could not be created
     */
    public DefaultClientSecurityContext(ExecutionEnv env, String username, CallbackHandler callbackHandler) throws GrmSecurityException {
        this.username = username;
        this.callbackHandler = callbackHandler;
        this.sslDisabled = SystemUtil.isSSLDisabled(env);
        this.env = env;
        // Try to get the default keystore file of the user
        File keystoreFile = SecurityPathUtil.getUserKeystoreFile(env, username);
        boolean useAnyKey = false;
        if(!keystoreFile.exists()) {
            // Use the callbackHandler to query the keystore file
            FileCallback keystoreFileCallback =
                new FileCallback(SecurityConstants.KEYSTORE_FILE_CALLBACK);
            try {
                callbackHandler.handle(new Callback [] { keystoreFileCallback });
                keystoreFile = keystoreFileCallback.getFile();
                useAnyKey = true;
            } catch (IOException ex) {
                keystoreFile = null;
            } catch (UnsupportedCallbackException ex) {
                keystoreFile = null;
            }
        }
        
        X509TrustManager trustManager = new AskingX509TrustManager(env, new GrmCATrustManager(env, callbackHandler), callbackHandler);
        
        if(keystoreFile != null && keystoreFile.exists()) {
            sslHelper = SSLHelper.createInstance(env, keystoreFile, new char[0], trustManager);
            keyManager = sslHelper.getKeyStore();
                KeyStore.PasswordProtection credentials = new KeyStore.PasswordProtection(new char[0]);
                X509Certificate[] cert = null;
                if(useAnyKey) {
                    try {
                        Enumeration<String> en = keyManager.aliases();
                        while(en.hasMoreElements()) {
                            String alias = en.nextElement();
                            if(keyManager.isKeyEntry(alias)) {
                                KeyStore.PrivateKeyEntry key = (PrivateKeyEntry) keyManager.getEntry(alias, credentials);
                                cert = (X509Certificate[]) key.getCertificateChain();
                                break;
                            }
                        }
                    } catch(Exception ex) {
                        throw new GrmSecurityException("DefaultClientSecurityContext.nonUserKeystore", ex, BUNDLE, username, keystoreFile);
                    }
                } else {
                    try {
                        KeyStore.PrivateKeyEntry key = (PrivateKeyEntry) keyManager.getEntry(username, credentials);
                        if(key != null) {
                            cert = (X509Certificate[]) key.getCertificateChain();
                        }
                    } catch(Exception ex) {
                        throw new GrmSecurityException("DefaultClientSecurityContext.nonUserKeystore", ex, BUNDLE, username, keystoreFile);
                    }

                }
                if(cert == null) {
                    throw new GrmSecurityException("DefaultClientSecurityContext.nonUserKeystore", BUNDLE, username, keystoreFile);
                }
                cachedCert = cert;
        } else {
            keyManager = null;
            cachedCert = null;
            sslHelper = SSLHelper.createInstance(env, trustManager);
        }
    }
    
    /**
     *  Destroy the security context. 
     *
     *  After calling destroy it is not longer possible to use
     *  the security context.
     */
    public void destroy() {
        SSLHelper.removeInstance(env);
    }
    
    /**
     * Get the execution env of this context.
     * @return the execution env
     */
    public ExecutionEnv getExecutionEnv() {
        return env;
    }
    
    
    /**
     * Create the jmx environment which is used for communication with
     * an MBeanServer.
     * @throws com.sun.grid.grm.security.GrmSecurityException If the jmx environment could not be created
     * @return the jmx environment
     */
    public synchronized Map<String, Object> createJMXEnvironment() throws GrmSecurityException {

        // Intialize the environment for the JMX connection

        int credCount  = 0;
        if (cachedCert != null && sslDisabled) {
            // ssl is disabled and we have a key manager we add
            // the X509 certificate to the credentials
            credCount++;
        } else if (keyManager == null && callbackHandler != null) {
            // We have no key manager => username /password authentication
            if (cachedUsername == null || cachedPassword == null) {
                String defaultUser = username;

                String userPrompt = ResourceBundle.getBundle(BUNDLE).getString("client.username");

                NameCallback nameCallback = new NameCallback(userPrompt, defaultUser);
                String pwPrompt = ResourceBundle.getBundle(BUNDLE).getString("client.password");
                PasswordCallback pwCallback = new PasswordCallback(pwPrompt, false);

                Callback[] cb = new Callback[]{nameCallback, pwCallback};
                try {
                    callbackHandler.handle(cb);
                } catch (IOException ex) {
                    throw new GrmSecurityException("client.error.reading_input", BUNDLE, ex.getLocalizedMessage());
                } catch (UnsupportedCallbackException ex) {
                    throw new GrmSecurityException("unsupported callback", BUNDLE, ex.getLocalizedMessage());
                }
                cachedUsername = nameCallback.getName();
                cachedPassword = pwCallback.getPassword();
                pwCallback.clearPassword();
            }
            if(cachedUsername != null && cachedPassword != null) {
                credCount += 2;
            }
        }
        
        Map<String, Object> jmxEnv = new HashMap<String, Object>();
        
        if(credCount > 0) {
            Map<String, Object> cred = new HashMap<String, Object>(credCount);
            if(cachedCert != null) {
                cred.put(JAASJMXAuthenticator.CERTIFICATE, cachedCert);
            }
            if(cachedUsername != null && cachedPassword != null) {
                cred.put(JAASJMXAuthenticator.USERNAME, cachedUsername);
                cred.put(JAASJMXAuthenticator.PASSWORD, cachedPassword);
            }
            jmxEnv.put("jmx.remote.credentials", cred );
        }
        
        // Write every hour a warning if the certificate expires in less than two weeks
        if (cachedCert != null 
            && lastCertValidationTimestamp + 1000 * 60 * 60 < System.currentTimeMillis()) {
            SSLHelper.validateCertificate(cachedCert);
            lastCertValidationTimestamp = System.currentTimeMillis();
        }
        
        return jmxEnv;
    }
    
    
    /**
     * Establish a JMX connection
     * @param url URL of the jmx service
     * @throws java.io.IOException on any IO error
     * @throws com.sun.grid.grm.security.GrmSecurityException if the jmx environment could not be created
     * @return the connection to the jmx service
     */
    public JMXConnector connect(JMXServiceURL url) throws IOException, GrmSecurityException {
        try {
            return JMXConnectorFactory.connect(url, createJMXEnvironment());
        } catch(GrmSecurityException ex) {
            synchronized(this) {
               cachedUsername = null;
               cachedPassword = null;
            }
           throw ex;
        } catch(IOException ex) {
            synchronized(this) {
               cachedUsername = null;
               cachedPassword = null;
            }
           throw ex;
        }
    }
    
}

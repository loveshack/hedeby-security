/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.security;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.AbstractModule;
import com.sun.grid.grm.bootstrap.ConfigPackage;
import com.sun.grid.grm.bootstrap.ConfigPackageImpl;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.cli.CliCommand;
import com.sun.grid.grm.security.ui.BootstrapSecurityCommand;
import com.sun.grid.grm.security.ui.RemoteInstSecurityCommand;
import com.sun.grid.grm.ui.Command;
import com.sun.grid.grm.validate.Validator;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.security.auth.callback.CallbackHandler;

/**
 * The default security module for a Hedeby system
 */
public class DefaultSecurityModule extends AbstractModule implements SecurityModule {

    public DefaultSecurityModule() {
        super("security");
    }

    /**
     * Create the security context for a server (JVM).
     *
     * @param env      the execution env
     * @param jvmName  name of the jvm
     * @throws com.sun.grid.grm.security.GrmSecurityException the security context could
     *            not be created
     * @return the security context
     */
    public ServerSecurityContext createServerContext(ExecutionEnv env, String jvmName) throws GrmSecurityException {
        return new DefaultServerSecurityContext(env, jvmName);
    }

    /**
     * Create the security context for a client.
     *
     * This method uses a <code>CallbackHandler</code> needed information (e.g credentials
     * for authentication).
     *
     * @param env        the execution env
     * @param username   name of the user
     * @param callbackHandler the callback handler
     * @throws com.sun.grid.grm.security.GrmSecurityException the security context could
     *            not be created
     * @return the security context
     */
    public ClientSecurityContext createClientContext(ExecutionEnv env, String username, CallbackHandler callbackHandler) throws GrmSecurityException {
        return new DefaultClientSecurityContext(env, username, callbackHandler);
    }

    /**
     * Create the command which boostraps the security for this security module.
     *
     * @param env    the execution env
     * @param params parameters for the boostraping
     * @throws com.sun.grid.grm.GrmException if the boostrap security command can not be created
     * @return the boostrap security command
     */
    public Command<Void> createBootstrapCommand(ExecutionEnv env, Map<String, Object> params) throws GrmException {
        return new BootstrapSecurityCommand(params);
    }

    /**
     * Create the command which bootstraps the security on a managed host.
     *
     * @param env    the execution env
     * @param params parameters for the bootstrapping
     * @throws com.sun.grid.grm.GrmException if the command can not be created
     * @return the command
     */
    public Command<Void> createRemoteInstCommand(ExecutionEnv env, Map<String, Object> params) throws GrmException {
        return new RemoteInstSecurityCommand(params);
    }

    /**
     *  Get the vendor of the module
     *
     *  @return the vendor of the module
     */
    public String getVendor() {
        return "Sun Microsystems";
    }

    /**
     *  Get the version of the module.
     *
     *  @return the version of the module
     */
    public String getVersion() {
        return "1.0u5";
    }
    
    /**
     * Get all the JAXB enabled packaged from the module, with their
     * name spaces and prefixes.    
     * @return all the JAXB enabled packaged from the module
     */
    public List<ConfigPackage> getConfigPackages() {
        List<ConfigPackage> cpList = new ArrayList<ConfigPackage>();
        
        URL localURL = getClass().getClassLoader().getResource("hedeby-security.xsd");
        
        cpList.add(new ConfigPackageImpl("com.sun.grid.grm.config.security",
                                         "http://hedeby.sunsource.net/hedeby-security",  
                                         "security",
                                         localURL,
                                         Collections.<String>singletonList("common")));
        return cpList;
    }
    

    /**
     *  Load the cli extensions of this module.
     *
     *  @param extensions list where the classes of the cli extensions are stored
     */
    protected void loadCliExtension(Set<Class<? extends CliCommand>> extensions) {
        extensions.add(com.sun.grid.grm.security.cli.UpdateCACertCliCommand.class);
        extensions.add(com.sun.grid.grm.security.cli.CreateUserCliCommand.class);
        extensions.add(com.sun.grid.grm.security.cli.RenewCertificateCliCommand.class);
        extensions.add(com.sun.grid.grm.security.cli.ShowCertificateCliCommand.class);
        extensions.add(com.sun.grid.grm.security.cli.CreateDaemonCliCommand.class);
        extensions.add(com.sun.grid.grm.security.cli.GetKeyStoreCliCommand.class);
        extensions.add(com.sun.grid.grm.security.cli.ShowAdminUsersCliCommand.class);
        extensions.add(com.sun.grid.grm.security.cli.AddAdminUserCliCommand.class);
        extensions.add(com.sun.grid.grm.security.cli.RemoveAdminUserCliCommand.class);
    }

    /**
     *  Load the validator extensions of this module.
     *  @param extensions list where the classes of the validator extensions are stored
     */
    protected void loadValidatorExtension(Set<Class<? extends Validator>> extensions) {
    }
    
}

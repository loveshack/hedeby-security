/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.security;

import com.sun.grid.grm.bootstrap.ExecutionEnv;

import com.sun.grid.grm.bootstrap.SystemUtil;
import com.sun.grid.grm.security.ca.GrmCATrustManager;
import java.io.File;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStore.PrivateKeyEntry;
import java.security.cert.X509Certificate;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.management.MBeanServer;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXConnectorServer;
import javax.management.remote.JMXConnectorServerFactory;
import javax.management.remote.JMXServiceURL;
import javax.management.remote.rmi.RMIConnectorServer;

/**
 *
 */
public class DefaultServerSecurityContext implements ServerSecurityContext {
    
    private static final String BUNDLE = "com.sun.grid.grm.security.security";
    private static final Logger log = Logger.getLogger(DefaultServerSecurityContext.class.getName());
    private final ExecutionEnv env;
    private final String jvmName;
    private final boolean sslDisabled;
    
    private final KeyStore keyManager;
    private final X509Certificate [] cachedCert;
    private final SSLHelper sslHelper;
    private volatile long lastCertValidationTimestamp = 0;
    
    /** Creates a new instance of DefaultServerSecurityContext 
     * 
     * @param env the execution env
     * @throws com.sun.grid.grm.security.GrmSecurityException if the key manager can not be created
     */
    public DefaultServerSecurityContext(ExecutionEnv env, String jvmName) throws GrmSecurityException{
        this.env = env;
        this.jvmName = jvmName;
        
        sslDisabled = SystemUtil.isSSLDisabled(env);
        
        File keystoreFile = SecurityPathUtil.getDaemonKeystoreFile(env, jvmName);

        GrmCATrustManager trustManager = new GrmCATrustManager(env);
        if (keystoreFile.exists()) {
            char[] password = new char[0];
            sslHelper = SSLHelper.createInstance(env, keystoreFile, password, trustManager);
            keyManager = sslHelper.getKeyStore();
            if (sslDisabled) {
                try {
                    KeyStore.PrivateKeyEntry key = (PrivateKeyEntry) keyManager.getEntry(jvmName, new KeyStore.PasswordProtection(new char[0]));
                    cachedCert = (X509Certificate[]) key.getCertificateChain();
                } catch (Exception ex) {
                    throw new GrmSecurityException("DefaultServerSecurityContext.nonJvmKeystore", ex, BUNDLE, jvmName, keystoreFile);
                }
            } else {
                cachedCert = null;
            }
        } else {
            sslHelper = SSLHelper.createInstance(env, trustManager);
            keyManager = null;
            cachedCert = null;
        }
    }
    

    
    public void destroy() {
        SSLHelper.removeInstance(env);
    }
    
    public String getRemoteUser() {
        return RmiSocket.getRemoteUser();
    }
    
    private void validateCert() {
        // Write every hour a warning if the certificate expires in less than two weeks
        if (cachedCert != null && lastCertValidationTimestamp + 1000 * 60 * 60 < System.currentTimeMillis()) {
            SSLHelper.validateCertificate(cachedCert);
            lastCertValidationTimestamp = System.currentTimeMillis();
        }
    }
    
    public JMXConnectorServer createConnector(MBeanServer mbs, JMXServiceURL url) throws IOException, GrmSecurityException {
        log.log(Level.FINE, "Create JMX connector (url={0}", url);
        Map<String, Object> jmxEnv = null;
        boolean sslEnabled = !sslDisabled;
        if(sslEnabled) {
            jmxEnv = new HashMap<String,Object>(3);
            GrmRMIClientSocketFactory clientSocketFactory  = new GrmRMIClientSocketFactory(env.getCSInfo());
            GrmRMIServerSocketFactory serverSocketFactory  = new GrmRMIServerSocketFactory(sslHelper.getSSLSocketFactory());
            
            jmxEnv.put(RMIConnectorServer.RMI_CLIENT_SOCKET_FACTORY_ATTRIBUTE, clientSocketFactory);
            jmxEnv.put(RMIConnectorServer.RMI_SERVER_SOCKET_FACTORY_ATTRIBUTE, serverSocketFactory);
        } else {
            jmxEnv = new HashMap<String,Object>(1);
        }
        JAASJMXAuthenticator authenticator = new JAASJMXAuthenticator(env.getSystemName(), sslEnabled);
        jmxEnv.put(JMXConnectorServer.AUTHENTICATOR, authenticator);
        
        validateCert();
        
        return JMXConnectorServerFactory.newJMXConnectorServer(url, jmxEnv, mbs);
    }
    
    
    public JMXConnector connect(JMXServiceURL url) throws IOException, GrmSecurityException {
        Map<String, Object> jmxenv = null;

        if (sslDisabled && cachedCert != null) {
            // ssl is disabled and we have a key manager we add
            // the X509 certificate to the credentials
            jmxenv = new HashMap<String, Object>(1);
            Map<String, Object> cred = Collections.<String,Object>singletonMap(JAASJMXAuthenticator.CERTIFICATE, cachedCert);
            jmxenv = Collections.<String,Object>singletonMap("jmx.remote.credentials", cred);
        }
        
        validateCert();
        
        return JMXConnectorFactory.connect(url, jmxenv);
    }
    
}

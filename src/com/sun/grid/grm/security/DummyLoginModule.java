/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.security;

import com.sun.grid.grm.util.I18NManager;
import java.io.IOException;
import java.security.Principal;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.security.auth.Subject;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.UnsupportedCallbackException;
import javax.security.auth.login.LoginException;
import javax.security.auth.spi.LoginModule;

/**
 * This <code>LoginModule</code> should only be used for tests.
 * It accessa all users with name sgetest and sgetest1.
 *
 */
public class DummyLoginModule implements LoginModule {

    private static final String BUNDLE = "com.sun.grid.grm.security.security";

    private CallbackHandler callbackHandler;
    private Subject subject;
    private boolean loginSucceded;
    private boolean commitSucceded;
    private Set<Principal> principals = new HashSet<Principal>() ;

    private static final List<String> KNOWN_USERS = Arrays.asList(new String [] {
        "sgetest", "sgetest1"
    });

    /**
     * Get a list of all known users
     * @return  list of all known users
     */
    public static List<String> getKnownUsers() {
        return Collections.unmodifiableList(KNOWN_USERS);
    }

    /**
     * Initialize the <code>DummyLoginModule</code>
     *
     * @param subject   the current subject
     * @param callbackHandler the callbackhandler (must at least handle a
     *                        <code>NameCallback</code> and a
     *                        </code>PasswordCallback</code>).
     * @param sharedState not used
     * @param options contains the options for the <code>UniDummyLoginModuleode>.
     */
    public void initialize(Subject subject, CallbackHandler callbackHandler, Map sharedState, Map options) {
       this.callbackHandler = callbackHandler;
       this.subject = subject;
    }

    /**
     * Perform the login.
     *
     * @throws javax.security.auth.login.LoginException <ul>
     */
    public boolean login() throws LoginException {

        NameCallback nameCallback = new NameCallback("username: ");

        try {
            callbackHandler.handle( new Callback[] { nameCallback });
        } catch (IOException ex) {
            LoginException le =  new LoginException(
                                    I18NManager.formatMessage("error.io", BUNDLE,
                                                                ex.getLocalizedMessage()));
            le.initCause(ex);
            throw le;
        } catch (UnsupportedCallbackException ex) {
            LoginException le =  new LoginException(
                                      I18NManager.formatMessage("error.invalidCallback", BUNDLE));
            le.initCause(ex);
            throw le;
        }

        loginSucceded = false;
        for (String user: KNOWN_USERS) {
            if (user.equalsIgnoreCase(nameCallback.getName())) {
                loginSucceded = true;
                principals.add(new com.sun.grid.security.login.UserPrincipal(nameCallback.getName()));
                break;
            }
        }
        return loginSucceded;
    }

    /**
     * Commit the login (adds the principals to the subject)
     *
     * @return <code>true</code> of the principals has been added to the subject.
     */
    public boolean commit() {
        if (loginSucceded) {
           subject.getPrincipals().addAll(principals);
           commitSucceded = true; 
        }

        return commitSucceded;
    }

    /**
     * Abort the login.
     * @return Always <code>true</code>
     */
    public boolean abort() {
        logout();
        return true;
    }

    /**
     * Removes all previously added prinicipals from the subject.
     *
     * @return Always <code>true</code>
     */
    public boolean logout() {
        if (commitSucceded) {
            subject.getPrincipals().removeAll(principals);
        }

        subject = null;
        principals.clear();
        callbackHandler = null;
        loginSucceded = false;
        commitSucceded = false;
        return true;
    }

}

/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.security;

import com.sun.grid.grm.security.ca.GrmCAException;
import com.sun.grid.grm.util.I18NManager;

/**
 * Helper class for parsing X500 names.
 *
 * Daemon certificates have the form
 *
 * <pre>
 *   ....,UID=grm_daemon_&lt;username&gt;,CN=<daemon name>,...
 * </pre>
 *
 * User certificates have the form
 *
 * <pre>
 *   ....,UID=&lt;username&gt;,...
 * </pre>
 */
public class GrmCAX500Name {

    private static final String BUNDLE = "com.sun.grid.grm.security.security";
    public static final String GRM_DAEMON_PREFIX = "grm_daemon_";
    private final CertificateType type;
    private final String  username;
    private final String  daemonName;
    
    private GrmCAX500Name(String name) throws GrmCAException {
        String uid = getField(name, "UID");
        if(uid.equals("CA")) {
            type = CertificateType.CA;
            username = null;
            daemonName = null;
        } else if(uid.startsWith(GRM_DAEMON_PREFIX)) {
            type = CertificateType.DAEMON;
            username = uid.substring(GRM_DAEMON_PREFIX.length());
            daemonName = getField(name, "CN");
        } else {
            type = CertificateType.USER;
            username = uid;
            daemonName = null;
        }
    }
    
    private static String getField(String name, String field) throws GrmCAException {
        
        String prefix = field + "=";
        int start = name.indexOf(prefix);
        
        if(start < 0) {
            throw new GrmCAException("x500.fieldNotFound", field, name);
        }
        start +=prefix.length();
        int end = name.indexOf(',', start);
        if(end < 0) {
            end = name.length();
        }
        return name.substring(start, end);
    }
    
    /**
     * Parse a x500 name.
     * 
     * @return the grid ca x500 name
     * @param name the x500 name
     * @throws com.sun.grid.grm.security.ca.GrmCAException if <code>name is not a valid
     *              x500 name of the GrmCA 
     */            
    public static GrmCAX500Name parse(String name) throws GrmCAException {
        return new GrmCAX500Name(name);
    }

    /**
     * Determine of the x500 name describes a daemon
     * @return  <code>true</code> if the x500 name describes a daemon
     */
    public boolean isDaemon() {
        return type == CertificateType.DAEMON;
    }

    /**
     * Get the type of the X509Name
     * @return the type of the X509Name
     */
    public CertificateType getType() {
        return type;
    }
    /**
     * Get the username from the x500 name
     * @return the name of the user
     */
    public String getUsername() {
        return username;
    }

    /**
     * Get the name of the daemon
     * @return the name of the damon or <code>null</code> of the name
     *         does not describe a daemon
     */
    public String getDaemonName() {
        return daemonName;
    }
    
    @Override
    public String toString() {
        switch(type) {
            case CA: return I18NManager.formatMessage("GrmCAX500Name.ca", BUNDLE);
            case DAEMON: return I18NManager.formatMessage("GrmCAX500Name.daemon", BUNDLE, getDaemonName());
            case USER: return I18NManager.formatMessage("GrmCAX500Name.user", BUNDLE, getUsername());
            default: throw new IllegalStateException("Unknown type " + type );
        }
    }
}

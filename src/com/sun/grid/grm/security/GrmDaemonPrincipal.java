/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.security;

import java.security.Principal;

/**
 * Principal for grm daemons (grm jvms)
 *
 * #see com.sun.grid.grm.security.GrmCATrustManagerLoginModule
 */
public class GrmDaemonPrincipal implements Principal {

    private String name = null;

    /** Creates a new instance of GrmDaemonPrincipal 
     *
     *  @param name name of the grm daemon
     */
    public GrmDaemonPrincipal(String name) {
        if (name == null) {
            throw new NullPointerException("name must not be null");
        }

        this.name = name;
    }

    /**
     * Get the name of the grm daemon
     * @return name of the grm daemon
     */
    public String getName() {
        return name;
    }

    /**
     * Determine if <code>obj</code> is equal to <code>this</code>.
     *
     * @param obj the object
     * @return <code>true</code> if <code>obj</code> is an instanceof of
     *         <code>GrmDaemonPrincipal</code> and the name is equals
     */
    public boolean equals(Object obj) {
        return obj instanceof GrmDaemonPrincipal
            && ((GrmDaemonPrincipal)obj).name.equals(name);
    }

    /**
     * Get a string representation of this object.
     *
     * @return the string representation
     */
    public String toString() {
        return "GrmDaemon: " + name;
    }

    /**
     * Get the hashcode of this object
     * @return the hashcode
     */
    public int hashCode() {
        return name.hashCode();
    }

}

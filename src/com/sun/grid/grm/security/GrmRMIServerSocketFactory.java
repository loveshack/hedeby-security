/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

/*
 * GrmRMIServerSocketFactory.java
 *
 * Created on May 10, 2006, 3:35 PM
 *
 */
package com.sun.grid.grm.security;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.rmi.ssl.SslRMIServerSocketFactory;

/**
 * This class is the default socket factory for RMI communication of the Haithabu
 * project.
 *
 * The created sockets of this factory are instances of <code>RmiSocket</code>. The
 * code which is executed in the RMI connection thread can use the static <code>getSocket</code>
 * method to retrieve the used socket for a RMI connection.
 *
 * The created sockets <i>wants<i> client authentification, but it is not required.
 *
 * @todo localization
 */
public class GrmRMIServerSocketFactory extends SslRMIServerSocketFactory {

    private static final String BUNDLE = "com.sun.grid.grm.security.security";
    private Logger logger = Logger.getLogger(GrmRMIServerSocketFactory.class.getName(), BUNDLE);

    private int lastUsedPort;

    private final SSLSocketFactory sslSocketFactory;
    
    /**
     * Create a new service socket factory
     * @param sslSocketFactory the ssl socket factory
     */
    public GrmRMIServerSocketFactory(SSLSocketFactory sslSocketFactory) {
        this.sslSocketFactory = sslSocketFactory;
    }
    /**
     * <p>Creates a server socket that accepts SSL connections
     * configured according to this factory's SSL socket configuration
     * parameters.</p>
     *
     * @param  port port for the socket
     * @return the server socket
     * @throws java.io.IOException on any IO error
     */
    @Override
    public final ServerSocket createServerSocket(final int port)
         throws IOException {

        ServerSocket ret = new ServerSocket(port) {
            @Override
            public Socket accept() throws IOException {

                logger.entering("GrmRMIServerSocketFactory$1", "accept");
                
                Socket socket = super.accept();
                
                if (logger.isLoggable(Level.FINER)) {
                    logger.log(Level.FINER, "GrmRMIServerSocketFactory.accept",
                            new Object [] { socket, Thread.currentThread().getName() });
                }
                
                SSLSocket sslSocket = (SSLSocket)
                sslSocketFactory.createSocket(
                        socket, socket.getInetAddress().getHostName(),
                        socket.getPort(), true);
                
                logger.log(Level.FINER, "ssl socket created");
                sslSocket.setUseClientMode(false);
                sslSocket.setWantClientAuth(true);
                
                String [] enabledCipherSuites = getEnabledCipherSuites();
                
                if (enabledCipherSuites != null) {
                    sslSocket.setEnabledCipherSuites(enabledCipherSuites);
                }
                
                String [] enabledProtocols = getEnabledProtocols();
                
                if (enabledProtocols != null) {
                    sslSocket.setEnabledProtocols(enabledProtocols);
                }
                
                logger.log(Level.FINER, "ssl socket configured");
                
                RmiSocket ret = new RmiSocket(sslSocket);
                
                logger.exiting("GrmRMIServerSocketFactory$1", "accept", ret);
                
                return ret;
            }
        };

        lastUsedPort = ret.getLocalPort();

        if (logger.isLoggable(Level.FINE)) {
            logger.log(Level.FINE, "GrmRMIServerSocketFactory.created",
                       new Object [] { lastUsedPort } );
        }

        return ret;
    }

    /**
     *  Get the last bounded server port.
     *
     *  @return the last bounded server port
     */
    public final int getLastUsedPort() {
        return lastUsedPort;
    }

}

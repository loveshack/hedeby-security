/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.security;


import com.sun.grid.grm.security.role.RolePrincipal;
import com.sun.grid.grm.util.I18NManager;
import java.io.IOException;
import java.net.Socket;
import java.rmi.server.ServerNotActiveException;
import java.security.Principal;
import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.management.remote.JMXAuthenticator;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import javax.security.auth.Subject;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.callback.UnsupportedCallbackException;
import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;

/**
 * The class JAASJMXAuthenticator tries to authenticate a JMX connection
 * with a JAAS LoginModule.
 *
 * <p>The name of the LoginModule is specified in the constructor of the
 * <code>JAASJMXAuthenticator</code>.</p>
 *
 * <p>JMX Clients can provide the user credentials in the following form:</p>
 *
 * <ul>
 *   <li>As string array with the length 2, the first string contains the
 *       username, the second string contains the password.</li>
 *   <li>As map with the username is stored under the key "username" as string
 *       and the password is stored under the key "password" as char array</li>
 *   <li>If the client does not provide any credential, the <code>JAASJMXAuthenticator</code>
 *       tries to retrieve the certificate of the user from the SSL layer. This can only work
 *       if the JMX connection uses the <code>GrmRMIServerSocketFactory</code>. On success the
 *       certificate is provided by a <code>CertifcateCallback</code> object to the LoginModule.
 *       </li>
 * </ul>
 *
 * <h3>Examples:</h3>
 *
 * <h4>Client authentification with username and password:</h4>
 *
 * <pre>
 *   String  username = ...
 *   char [] password = ...
 *   JMXServiceURL url = ...
 *   HashMap env = new HashMap();
 *
 *   Map cred = new HashMap();
 *   cred.put("username", username);
 *   cred.put("password", password);
 *
 *   env.put("jmx.remote.credentials", cred);
 *
 *   JMXConnector jmxc = JMXConnectorFactory.connect(url, env);
 *
 * </pre>
 *
 * <h4>JAAS configuration example</h4>
 *
 * <pre>
 *  sample {
 *     com.sun.grid.grm.security.GrmCATrustManagerLoginModule requisite;
 *
 *     com.sun.security.auth.module.JndiLoginModule optional
 *           user.provider.url="nis://129.157.140.10/federation.Germany.Sun.COM/user"
 *           group.provider.url="nis://129.157.140.10/federation.Germany.Sun.COM/system/group";
 *
 *     com.sun.grid.grm.security.role.RoleLoginModule optional 
 *                                   ;
 *  };
 * </pre>
 *
 * <h4>Setup of a JMX Connector</h4>
 *
 * <pre>
 *   MBeanServer mbs = ...
 *   String jaasConfigName = ...
 *   JMXServiceURL url = ...
 *   HashMap&lt;String,Object&gt; env = new HashMap&lt;String,Object&gt;();
 *
 *   JAASJMXAuthenticator authenticator = new JAASJMXAuthenticator(jaasConfigName);
 *
 *   env.put(JMXConnectorServer.AUTHENTICATOR, authenticator);
 *
 * </pre>
 * <p>The the authenticator should retrieve the credentials from the SSL layer we need
 * the <code>GrmRMIServerSocketFactory</code> socket factory.</p>
 * <pre>
 *
 *   GrmRMIServerSocketFactory ssf = new GrmRMIServerSocketFactory(false, true);
 *
 *   env.put(RMIConnectorServer.RMI_SERVER_SOCKET_FACTORY_ATTRIBUTE,ssf);
 *
 *   JMXConnectorServer cs = JMXConnectorServerFactory.newJMXConnectorServer(url, env, mbs);
 * </pre>
 *
 * @todo    localization
 */
public class JAASJMXAuthenticator implements JMXAuthenticator {

    /** name of the username credential. */
    public static final String USERNAME = "username";

    /** name of the password credential. */
    public static final String PASSWORD = "password";

    /** name of the certificate credential. */
    public static final String CERTIFICATE = "certificate";

    private static final String BUNDLE = "com.sun.grid.grm.security.security";
    private static final Logger LOGGER =
                         Logger.getLogger(JAASJMXAuthenticator.class.getName(), BUNDLE);

    private final String  systemName;
    private final boolean sslEnabled;
    /**
     * Create a new <code>JAASJMXAuthenticator</code>.
     * @param aSystemName the name of the hedeby system
     * @param isSslEnabled is ssl enabled
     */
    public JAASJMXAuthenticator( String aSystemName, boolean isSslEnabled)  {
        systemName = aSystemName;
        sslEnabled = isSslEnabled;
    }

    /**
     * Try to authenticate a jmx connection.
     *
     * @param credentials  credentials for the authentification
     * @throws java.lang.SecurityException if the user can not be authenticated
     * @return the authenticated subject
     */
    @SuppressWarnings("unchecked")
    public final Subject authenticate(final Object credentials)
        throws SecurityException {

        Map<String, Object> myCredentials = new HashMap<String, Object>();

        if (credentials == null && sslEnabled) {
            // Try authentication with credentials from the SSL layer
            try {
                Socket socket = RmiSocket.getSocket();

                if (socket instanceof SSLSocket) {
                    SSLSession session = ((SSLSocket)socket).getSession();

                    try {
                        Certificate[] certs = session.getPeerCertificates();
                        // We only have X509Certificates in our system
                        X509Certificate[] x509Certs =
                                new X509Certificate[certs.length];

                        for(int i = 0; i < certs.length; i++) {
                            x509Certs[i] = (X509Certificate)certs[i];
                        }

                        myCredentials.put(CERTIFICATE, x509Certs);
                        myCredentials.put(USERNAME, "");
                    } catch (SSLPeerUnverifiedException ex) {
                        throw new SecurityException(I18NManager.formatMessage("JAASJMXAuthenticator.insufficientCredential", BUNDLE), ex);
                    }
                } else {
                    throw new SecurityException(I18NManager.formatMessage("JAASJMXAuthenticator.insufficientCredential", BUNDLE));
                }
            } catch (ServerNotActiveException ex) {
                // This error should never happen. JAASJMXAuthenticator can only
                // be used in an rmi connection thread
                throw new IllegalStateException("SSL socket not found in thread local storage", ex);
            }
        } else if (credentials instanceof String[]) {
            // JConsole sends the credentials as string array
            // credentials[0] is the username
            // credentials[1] is the password
            String[] args = (String[])credentials;

            if (args.length == 2) {
                myCredentials.put(USERNAME, args[0]);

                char[] pw = null;

                if (args[1] != null) {
                    pw = args[1].toCharArray();
                }

                myCredentials.put(PASSWORD, pw);
            } else {
                throw new SecurityException(
                             I18NManager.formatMessage("JAASJMXAuthenticator.invalidStringCredentials", BUNDLE));
            }
        } else if (credentials instanceof Map) {
            myCredentials.putAll((Map)credentials);
            if (sslEnabled && myCredentials.containsKey(CERTIFICATE)) {
                throw new SecurityException(
                             I18NManager.formatMessage("JAASJMXAuthenticator.forbiddenCredentialInMap", BUNDLE,
                                                         CERTIFICATE));
            }
        } else {
            throw new SecurityException(
                         I18NManager.formatMessage("JAASJMXAuthenticator.unkownCredentials", BUNDLE,
                                                     credentials.getClass().getName()));
        }

        LoginContext lc = null;

        try {
            lc = new LoginContext(systemName, new CredentialCallbackHandler(systemName, myCredentials));
        } catch (LoginException le) {
            throw new SecurityException(
                         I18NManager.formatMessage("JAASJMXAuthenticator.error.loginContext", BUNDLE),
                         le);
        }

        try {
            
            lc.login();

            if (LOGGER.isLoggable(Level.FINE)) {
                for(Principal p : lc.getSubject().getPrincipals()) {
                    LOGGER.log(Level.FINE, "JAASJMXAuthenticator.principals", p);
                }
            }

            Set<RolePrincipal> roles = lc.getSubject().getPrincipals(RolePrincipal.class);

            if (roles.isEmpty()) {
                LOGGER.log(Level.FINE, "JAASJMXAuthenticator.subjectHasNoRole");
                throw new SecurityException(I18NManager.formatMessage("JAASJMXAuthenticator.permissionDenied", BUNDLE));
            }

            try {
                Subject.doAsPrivileged(lc.getSubject(), new PrintCodeBaseAndPrincipalsAction(), null);
            } catch (PrivilegedActionException ex) {
                if (ex.getException() instanceof SecurityException) {
                    throw (SecurityException)ex.getException();
                } else {
                    throw new SecurityException(
                                I18NManager.formatMessage("JAASJMXAuthenticator.invalidPrivileges", BUNDLE,
                                                            ex.getException().getLocalizedMessage()),
                                ex.getException());
                }
            }

            return lc.getSubject();
        } catch (LoginException ex) {
            LOGGER.throwing("JAASJMXAuthenticator", "authenticate", ex);
            throw new SecurityException(
                        I18NManager.formatMessage("JAASJMXAuthenticator.loginFailed", BUNDLE,
                                                    ex.getLocalizedMessage()),
                        ex);
        } catch(SecurityException ex) {
            throw ex;
        } catch(Throwable ex) {
            throw new SecurityException(
                    I18NManager.formatMessage("JAASJMXAuthenticator.unexpectedError", BUNDLE,
                    ex.getLocalizedMessage()),
                    ex);
        }
    }

    /**
     *  This class has only been added for debugging purpose. 
     *  This action is executed in the security context of the authenticated
     *  user and logs the codebase and principals of the user.
     */
    private static class PrintCodeBaseAndPrincipalsAction implements PrivilegedExceptionAction {

        public Object run() throws Exception {

            if(LOGGER.isLoggable(Level.FINE)) {
                LOGGER.log(Level.FINE, "JAASJMXAuthenticator.codesource",
                           getClass().getProtectionDomain().getCodeSource());

                Principal [] ps = getClass().getProtectionDomain().getPrincipals();

                for (Principal p: ps) {
                    LOGGER.log(Level.FINE, "JAASJMXAuthenticator.codeprincipal", p );
                }
            }
            return null;
        }
    }

    /**
     *  Private credential <code>CallbackHandler</code>.
     */
    private static class CredentialCallbackHandler implements CallbackHandler {

        private final Map credentials;
        private final String systemName;

        public CredentialCallbackHandler(String systemName, final Map credentials) {

            this.credentials = credentials;
            this.systemName = systemName;

        }

        public void handle(final Callback[] callbacks)
          throws IOException, UnsupportedCallbackException {
            for (int i = 0; i < callbacks.length; i++) {
                if (callbacks[i] instanceof NameCallback) {

                    // prompt the user for a username
                    NameCallback nc = (NameCallback)callbacks[i];

                    nc.setName((String)credentials.get(USERNAME));

                } else if (callbacks[i] instanceof PasswordCallback) {

                    // prompt the user for sensitive information
                    PasswordCallback pc = (PasswordCallback)callbacks[i];

                    if (credentials.get(PASSWORD) == null) {
                        pc.setPassword(new char[0]);
                    } else {
                        pc.setPassword((char[])credentials.get(PASSWORD));
                    }
                } else if (callbacks[i] instanceof X509CertificateChainCallback) {

                    X509Certificate [] certs =
                            (X509Certificate[])credentials.get(CERTIFICATE);

                    X509CertificateChainCallback pkc =
                            (X509CertificateChainCallback)callbacks[i];
                    pkc.setChain(certs);

                } else {
                    throw new UnsupportedCallbackException(callbacks[i]);
                }

            }

        }
    }
}

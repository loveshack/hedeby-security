/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.security;

import com.sun.grid.grm.security.ca.GrmCAException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.nio.channels.SocketChannel;
import java.rmi.server.RemoteServer;
import java.rmi.server.ServerNotActiveException;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;

/**
 * A <code>RmiSocket</code> stores a reference of the socket which is used
 * for a rmi connection in the thread local storage of the RMI connection thread.
 *
 * @see java.rmi.server.RemoteServer
 */
public class RmiSocket extends Socket implements Serializable {

    private final static long serialVersionUID = -2007070101L;
    private static final String BUNDLE = "com.sun.grid.grm.security.security";
    private static final Logger LOGGER =
            Logger.getLogger(RmiSocket.class.getName(), BUNDLE);
    private static final ThreadLocal<WeakReference<RmiSocket>> THREAD_LOCAL =
            new ThreadLocal<WeakReference<RmiSocket>>();
    private Socket socket = null;

    /**
     * Create a new <code>RmiSocket</code>.
     *
     * Package private for security reasons.
     *
     * @param socket the wrapped socket
     */
    RmiSocket(final Socket socket) {
        LOGGER.entering("RmiSocket", "<init>", socket);
        this.socket = socket;
        LOGGER.exiting("RmiSocket", "<init>");
    }

    private void initThreadLocal() {
        LOGGER.entering("RmiSocket", "initThreadLocal");
        WeakReference<RmiSocket> ref = THREAD_LOCAL.get();
        if (ref == null || ref.get() != this) {
            try {
                // The call of getClientHost will throw an exception
                // if we are not in the RMI connection thread
                RemoteServer.getClientHost();
                // We are in the RMI connection thread, set the thread local
                THREAD_LOCAL.set(new WeakReference<RmiSocket>(this));
            } catch (ServerNotActiveException sna) {
                // We are not in the RMI connection thread
                if (LOGGER.isLoggable(Level.FINE)) {
                    LOGGER.log(Level.FINE, "RMISocket.init",
                            Thread.currentThread());
                }
            }
        }
        LOGGER.exiting("RmiSocket", "initThreadLocal");
    }

    /**
     *  Get the socket which is used by the current <code>RemoteServer</code> connection.
     *
     *  @return the socket
     *  @java.rmi.server.ServerNotActiveException if no remote method invocation is
     *                                            being processed in the current thread
     */
    static Socket getSocket() throws ServerNotActiveException {
        LOGGER.entering("RmiSocket", "getSocket");
        try {
            RemoteServer.getClientHost();
            RmiSocket rmiSocket = THREAD_LOCAL.get().get();
            Socket ret = rmiSocket.getInnerSocket();
            LOGGER.exiting("RmiSocket", "getSocket", ret);
            return ret;
        } catch (NullPointerException ex) {
            throw new ServerNotActiveException();
        } catch (ServerNotActiveException ex) {
            LOGGER.throwing("RmiSocket", "getSocket", ex);
            throw ex;
        }
    }

    /**
     * Get the name of the remote user in the form <code>&lt;user&gt;@&lt;host&gt</code>.
     *
     * @return the name of the remote user or <code>null</code> if the remote user
     *         can not be determined, becaouse the underlaying socket does not provide
     *         this information
     */
    static public String getRemoteUser() {
        Socket socket;
        try {
            socket = getSocket();
        } catch (ServerNotActiveException ex) {
            return null;
        }

        if (socket instanceof SSLSocket) {
            SSLSession session = ((SSLSocket) socket).getSession();

            try {
                Certificate[] certs = session.getPeerCertificates();

                X509Certificate cert = (X509Certificate) certs[0];

                GrmCAX500Name name;
                try {
                    name = GrmCAX500Name.parse(cert.getSubjectX500Principal().getName());
                } catch (GrmCAException ex) {
                    return null;
                }

                if (name.isDaemon()) {
                    return name.getDaemonName() + "@" + socket.getInetAddress().getHostName();
                } else {
                    return name.getUsername() + "@" + socket.getInetAddress().getHostName();
                }
            } catch (SSLPeerUnverifiedException ex) {

                return null;
            }
        } else {
            return null;
        }
    }

    private Socket getInnerSocket() {
        return socket;
    }

    /*
     * Inherited from super class.
     *
     * @return the socket channel
     * @see java.net.Socket#getChannel();
     */
    public SocketChannel getChannel() {
        LOGGER.entering("RmiSocket", "getChannel");
        initThreadLocal();
        LOGGER.exiting("RmiSocket", "getChannel");
        SocketChannel ret = socket.getChannel();
        LOGGER.exiting("RmiSocket", "getChannel");
        return ret;
    }

    /*
     * Inherited from super class.
     *
     * @param on  the on flag
     * @throws java.net.SocketException
     * @see java.net.Socket#setTcpNoDelay
     */
    public void setTcpNoDelay(final boolean on) throws SocketException {
        LOGGER.entering("RmiSocket", "setTcpNoDelay");
        initThreadLocal();

        try {
            socket.setTcpNoDelay(on);
        } catch (SocketException ex) {
            LOGGER.throwing("RmiSocket", "setTcpNoDelay", ex);
            throw ex;
        }

        LOGGER.exiting("RmiSocket", "setTcpNoDelay");
    }

    /*
     * Inherited from super class.
     *
     * @param on the on flag
     * @throws java.net.SocketException
     * @see java.net.Socket#setReuseAddress
     */
    public void setReuseAddress(final boolean on) throws SocketException {
        LOGGER.entering("RmiSocket", "setReuseAddress");
        initThreadLocal();

        try {
            socket.setReuseAddress(on);
        } catch (SocketException ex) {
            LOGGER.throwing("RmiSocket", "setReuseAddress", ex);
            throw ex;
        }

        LOGGER.exiting("RmiSocket", "setReuseAddress");
    }

    /*
     * Inherited from super class.
     *
     * @param on the on flag
     * @throws java.net.SocketException
     * @see java.net.Socket#setOOBInline
     */
    public void setOOBInline(final boolean on) throws SocketException {
        LOGGER.entering("RmiSocket", "setOOBInline");
        initThreadLocal();

        try {
            socket.setOOBInline(on);
        } catch (SocketException ex) {
            LOGGER.throwing("RmiSocket", "setOOBInline", ex);
            throw ex;
        }

        LOGGER.exiting("RmiSocket", "setOOBInline");
    }

    /*
     * Inherited from super class.
     *
     * @param on the on flag
     * @throws java.net.SocketException
     * @see java.net.Socket#setKeepAlive
     */
    public void setKeepAlive(final boolean on) throws SocketException {
        LOGGER.entering("RmiSocket", "setKeepAlive");
        initThreadLocal();

        try {
            socket.setKeepAlive(on);
        } catch (SocketException ex) {
            LOGGER.throwing("RmiSocket", "setKeepAlive", ex);
            throw ex;
        }

        LOGGER.exiting("RmiSocket", "setKeepAlive");
    }

    /*
     * Inherited from super class.
     *
     * @param endpoint the endpoint
     * @throws java.io.IOException
     * @see java.net.Socket#connect(java.net.SocketAddress)
     */
    public void connect(final SocketAddress endpoint) throws IOException {
        LOGGER.entering("RmiSocket", "connect");
        initThreadLocal();

        try {
            socket.connect(endpoint);
        } catch (IOException ex) {
            LOGGER.throwing("RmiSocket", "connect", ex);
            throw ex;
        }

        LOGGER.exiting("RmiSocket", "connect");
    }

    /*
     * Inherited from super class.
     *
     * @param bindpoint
     * @throws java.io.IOException
     * @see java.net.Socket#bind
     */
    public void bind(final SocketAddress bindpoint) throws IOException {
        LOGGER.entering("RmiSocket", "bind");
        initThreadLocal();

        try {
            socket.bind(bindpoint);
        } catch (IOException ex) {
            LOGGER.throwing("RmiSocket", "bind", ex);
            throw ex;
        }

        LOGGER.exiting("RmiSocket", "bind");
    }

    /*
     * Inherited from super class.
     *
     * @param tc
     * @throws java.net.SocketException
     * @see java.net.Socket#setTrafficClass
     */
    public void setTrafficClass(final int tc) throws SocketException {
        LOGGER.entering("RmiSocket", "setTrafficClass");
        initThreadLocal();

        try {
            socket.setTrafficClass(tc);
        } catch (SocketException ex) {
            LOGGER.throwing("RmiSocket", "setTrafficClass", ex);
            throw ex;
        }

        LOGGER.exiting("RmiSocket", "setTrafficClass");
    }

    /*
     * Inherited from super class.
     *
     * @param timeout
     * @throws java.net.SocketException
     * @see java.net.Socket#setSoTimeout
     */
    public void setSoTimeout(final int timeout) throws SocketException {
        LOGGER.entering("RmiSocket", "setSoTimeout");
        initThreadLocal();

        try {
            socket.setSoTimeout(timeout);
        } catch (SocketException ex) {
            LOGGER.throwing("RmiSocket", "setSoTimeout", ex);
            throw ex;
        }

        LOGGER.exiting("RmiSocket", "setSoTimeout");
    }

    /*
     * Inherited from super class.
     *
     * @param size
     * @throws java.net.SocketException
     * @see java.net.Socket#setSendBufferSize
     */
    public void setSendBufferSize(final int size) throws SocketException {
        LOGGER.entering("RmiSocket", "setSendBufferSize");
        initThreadLocal();

        try {
            socket.setSendBufferSize(size);
        } catch (SocketException ex) {
            LOGGER.throwing("RmiSocket", "setSendBufferSize", ex);
            throw ex;
        }

        LOGGER.exiting("RmiSocket", "setSendBufferSize");
    }

    /*
     * Inherited from super class.
     *
     * @param size
     * @throws java.net.SocketException
     * @see java.net.Socket#setReceiveBufferSize
     */
    public void setReceiveBufferSize(final int size) throws SocketException {
        LOGGER.entering("RmiSocket", "setReceiveBufferSize");
        initThreadLocal();

        try {
            socket.setReceiveBufferSize(size);
        } catch (SocketException ex) {
            LOGGER.throwing("RmiSocket", "setReceiveBufferSize", ex);
            throw ex;
        }

        LOGGER.exiting("RmiSocket", "setReceiveBufferSize");
    }

    /*
     * Inherited from super class.
     *
     * @see java.net.Socket#sendUrgentData
     * @param data
     * @throws java.io.IOException
     */
    public void sendUrgentData(final int data) throws IOException {
        LOGGER.entering("RmiSocket", "sendUrgentData");
        initThreadLocal();

        try {
            socket.sendUrgentData(data);
        } catch (IOException ex) {
            LOGGER.throwing("RmiSocket", "sendUrgentData", ex);
            throw ex;
        }

        LOGGER.exiting("RmiSocket", "sendUrgentData");
    }

    /*
     * Inherited from super class.
     *
     * @param endpoint
     * @param timeout
     * @see java.net.Socket#connect(java.net.SocketAddress,int)
     */
    public void connect(final SocketAddress endpoint, final int timeout)
            throws IOException {
        LOGGER.entering("RmiSocket", "connect");
        initThreadLocal();

        try {
            socket.connect(endpoint, timeout);
        } catch (IOException ex) {
            LOGGER.throwing("RmiSocket", "sendUrgentData", ex);
            throw ex;
        }

        LOGGER.exiting("RmiSocket", "connect");
    }

    /*
     * Inherited from super class.
     *
     * @param on
     * @param linger
     * @throws java.net.SocketException
     * @see java.net.Socket#setSoLinger
     */
    public void setSoLinger(final boolean on, final int linger)
            throws SocketException {
        LOGGER.entering("RmiSocket", "setSoLinger");

        try {
            initThreadLocal();
            socket.setSoLinger(on, linger);
        } catch (SocketException ex) {
            LOGGER.throwing("RmiSocket", "setSoLinger", ex);
            throw ex;
        }

        LOGGER.exiting("RmiSocket", "setSoLinger");
    }

    /*
     * Inherited from super class.
     *
     * @throws java.io.IOException
     * @see java.net.Socket#shutdownOutput
     */
    public void shutdownOutput() throws IOException {
        LOGGER.entering("RmiSocket", "shutdownOutput");
        initThreadLocal();

        try {
            socket.shutdownOutput();
        } catch (IOException ex) {
            LOGGER.throwing("RmiSocket", "shutdownOutput", ex);
            throw ex;
        }

        LOGGER.exiting("RmiSocket", "shutdownOutput");
    }

    /*
     * Inherited from super class.
     *
     * @throws java.io.IOException
     * @see java.net.Socket#shutdownInput
     */
    public void shutdownInput() throws IOException {
        LOGGER.entering("RmiSocket", "shutdownInput");
        initThreadLocal();

        try {
            socket.shutdownInput();
        } catch (IOException ex) {
            LOGGER.throwing("RmiSocket", "shutdownInput", ex);
            throw ex;
        }

        LOGGER.exiting("RmiSocket", "shutdownInput");
    }

    /*
     * Inherited from super class.
     *
     * @see java.net.Socket#shutdownInput
     * @param connectionTime
     * @param latency
     * @param bandwidth
     * @see java.net.Socket#setPerformancePreferences
     */
    public void setPerformancePreferences(final int connectionTime,
            final int latency,
            final int bandwidth) {
        LOGGER.entering("RmiSocket", "setPerformancePreferences");
        initThreadLocal();
        socket.setPerformancePreferences(connectionTime, latency, bandwidth);
        LOGGER.exiting("RmiSocket", "setPerformancePreferences");
    }

    /*
     * Inherited from super class.
     *
     * @return
     * @see java.net.Socket#isOutputShutdown
     */
    public boolean isOutputShutdown() {
        LOGGER.entering("RmiSocket", "isOutputShutdown");
        initThreadLocal();
        boolean ret = socket.isOutputShutdown();
        LOGGER.exiting("RmiSocket", "isOutputShutdown", ret);
        return ret;
    }

    /*
     * Inherited from super class.
     *
     * @return
     * @see java.net.Socket#isInputShutdown
     */
    public boolean isInputShutdown() {
        LOGGER.entering("RmiSocket", "isInputShutdown");
        initThreadLocal();
        boolean ret = socket.isInputShutdown();
        LOGGER.exiting("RmiSocket", "isInputShutdown", ret);
        return ret;
    }

    /*
     * Inherited from super class.
     *
     * @return
     * @see java.net.Socket#isConnected
     */
    public boolean isConnected() {
        LOGGER.entering("RmiSocket", "isConnected");
        initThreadLocal();
        boolean ret = socket.isConnected();
        LOGGER.exiting("RmiSocket", "isConnected", ret);
        return ret;
    }

    /*
     * Inherited from super class.
     *
     * @return
     * @see java.net.Socket#isClosed
     */
    public boolean isClosed() {
        LOGGER.entering("RmiSocket", "isClosed");
        initThreadLocal();
        boolean ret = socket.isClosed();
        LOGGER.exiting("RmiSocket", "isClosed", ret);
        return ret;
    }

    /*
     * Inherited from super class.
     *
     * @return
     * @see java.net.Socket#isBound
     */
    public boolean isBound() {
        LOGGER.entering("RmiSocket", "isBound");
        initThreadLocal();
        boolean ret = socket.isBound();
        LOGGER.exiting("RmiSocket", "isBound", ret);
        return ret;
    }

    /*
     * Inherited from super class.
     *
     * @return
     * @see java.net.Socket#getLocalSocketAddress
     */
    public SocketAddress getLocalSocketAddress() {
        LOGGER.entering("RmiSocket", "getLocalSocketAddress");
        initThreadLocal();
        SocketAddress ret = socket.getLocalSocketAddress();
        LOGGER.exiting("RmiSocket", "getLocalSocketAddress", ret);
        return ret;
    }

    /*
     * Inherited from super class.
     *
     * @return
     * @see java.net.Socket#getLocalPort
     */
    public int getLocalPort() {
        LOGGER.entering("RmiSocket", "getLocalPort");
        initThreadLocal();
        int ret = socket.getLocalPort();
        LOGGER.exiting("RmiSocket", "getLocalPort", ret);
        return ret;
    }

    /*
     * Inherited from super class.
     *
     * @return
     * @see java.net.Socket#getLocalAddress
     */
    public InetAddress getLocalAddress() {
        LOGGER.entering("RmiSocket", "getLocalAddress");
        initThreadLocal();
        InetAddress ret = socket.getLocalAddress();
        LOGGER.exiting("RmiSocket", "getLocalAddress", ret);
        return ret;
    }

    /*
     * Inherited from super class.
     *
     * @return
     * @see java.net.Socket#getKeepAlive
     */
    public boolean getKeepAlive() throws SocketException {
        LOGGER.entering("RmiSocket", "getKeepAlive");
        initThreadLocal();

        try {
            boolean ret = socket.getKeepAlive();
            LOGGER.exiting("RmiSocket", "getKeepAlive", ret);
            return ret;
        } catch (SocketException ex) {
            LOGGER.throwing("RmiSocket", "getKeepAlive", ex);
            throw ex;
        }
    }

    /*
     * Inherited from super class.
     *
     * @see java.net.Socket#getInputStream
     */
    public InputStream getInputStream() throws IOException {
        LOGGER.entering("RmiSocket", "getInputStream");
        initThreadLocal();

        try {
            InputStream ret = socket.getInputStream();
            LOGGER.exiting("RmiSocket", "getInputStream", ret);
            return ret;
        } catch (IOException ex) {
            LOGGER.throwing("RmiSocket", "getInputStream", ex);
            throw ex;
        }
    }

    /*
     * Inherited from super class.
     *
     * @see java.net.Socket#getInetAddress
     */
    public InetAddress getInetAddress() {
        LOGGER.entering("RmiSocket", "getInetAddress");
        initThreadLocal();
        InetAddress ret = socket.getInetAddress();
        LOGGER.exiting("RmiSocket", "getInetAddress", ret);
        return ret;
    }

    /*
     * Inherited from super class.
     *
     * @see java.net.Socket#close
     */
    public void close() throws IOException {
        LOGGER.entering("RmiSocket", "close");

        initThreadLocal();

        try {
            socket.close();
            LOGGER.exiting("RmiSocket", "close");
        } catch (IOException ex) {
            LOGGER.throwing("RmiSocket", "close", ex);
            throw ex;
        }
    }


    /*
     * Inherited from super class.
     *
     * @see java.net.Socket#getOOBInline
     */
    public boolean getOOBInline() throws SocketException {
        LOGGER.entering("RmiSocket", "getOOBInline");
        initThreadLocal();

        try {
            boolean ret = socket.getOOBInline();
            LOGGER.exiting("RmiSocket", "getOOBInline", ret);
            return ret;
        } catch (SocketException ex) {
            LOGGER.throwing("RmiSocket", "getOOBInline", ex);
            throw ex;
        }
    }

    /*
     * Inherited from super class.
     *
     * @see java.net.Socket#getOutputStream
     */
    public OutputStream getOutputStream() throws IOException {
        LOGGER.entering("RmiSocket", "getOutputStream");
        initThreadLocal();

        try {
            OutputStream ret = socket.getOutputStream();
            LOGGER.exiting("RmiSocket", "getOutputStream", ret);
            return ret;
        } catch (IOException ex) {
            LOGGER.throwing("RmiSocket", "getOutputStream", ex);
            throw ex;
        }
    }

    /*
     * Inherited from super class.
     *
     * @see java.net.Socket#getPort
     */
    public int getPort() {
        LOGGER.entering("RmiSocket", "getPort");
        initThreadLocal();
        int ret = socket.getPort();
        LOGGER.exiting("RmiSocket", "getPort", ret);
        return ret;
    }

    /*
     * Inherited from super class.
     *
     * @see java.net.Socket#getReceiveBufferSize
     */
    public int getReceiveBufferSize() throws SocketException {
        LOGGER.entering("RmiSocket", "getReceiveBufferSize");
        initThreadLocal();

        try {
            int ret = socket.getReceiveBufferSize();
            LOGGER.exiting("RmiSocket", "getReceiveBufferSize", ret);
            return ret;
        } catch (SocketException ex) {
            LOGGER.throwing("RmiSocket", "getReceiveBufferSize", ex);
            throw ex;
        }
    }

    /*
     * Inherited from super class.
     *
     * @see java.net.Socket#getRemoteSocketAddress
     */
    public SocketAddress getRemoteSocketAddress() {
        LOGGER.entering("RmiSocket", "getRemoteSocketAddress");
        initThreadLocal();
        SocketAddress ret = socket.getRemoteSocketAddress();
        LOGGER.exiting("RmiSocket", "getRemoteSocketAddress", ret);
        return ret;
    }

    /*
     * Inherited from super class.
     *
     * @see java.net.Socket#getReuseAddress
     */
    public boolean getReuseAddress() throws SocketException {
        LOGGER.entering("RmiSocket", "getReuseAddress");
        initThreadLocal();

        try {
            boolean ret = socket.getReuseAddress();
            LOGGER.exiting("RmiSocket", "getReuseAddress", ret);
            return ret;
        } catch (SocketException ex) {
            LOGGER.throwing("RmiSocket", "getReuseAddress", ex);
            throw ex;
        }
    }

    /*
     * Inherited from super class.
     *
     * @see java.net.Socket#getSendBufferSize
     */
    public int getSendBufferSize() throws SocketException {
        LOGGER.entering("RmiSocket", "getSendBufferSize");
        initThreadLocal();

        try {
            int ret = socket.getSendBufferSize();
            LOGGER.exiting("RmiSocket", "getSendBufferSize", ret);
            return ret;
        } catch (SocketException ex) {
            LOGGER.throwing("RmiSocket", "getSendBufferSize", ex);
            throw ex;
        }
    }

    /*
     * Inherited from super class.
     *
     * @see java.net.Socket#getSoLinger
     */
    public int getSoLinger() throws SocketException {
        LOGGER.entering("RmiSocket", "getSoLinger");
        initThreadLocal();

        try {
            int ret = socket.getSoLinger();
            LOGGER.exiting("RmiSocket", "getSoLinger", ret);
            return ret;
        } catch (SocketException ex) {
            LOGGER.throwing("RmiSocket", "getSoLinger", ex);
            throw ex;
        }
    }

    /*
     * Inherited from super class.
     *
     * @see java.net.Socket#getSoTimeout
     */
    public int getSoTimeout() throws SocketException {
        LOGGER.entering("RmiSocket", "getSoTimeout");
        initThreadLocal();

        try {
            int ret = socket.getSoTimeout();
            LOGGER.exiting("RmiSocket", "getSoTimeout", ret);
            return ret;
        } catch (SocketException ex) {
            LOGGER.throwing("RmiSocket", "getSoTimeout", ex);
            throw ex;
        }
    }

    /*
     * Inherited from super class.
     *
     * @see java.net.Socket#getTcpNoDelay
     */
    public boolean getTcpNoDelay() throws SocketException {
        LOGGER.entering("RmiSocket", "getTcpNoDelay");
        initThreadLocal();

        try {
            boolean ret = socket.getTcpNoDelay();
            LOGGER.exiting("RmiSocket", "getTcpNoDelay", ret);
            return ret;
        } catch (SocketException ex) {
            LOGGER.throwing("RmiSocket", "getTcpNoDelay", ex);
            throw ex;
        }
    }

    /*
     * Inherited from super class.
     *
     * @see java.net.Socket#getTrafficClass
     */
    public int getTrafficClass() throws SocketException {
        LOGGER.entering("RmiSocket", "getTrafficClass");
        initThreadLocal();

        try {
            int ret = socket.getTrafficClass();
            LOGGER.exiting("RmiSocket", "getTrafficClass", ret);
            return ret;
        } catch (SocketException ex) {
            LOGGER.throwing("RmiSocket", "getTrafficClass", ex);
            throw ex;
        }
    }
}

/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.security;

import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.security.ca.GrmCAException;
import com.sun.grid.grm.util.HostAndPort;
import java.io.File;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyStore;
import java.security.KeyStore.Builder;
import java.security.KeyStore.PasswordProtection;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.KeyStoreBuilderParameters;
import javax.net.ssl.ManagerFactoryParameters;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509KeyManager;
import javax.net.ssl.X509TrustManager;

/**
 * Helper class for SSL setup.
 *
 */
final class SSLHelper {

    private static final String BUNDLE = "com.sun.grid.grm.security.security";
    private static final Logger log = Logger.getLogger(SSLHelper.class.getName(), BUNDLE);
    private final static Map<HostAndPort, SSLHelper> contextMap = new HashMap<HostAndPort, SSLHelper>();
    private final X509KeyManager keyManager;
    private final KeyStore keystore;
    private final SSLContext ctx;

    private SSLHelper(Builder fsBuilder, X509TrustManager trustManager) throws GrmSecurityException {

        if (fsBuilder != null) {
            ManagerFactoryParameters ksParams =
                    new KeyStoreBuilderParameters(Collections.singletonList(fsBuilder));
            KeyManagerFactory factory;
            try {
                factory = KeyManagerFactory.getInstance(SecurityConstants.KEYMANAGERFACTORY_ALGORITHM);
            } catch (NoSuchAlgorithmException ex) {
                throw new GrmSecurityException(ex.getLocalizedMessage(), ex);
            }

            try {
                factory.init(ksParams);
            } catch (InvalidAlgorithmParameterException ex) {
                throw new GrmSecurityException("SSLHelper.inalidKeyManagerAlgorithm", ex, BUNDLE);
            }
            this.keyManager = (X509KeyManager) factory.getKeyManagers()[0];

            try {
                keystore = fsBuilder.getKeyStore();
            } catch (KeyStoreException ex) {
                throw new IllegalStateException("Can not get keystore", ex);
            }
        } else {
            keyManager = null;
            keystore = null;
        }

        try {
            ctx = SSLContext.getInstance(SecurityConstants.SSL_PROTOCOL);
        } catch (NoSuchAlgorithmException ex) {
            throw new GrmSecurityException("SSLHelper.sslCreateError", ex,
                    BUNDLE, SecurityConstants.SSL_PROTOCOL,
                    ex.getLocalizedMessage());
        }

        KeyManager[] keyManagers;

        if (keyManager == null) {
            keyManagers = null;
        } else {
            keyManagers = new KeyManager[]{keyManager};
        }

        try {
            ctx.init(keyManagers,
                    new TrustManager[]{trustManager},
                    null);
        } catch (Exception ex) {
            throw new IllegalStateException(ex.getLocalizedMessage(), ex);
        }
    }

    private SSLHelper(File keystoreFile, char[] password, X509TrustManager trustManager) throws GrmSecurityException {
        this(Builder.newInstance(SecurityConstants.KEYSTORE_ALGORITHM, null,
                keystoreFile, new PasswordProtection(password)), trustManager);
    }

    private SSLHelper(KeyStore keystore, char[] password, X509TrustManager trustManager) throws GrmSecurityException {
        this(Builder.newInstance(keystore, new PasswordProtection(password)), trustManager);
    }

    private SSLHelper(X509TrustManager trustManager) throws GrmSecurityException {
        this(null, trustManager);
    }

    KeyStore getKeyStore() {
        return keystore;
    }

    /**
     * Get the ssl context
     * @return
     */
    SSLContext getSSLContext() {
        return ctx;
    }

    /**
     * Get the socket factory
     * @return the socket factory
     */
    SSLSocketFactory getSSLSocketFactory() {
        return ctx.getSocketFactory();
    }

    /**
     * Remove the ssl context of a hedeby system
     * @param env the execution env
     */
    synchronized static void removeInstance(ExecutionEnv env) {
        contextMap.remove(env.getCSInfo());
    }

    /**
     * Create a new SSLHelper for a hedeby system
     * @param env            the exection env of the hedeby system
     * @param keystoreFile   the keystore file
     * @param password       password for the keystore file
     * @param trustManager   the trustmanager
     * @return  the SSLHelper
     * @throws com.sun.grid.grm.security.GrmSecurityException if the SSLHelper can not be created
     */
    synchronized static SSLHelper createInstance(ExecutionEnv env, File keystoreFile, char[] password, X509TrustManager trustManager) throws GrmSecurityException {
        if (contextMap.containsKey(env.getSystemName())) {
            throw new GrmSecurityException("security context for {0}:{1} already exists", BUNDLE, env.getCSHost(), env.getCSPort());
        }
        SSLHelper ret = new SSLHelper(keystoreFile, password, trustManager);
        contextMap.put(env.getCSInfo(), ret);
        return ret;
    }

    /**
     * Create a new SSLHelper for a hedeby system
     * 
     * @param env           the exection env of the hedeby system
     * @param keystore      the keystore
     * @param password      password for the keystore
     * @param trustManager  the trust manager
     * @return the SSLHelper
     * @throws com.sun.grid.grm.security.GrmSecurityException if the SSLHelper can not be created
     */
    synchronized static SSLHelper createInstance(ExecutionEnv env, KeyStore keystore, char[] password, X509TrustManager trustManager) throws GrmSecurityException {
        if (contextMap.containsKey(env.getSystemName())) {
            throw new GrmSecurityException("security context for {0}:{1} already exists", BUNDLE, env.getCSHost(), env.getCSPort());
        }
        SSLHelper ret = new SSLHelper(keystore, password, trustManager);
        contextMap.put(env.getCSInfo(), ret);
        return ret;
    }

    /**
     * Create a new SSLHelper for a hedeby for users without keystore
     * @param env           the execution env of the hedeby system
     * @param trustManager  the trust manager
     * @return the SSLHelper
     * @throws com.sun.grid.grm.security.GrmSecurityException if the SSLHelper can not be created
     */
    synchronized static SSLHelper createInstance(ExecutionEnv env, X509TrustManager trustManager) throws GrmSecurityException {
        if (contextMap.containsKey(env.getSystemName())) {
            throw new GrmSecurityException("security context for {0}:{1} already exists", BUNDLE, env.getCSHost(), env.getCSPort());
        }
        SSLHelper ret = new SSLHelper(trustManager);
        contextMap.put(env.getCSInfo(), ret);
        return ret;
    }

    /**
     *  Get the ssl context for a system.
     *
     * @param csInfo the CS context info of the system
     * @return the ssl context of <code>null</code>
     */
    public static SSLContext getSSLContext(HostAndPort csInfo) {
        SSLHelper helper = contextMap.get(csInfo);
        if (helper != null) {
            return helper.getSSLContext();
        }
        return null;
    }

    /**
     *  Get the ssl socket factory for the application
     *  @param csInfo the CS context info of the system
     * @return the socket factor for the application
     */
    public static SSLSocketFactory getSocketFactory(HostAndPort csInfo) {
        SSLContext ctx = getSSLContext(csInfo);
        if (ctx != null) {
            return ctx.getSocketFactory();
        } else {
            return null;
        }
    }

    /**
     * Validates the certificate chain and write a warning into the logging
     * system if it is not yet valid, or if it will expire soon
     * @param certChain
     */
    public static void validateCertificate(X509Certificate[] certChain) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.WEEK_OF_YEAR, 2);
        Date checkDate = cal.getTime();

        for (X509Certificate cert : certChain) {
            try {
                cert.checkValidity(checkDate);
            } catch (CertificateExpiredException ex) {
                long now = System.currentTimeMillis();
                long expire = cert.getNotAfter().getTime();
                long diff = expire - now;
                long days = diff / (1000 * 60 * 60 * 24);
                String name = null;
                try {
                    name = GrmCAX500Name.parse(cert.getSubjectDN().getName()).toString();
                } catch (GrmCAException ex1) {
                    // Should not happen
                    name = "unknown";
                }
                if (diff < 0) {
                    log.log(Level.WARNING, "cert.expired", name);
                } else {
                    log.log(Level.WARNING, "cert.expires", new Object[]{name, days});
                }
            } catch (CertificateNotYetValidException ex) {
                String name = null;
                try {
                    name = GrmCAX500Name.parse(cert.getSubjectDN().getName()).toString();
                } catch (GrmCAException ex1) {
                    // Should not happen
                    name = "unknown";
                }
                log.log(Level.WARNING, "cert.NotYetValid", new Object[]{name, cert.getNotBefore()});
            }
        }

    }
}

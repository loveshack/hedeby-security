/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.security;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.*;
import com.sun.grid.grm.config.security.HedebySecurity;
import com.sun.grid.grm.security.ca.GrmCAException;
import com.sun.grid.grm.security.ui.GetSecurityCommand;
import com.sun.grid.grm.security.ui.UpdateSecurityCommand;
import com.sun.grid.grm.util.FileUtil;
import java.io.File;
import java.io.IOException;

/**
 Helper class that returns paths and filenames used by security component
 */
public class SecurityPathUtil {

    private static final String BUNDLE = "com.sun.grid.grm.security.security";

    /**
     * Creates a new instance of SecurityPathUtil
     */
    public SecurityPathUtil() {
    }
    
    private final static String SECURITY_DIR = "security";
    /** 
     *  Name of certificate authority directory (ca).
     *  @see #getCaTop
     *  @see #getCaLocalTop
     */
    private static final String CA_DIR = "ca";
    private static final String CA_LOCAL_TOP = "ca_local_top";
    private static final String CA_TOP = "ca_top";
    
    /**
     *  Get the local security directory.
     *  (&lt;local_spool&gt;/security)
     *  @param env ExecutionEnv of a current hedeby system
     *  @return the local security directory
     */
    public static File getLocalSecurityPath(ExecutionEnv env) {
        return new File(env.getLocalSpoolDir(), SECURITY_DIR);
    }
    
    
    /**
     *  Get the path to the daemon keystores
     *
     *  @param env ExecutionEnv of a current hedeby system
     *  @return path to the daemon keystores
     */
    public static File getDaemonKeystorePath(ExecutionEnv env) {
        return new File(getLocalSecurityPath(env), "daemons");
    }
    
    /**
     *  Get the keystore file of a jvm.
     *
     *  @param env ExecutionEnv of a current hedeby system
     *  @param  jvmname name of the jvm (the daemon)
     *  @return the keystore file
     */
    public static File getDaemonKeystoreFile(ExecutionEnv env, String jvmname) {
        return new File(getDaemonKeystorePath(env), jvmname + ".keystore");
    }
    
    /**
     *  Get the keystore password file of a jvm
     *
     *  @param env ExecutionEnv of a current hedeby system
     *  @param  jvmname name of the jvm (the daemon)
     *  @return path to the daemon keystore password file
     */
    public static File getDaemonKeystorePasswordFile(ExecutionEnv env, String jvmname) {
        return new File(getDaemonKeystorePath(env), jvmname + ".password");
    }

    /**
     *  Get the path to the user keystores
     *
     *  @param env ExecutionEnv of a current hedeby system
     *  @return path to the user keystores
     */
    public static File getUserKeyStorePath(ExecutionEnv env) {
        return new File(getLocalSecurityPath(env), "users");
    }
    
    /**
     *   Get the keystore of a user
     *
     *   @param env the execution env
     * @param  username  name of the user
     *   @return the keystore file of the user
     */
    public static File getUserKeystoreFile(ExecutionEnv env, String username) {
        return new File(getUserKeyStorePath(env), username + ".keystore" );
    }
    
    /**
     *  Get the keystore password file of a user
     *
     *  @param env ExecutionEnv of a current hedeby system
     *  @param  username  name of the user
     *  @return the keystore password file of the user
     */
    public static File getKeystorePasswordFileForUser(ExecutionEnv env, String username) {
        return new File(getUserKeyStorePath(env), username + ".password");
    }
        
    /**
     *  Get the shared top directory of the certificate authority
     *  Shouldn't be used
     *  @param env ExecutionEnv of a current hedeby system
     *  @return the shared top directory of the certificate authority
     */
    public static File getCaTop(ExecutionEnv env) {
        File tmp = new File(getLocalSecurityPath(env) , CA_DIR);       
        return new File(tmp, CA_TOP);
    }
    
    /**
     *  Get the file with the CA certificate
     *  @param env the execution env
     *  @return the CA cert file
     */
    public static File getCaCertFile(ExecutionEnv env) {
        return new File(getCaTop(env), SecurityConstants.CACERT_FILE);
    }
    
    /**
     *  Get the file with the CA  CRL certificate
     *  @param env the execution env
     *  @return the CA CRL file
     */
    public static File getCaCRLFile(ExecutionEnv env) {
        return new File(getCaTop(env), SecurityConstants.CRL_FILE);
    }
    
    /**
     *  Get the local top directory of the certificate authority
     *
     *  @param env ExecutionEnv of a current hedeby system
     *  @return the local top directory of the certificate authority
     */
    public static File getCaLocalTop(ExecutionEnv env) {
        File tmp = new File(getLocalSecurityPath(env) , CA_DIR);
        return new File (tmp, CA_LOCAL_TOP);
    }
    
    
    public static boolean updateCerts(ExecutionEnv env, HedebySecurity sec) throws GrmCAException {
        try {
            File cacertFile = getCaCertFile(env);
            String caCert = FileUtil.read(cacertFile);

            File crlFile = getCaCRLFile(env);
            String crl = null;
            if(crlFile.exists()) {
                crl = FileUtil.read(crlFile);
            }

            //when crl, caCert are somehow null there is no update of field
            boolean update = false;
            if ((caCert != null) && (sec.getCaCertificate()==null || !sec.getCaCertificate().equals(caCert))) {
                sec.setCaCertificate(caCert);
                update = true;
            }
            if ((crl!=null)&&(sec.getCaCRL()==null || !sec.getCaCRL().equals(crl))) {
                sec.setCaCRL(crl);
                update = true;
            }
            return update;
        } catch(IOException ex) {
            throw new GrmCAException("grmCerificatefile.error.updateCACert", ex.getLocalizedMessage(), 
                                     BUNDLE, ex);
        }
    }
            
            
    public static void updateCerts(ExecutionEnv env) throws GrmCAException {

        try {
            GetSecurityCommand secCmd = new GetSecurityCommand();
            HedebySecurity sec = env.getCommandService().<HedebySecurity>execute(secCmd).getReturnValue();

            if(updateCerts(env, sec)) {
                UpdateSecurityCommand updCmd = new UpdateSecurityCommand(sec);
                env.getCommandService().execute(updCmd);
            }
        } catch(GrmException ex) {
            throw new GrmCAException("grmCerificatefile.error.updateCACert", ex.getLocalizedMessage(), 
                                     BUNDLE, ex);
        }
    }
    
    
}

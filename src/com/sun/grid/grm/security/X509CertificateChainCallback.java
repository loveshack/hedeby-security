/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.security;

import java.security.cert.X509Certificate;
import javax.security.auth.callback.Callback;

/**
 * Simple callback which retrieves a <code>X509Certificate</code> chain.
 *
 */
public class X509CertificateChainCallback implements Callback {

    private X509Certificate [] chain;

    /**
     * get the <code>X509Certificate</code> chain.
     *
     * @return  the X509Certificate chain
     */
    public X509Certificate[] getChain() {
        return chain;
    }

    /**
     * Set the <code>X509Certificate</code>chain
     * @param chain the <code>X509Certificate</code>chain
     */
    public void setChain(final X509Certificate[] chain) {
        this.chain = chain;
    }

}

/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.security.ca;

import com.sun.grid.grm.ComponentState;
import com.sun.grid.grm.GrmComponent;
import com.sun.grid.grm.GrmSingleton;
import com.sun.grid.grm.GrmRemoteException;
import com.sun.grid.grm.management.Manageable;
import com.sun.grid.grm.event.ComponentEventListener;
import com.sun.grid.grm.util.Hostname;
import java.security.cert.X509Certificate;

/**
 * This interface defines all actions which are possible in the Grm Certificate
 * authority
 */
 @Manageable
public interface GrmCA extends GrmComponent, GrmSingleton, GrmCABase {
    
    public static GrmCA NULL = new GrmCA() {
        
        public X509Certificate getCACertificate() throws GrmRemoteException, GrmCAException {
            throw new UnsupportedOperationException("Null CA implementation");
        }
        
        public X509Certificate getDaemonCertificate(String string) throws GrmCAException {
            throw new UnsupportedOperationException("Null CA implementation");
        }
        
        public X509Certificate getCertificate(String string) throws GrmCAException {
            throw new UnsupportedOperationException("Null CA implementation");
        }
        
        public KeyStoreWrapper createDaemonKeyStore(String string) throws GrmCAException {
            throw new UnsupportedOperationException("Null CA implementation");
        }
        
        public KeyStoreWrapper createKeyStore(String string, char[] c, char[] c0) throws GrmCAException {
            throw new UnsupportedOperationException("Null CA implementation");
        }
        
        public X509Certificate renewDaemonCertificate(String string, int i) throws GrmCAException {
            throw new UnsupportedOperationException("Null CA implementation");
        }
        
        public X509Certificate renewCertificate(String string, int i) throws GrmCAException {
            throw new UnsupportedOperationException("Null CA implementation");
        }
        
        public void renewCaCertificate(int i) throws GrmCAException {
            throw new UnsupportedOperationException("Null CA implementation");
        }
        
        @Override
        public String toString() {
            String retValue;
            
            retValue = super.toString() + ", Null CA implementation";
            return retValue;
        }
        
        public void createUser(String string, String string0, String string1) throws GrmCAException {
            throw new UnsupportedOperationException("Null CA implementation");
        }
        
        public void createDaemon(String string, String string0, String string1) throws GrmCAException {
            throw new UnsupportedOperationException("Null CA implementation");
        }
        
        public String getName() {
            throw new UnsupportedOperationException("Null CA implementation");
        }
        
        public void addComponentEventListener(ComponentEventListener listener) {
            throw new UnsupportedOperationException("Null CA implementation");
        }

        public void removeComponentEventListener(ComponentEventListener listener) {
            throw new UnsupportedOperationException("Null CA implementation");
        }
        
        public ComponentState getState() {
            throw new UnsupportedOperationException("Null CA implementation");
        }
        
        public void reload(boolean forced) {
            throw new UnsupportedOperationException("Null CA implementation");
        }
        
        public void start() {
            throw new UnsupportedOperationException("Null CA implementation");
        }
        
        public void stop(boolean forced) {
            throw new UnsupportedOperationException("Null CA implementation");
        }

        public Hostname getHostname() throws GrmRemoteException {
            throw new UnsupportedOperationException("Null CA implementation");
        }

    };
    
}

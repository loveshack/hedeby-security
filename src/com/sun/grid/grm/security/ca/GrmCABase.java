/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2008 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.security.ca;

import com.sun.grid.grm.GrmRemoteException;
import java.security.cert.X509Certificate;

/**
 * This interface defines the base methods of the <code>GrmCA</code> component.
 * without extending <code>GrmComponent</code>.
 */
public interface GrmCABase {
    /**
     *  Create private key and certificate for a user.
     *
     *  @param username  name of the user
     *  @param group     group of the user
     *  @param email     email address of the user
     * @throws com.sun.grid.grm.GrmRemoteException on any communication error
     * @throws GrmCAException if the creation of the private key or the certificate fails
     */
    public void createUser(String username, String group, String email) throws GrmRemoteException, GrmCAException;

    /**
     * Get the certificate of the Certificate Authority
     * @return the CA certificate
     * @throws com.sun.grid.grm.GrmRemoteException on any communication error
     * @throws com.sun.grid.grm.security.ca.GrmCAException if the certificate does not exist
     */
    public X509Certificate getCACertificate() throws GrmRemoteException, GrmCAException;
    
    /**
     *  Get the X.509 certificate of a user.
     *
     *  @param username  name of the user
     *  @return X.509 certificate
     *  @throws com.sun.grid.grm.GrmRemoteException on any communication error
     *  @throws GrmCAException if the certificate does not exist
     */
    public X509Certificate getCertificate(String username) throws GrmRemoteException, GrmCAException;
    
    /**
     *  Get the X.509 certificate of a daemon.
     *
     *  @param daemon  common name of the daemon
     *  @return X.509 certificate
     *  @throws com.sun.grid.grm.GrmRemoteException on any communication error
     *  @throws GrmCAException if the certificate does not exist
     */
    public X509Certificate getDaemonCertificate(String daemon) throws GrmRemoteException, GrmCAException;
    
    /**
     *  Create a keystore which contains the private key and
     *  certificate of an user.
     * 
     * @param privateKeyPassword 
     * @param username name of the user
     * @param keystorePassword password used for encrypt the keystore
     * @throws GrmCAException if the keystore could not be created
     * @throws com.sun.grid.grm.GrmRemoteException on any communication error
     * @return the keystore
     */
    public KeyStoreWrapper createKeyStore(String username, char [] keystorePassword, char[] privateKeyPassword) throws GrmRemoteException, GrmCAException;
    
    
    /**
     *  Renew the certificate of a user.
     *
     *  @param username  name of the user
     *  @param days      validity of the new certificate in days
     *  @return the renewed certificate
     * @throws com.sun.grid.grm.GrmRemoteException on any communication error
     *  @throws GrmCAException if the certificate can not be renewed
     */
    public X509Certificate renewCertificate(String username, int days) throws GrmRemoteException, GrmCAException;
    
    
    /**
     *  Renew the certificate of a daemon.
     *
     *  @param daemon  name of the daemon
     *  @param days      validity of the new certificate in days
     *  @return the renewed certificate
     *  @throws com.sun.grid.grm.GrmRemoteException on any communication error
     *  @throws GrmCAException if the certificate can not be renewed
     */
    public X509Certificate renewDaemonCertificate(String daemon, int days) throws GrmRemoteException, GrmCAException;

    
    /**
     *  Renew the certificate of the certificate authority
     *
     *  @param days  validity of the new certificate in days
     * @throws com.sun.grid.grm.GrmRemoteException on any communication error
     *  @throws GrmCAException if the certificate can not be renewed
     */
    public void renewCaCertificate(int days) throws GrmRemoteException, GrmCAException;    
    
    /**
     * Create private key and certificate for a SDM daemon.
     *
     * @param daemon name of the daemon
     * @param user   username of the daemon (owner of the process)
     * @param email  email address of the process owner
     * @throws com.sun.grid.grm.GrmRemoteException 
     * @throws com.sun.grid.grm.security.ca.GrmCAException if the create of the daemon failed
     */
    public void createDaemon(String daemon, String user, String email) throws GrmRemoteException, GrmCAException;
    
    
    /**
     * Get the keystore for a daemon.
     *
     * This method can be used be the installation to create keystore for
     * the daemon of a SDM system.
     *
     * @param daemon name of the daemon
     * @throws com.sun.grid.grm.GrmRemoteException on any communication error
     * @throws com.sun.grid.grm.security.ca.GrmCAException if the keystore could not be created
     * @return the keystore of the daemon
     */
    public KeyStoreWrapper createDaemonKeyStore(String daemon) throws GrmRemoteException, GrmCAException;

}

/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.security.ca;

import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.security.FileCallback;
import com.sun.grid.grm.security.SecurityConstants;
import com.sun.grid.grm.security.SecurityPathUtil;
import com.sun.grid.grm.util.I18NManager;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CRLException;
import java.security.cert.CertStore;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.CollectionCertStoreParameters;
import java.security.cert.PKIXBuilderParameters;
import java.security.cert.X509CertSelector;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.CertPathTrustManagerParameters;
import javax.net.ssl.ManagerFactoryParameters;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;

/**
 * The GidCATrustManager validates the certificates of incoming SSL connections.
 * It uses the certificates and CRLs of the gridengine certificate authority.
 *
 * <p>The GrmCATrustManager delegates all request to a PKIX TrustManager. If the CRL
 * file or the ca certificate file of the gridengine CA has been changed the PKIX
 * TrustManager is recreated.</p>
 *
 * <p>The path to the CA certificate file is taken from the default path
 * (<code>SecurityPathUtil#getCaCertFile</code>). If the default file does not 
 * exist the <code>callbackHandler<code> is used to query this file (<code>FileCallback</code>
 * with type <code>SecurityConstants.CACERT_FILE_CALLBACK</code></p>
 *
 * <b>Example:</b>
 *
 * <pre>
 *
 *  ExecutionEnv env = ...
 *
 *  TrustManager [] trustManagers = new TrustManager [] {
 *     new GrmCATrustManager(env);
 *  }
 *
 *  SSLContext ctx = SSLContext.getInstance("SSL");
 *  ctx.init(null, trustManagers, null);
 *
 * </pre>
 *
 * @see com.sun.grid.grm.security.SecurityPathUtil#getCaCertFile
 * @see com.sun.grid.grm.security.FileCallback
 * @see com.sun.grid.grm.security.SecurityConstants#CACERT_FILE_CALLBACK
 */
public class GrmCATrustManager implements X509TrustManager {
    
    /** alias for the ca certificate */
    public static final String CA_ALIAS = "ca";

    private static final String BUNDLE = "com.sun.grid.grm.security.security";
    
    private static final Logger LOGGER = Logger.getLogger(GrmCATrustManager.class.getName(),
            BUNDLE);
    
    private final ExecutionEnv env;
    private final CallbackHandler callbackHandler;
    
    /**
     *
     *  Creates a new instance of GrmCATrustManager.
     *
     * @param env  the execution environment
     */
    public GrmCATrustManager(ExecutionEnv env) {
        this(env, null);
    }
    
    /**
     * Createa a new <code>GrmCATrustManager</code> which
     * can use a callback handler to query the path to
     * the ca cert file.
     *
     * @param env   the execution env
     * @param callbackHandler the callback handler
     */
    public GrmCATrustManager(ExecutionEnv env, CallbackHandler callbackHandler) {
        this.env = env;
        this.callbackHandler = callbackHandler;
    }
    
    
    private static class TrustManagerCache {
        
        private final static Map<ExecutionEnv,TrustManagerCache> tmCache = new HashMap<ExecutionEnv,TrustManagerCache>();
        
        private final ExecutionEnv env;
        private final Object syncObject = new Object();
        
        private long lastUpdate;
        private X509TrustManager trustManager;
        
        public TrustManagerCache(ExecutionEnv env) {
            this.env = env;
        }
        
        public static TrustManagerCache getInstance(ExecutionEnv env) {
            synchronized(tmCache) {
                TrustManagerCache ret = tmCache.get(env);
                if(ret == null) {
                    ret = new TrustManagerCache(env);
                    tmCache.put(env, ret);
                }
                return ret;
            }
        }
        
        public X509TrustManager getTrustManager(CallbackHandler callbackHandler) throws CertificateException {
            synchronized(syncObject) {
                reinit(callbackHandler);
                return trustManager;
            }
        }
        
        
        private void reinit(CallbackHandler callbackHandler) throws CertificateException {
            
            LOGGER.log(Level.FINE, "grmCATrustManager.reinit");
            
            File caCertFile = SecurityPathUtil.getCaCertFile(env);
            if(!caCertFile.exists() && callbackHandler != null) {                
                // Use the callback handler to query the caCertFile
                FileCallback cb = new FileCallback(SecurityConstants.CACERT_FILE_CALLBACK);
                try {
                    callbackHandler.handle(new Callback[] { cb });
                    if(cb.getFile() != null) {
                        caCertFile = cb.getFile();
                    }
                } catch (IOException ex) {
                    // Ignore
                } catch (UnsupportedCallbackException ex) {
                    // Ignore
                }
            }
            File caCrlFile = SecurityPathUtil.getCaCRLFile(env);
            
            if(lastUpdate < caCertFile.lastModified() ||
                    lastUpdate < caCrlFile.lastModified() ) {
                long alastUpdate = System.currentTimeMillis();
                init(caCertFile, caCrlFile);
                this.lastUpdate = alastUpdate;
            }
        }
        
        private void init(File caCertFile, File caCrlFile) throws CertificateException {
            
            try {
                // Initialize an new empty keystore
                KeyStore ks  = KeyStore.getInstance(SecurityConstants.KEYSTORE_ALGORITHM);
                ks.load(null, new char[0]);
                
                LOGGER.log(Level.FINE, "grmCATrustManager.generateCert", caCertFile);
                CertificateFactory certFac = CertificateFactory.getInstance(SecurityConstants.CERTIFICATE_FACTORY);
                
                InputStream in = new FileInputStream(caCertFile);
                
                try {
                    
                    X509Certificate cert =
                            (X509Certificate)certFac.generateCertificate(in);
                    ks.setCertificateEntry(CA_ALIAS, cert);
                } finally {
                    try {
                        in.close();
                    } catch(IOException ioe) {
                        // Ignore
                    }
                }
                
                PKIXBuilderParameters pkixParams =
                        new PKIXBuilderParameters(ks, new X509CertSelector());
                
                
                if (caCrlFile.exists()) {
                    LOGGER.log(Level.FINE, "grmCATrustManager.generateCrl", caCrlFile);
                    in = new FileInputStream(caCrlFile);
                    
                    try {
                        Collection crls = certFac.generateCRLs(in);
                        
                        CollectionCertStoreParameters certStoreParams =
                                new CollectionCertStoreParameters(crls);
                        CertStore certStore =
                                CertStore.getInstance("Collection", certStoreParams);
                        
                        pkixParams.setRevocationEnabled(true);
                        pkixParams.addCertStore(certStore);
                    } finally {
                        try {
                            in.close();
                        } catch(IOException ioe) {
                            // Ignore
                        }
                    }
                } else {
                    // crl file does not exists, disable revocation
                    pkixParams.setRevocationEnabled(false);
                }
                
                // Wrap them as trust manager parameters
                ManagerFactoryParameters trustParams =
                        new CertPathTrustManagerParameters(pkixParams);
                TrustManagerFactory fac = TrustManagerFactory.getInstance(SecurityConstants.TRUSTMANAGER_ALGORITHM);
                
                fac.init(trustParams);
                
                trustManager = null;
                TrustManager [] trustManagers = fac.getTrustManagers();
                for (int i = 0; i < trustManagers.length; i++) {
                    if (trustManagers[i] instanceof X509TrustManager) {
                        trustManager = (X509TrustManager)trustManagers[i];
                        break;
                    }
                }
                
                if (trustManager == null) {
                    // This should never happen since we handle only X509 certificates
                    throw new AssertionError("Found no X509TrustManager");
                }
                
                
                
            } catch (NoSuchAlgorithmException ex) {
                CertificateException cae =
                        new CertificateException(
                        I18NManager.formatMessage("grmCATrustManager.error.init",
                        SecurityConstants.TRUSTMANAGER_ALGORITHM,
                        ex.getLocalizedMessage()),
                        ex);
                LOGGER.throwing("SgeCATrustManager", "init", cae);
                throw cae;
            } catch (InvalidAlgorithmParameterException ex) {
                CertificateException cae =
                        new CertificateException(
                        I18NManager.formatMessage("grmCATrustManager.error.init",
                        SecurityConstants.TRUSTMANAGER_ALGORITHM,
                        ex.getLocalizedMessage()),
                        ex);
                LOGGER.throwing("SgeCATrustManager", "init", cae);
                throw cae;
            } catch(KeyStoreException ex) {
                CertificateException cae =
                        new CertificateException(
                        I18NManager.formatMessage("grmCATrustManager.error.init",
                        SecurityConstants.TRUSTMANAGER_ALGORITHM,
                        ex.getLocalizedMessage()),
                        ex);
                LOGGER.throwing("SgeCATrustManager", "init", cae);
                throw cae;
            } catch(IOException ex) {
                CertificateException cae =
                        new CertificateException(
                        I18NManager.formatMessage("grmCATrustManager.error.init",
                        SecurityConstants.TRUSTMANAGER_ALGORITHM,
                        ex.getLocalizedMessage()),
                        ex);
                LOGGER.throwing("SgeCATrustManager", "init", cae);
                throw cae;
            } catch(CRLException ex) {
                CertificateException cae =
                        new CertificateException(
                        I18NManager.formatMessage("grmCATrustManager.error.crl",
                        ex.getLocalizedMessage()),
                        ex);
                LOGGER.throwing("SgeCATrustManager", "init", ex);
                throw cae;
            }
        }
    }
    
    /**
     * Given the partial or complete certificate chain provided by the
     * peer, build a certificate path to a trusted root and return if
     * it can be validated and is trusted for client SSL
     * authentication based on the authentication type.
     *
     * The authentication type is determined by the actual certificate
     * used. For instance, if RSAPublicKey is used, the authType
     * should be "RSA". Checking is case-sensitive.
     *
     * @param chain the peer certificate chain
     * @param authType the authentication type based on the client certificate
     * @throws IllegalArgumentException if null or zero-length chain
     *         is passed in for the chain parameter or if null or zero-length
     *         string is passed in for the  authType parameter
     * @throws CertificateException if the certificate chain is not trusted
     *         by this TrustManager.
     */
    public void checkClientTrusted(X509Certificate[] chain, String authType)
    throws CertificateException {
        
        X509TrustManager trustManager = TrustManagerCache.getInstance(env).getTrustManager(callbackHandler);
        if (trustManager == null) {
            throw new CertificateException(
                    I18NManager.formatMessage("grmCATrustManager.error.notInitialized", BUNDLE));
        }
        
        trustManager.checkClientTrusted(chain, authType);
    }
    
    /**
     * Given the partial or complete certificate chain provided by the
     * peer, build a certificate path to a trusted root and return if
     * it can be validated and is trusted for server SSL
     * authentication based on the authentication type.
     *
     * The authentication type is the key exchange algorithm portion
     * of the cipher suites represented as a String, such as "RSA",
     * "DHE_DSS". Note: for some exportable cipher suites, the key
     * exchange algorithm is determined at run time during the
     * handshake. For instance, for TLS_RSA_EXPORT_WITH_RC4_40_MD5,
     * the authType should be RSA_EXPORT when an ephemeral RSA key is
     * used for the key exchange, and RSA when the key from the server
     * certificate is used. Checking is case-sensitive.
     *
     * @param chain the peer certificate chain
     * @param authType the key exchange algorithm used
     * @throws IllegalArgumentException if null or zero-length chain
     *         is passed in for the chain parameter or if null or zero-length
     *         string is passed in for the  authType parameter
     * @throws CertificateException if the certificate chain is not trusted
     *         by this TrustManager.
     */
    public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        X509TrustManager trustManager = TrustManagerCache.getInstance(env).getTrustManager(callbackHandler);
        if (trustManager == null) {
            throw new CertificateException(
                    I18NManager.formatMessage("grmCATrustManager.error.notInitialized", BUNDLE));
        }
        
        trustManager.checkServerTrusted(chain, authType);
    }
    
    /**
     * Return an array of certificate authority certificates
     * which are trusted for authenticating peers.
     *
     * @return a non-null (possibly empty) array of acceptable
     *		CA issuer certificates.
     */
    public X509Certificate[] getAcceptedIssuers() {
        try {
            X509TrustManager trustManager = TrustManagerCache.getInstance(env).getTrustManager(callbackHandler);
            if (trustManager == null) {
                return new X509Certificate[0];
            } else {
                return trustManager.getAcceptedIssuers();
            }
        } catch(CertificateException ex) {
            return new X509Certificate[0];
        }
    }
    
}

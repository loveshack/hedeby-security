/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.security.ca;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.security.KeyStore;
import javax.management.MBeanException;

/**
 *  Simple wrapper class which make a keystore 
 *  <code>Serializable</code>
 */
public class KeyStoreWrapper implements Serializable {
    
    private transient KeyStore ks;
    private byte [] content;
    private String  type;
    
    /**
     * Creates a new instance of KeyStoreWrapper.
     * @param ks 
     */
    public KeyStoreWrapper(KeyStore ks) {
        this.ks = ks;
    }
    
    /**
     *  Creates a new instance of KeyStoreWrapper.
     */
    public KeyStoreWrapper() {
    }
    
    /**
     * Get the real keystore.
     *
     * @return the real keystore
     */
    public KeyStore getKeyStore() {
        return ks;
    }
    
    private void writeObject(ObjectOutputStream out) throws IOException {
        
        if(ks != null) {
            try {
                ByteArrayOutputStream bout = new  ByteArrayOutputStream();
                ks.store(bout, new char[0]);
                bout.close();
                content = bout.toByteArray();
                type = ks.getType();
            } catch (Exception e) {
                IOException ex = new IOException("Can not serialize keystore");
                ex.initCause(e);
                throw ex;
            }
        }
        out.defaultWriteObject();
    }
    
    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        
        in.defaultReadObject();
        if(content != null) {
            try {
                ks = KeyStore.getInstance(type);
                ks.load(new ByteArrayInputStream(content), new char[0]);
            } catch(Exception e) {
                IOException ex = new IOException("Can not deserialize keystore");
                ex.initCause(e);
                throw ex;
            }
        }
    }
    
    
    
}

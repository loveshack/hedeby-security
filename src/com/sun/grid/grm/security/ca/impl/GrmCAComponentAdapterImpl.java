/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2008 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.security.ca.impl;

import com.sun.grid.grm.impl.AbstractComponentAdapter;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.config.security.CAComponentConfig;
import com.sun.grid.grm.GrmRemoteException;
import com.sun.grid.grm.security.SecurityPathUtil;
import com.sun.grid.grm.security.ca.GrmCA;
import com.sun.grid.grm.security.ca.GrmCAException;
import com.sun.grid.grm.security.ca.KeyStoreWrapper;
import java.io.File;
import java.security.cert.X509Certificate;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Default <code>ComponentAdapter</code> implementation of the ceritificate authority of 
 * a Hedeby system.
 * 
 * <p>The <code>GrmCAComponentContainer</code> class delegates all method calls defined in
 *    <code>GrmCABase</code> to this <code>ComponentAdapter</code>.
 * 
 * <h3>Note</h3>
 * 
 * <p>Lifetime of all method calls which are modifing the CA can exceed the lifetime of the CA component.
 *    This behavior is intended, because the CA implementation supports no transaction concept. Running 
 *    actions must not be interrupted.</p>
 */
public class GrmCAComponentAdapterImpl extends AbstractComponentAdapter<ExecutorService,CAComponentConfig> 
        implements GrmCAComponentAdapter {
    
    private static final String BUNDLE = "com.sun.grid.grm.security.security";
    private final Logger log = Logger.getLogger(GrmCAComponentAdapterImpl.class.getName(), BUNDLE);

    /**
     *  Use a AtomicReference here to make this class thread safe.
     */
    private final AtomicReference<GrmCAImpl> grmCAImpl = new AtomicReference<GrmCAImpl>();
    
    /**
     * Create a new instanceof the <code>GrmCAComponentAdapterImpl</code>.
     * @param comp  The owning component
     */
    public GrmCAComponentAdapterImpl(GrmCAComponentContainer comp) {
        super(comp);
    }
    
    private GrmCA getCA() throws GrmCAException {
        log.entering(getClass().getName(), "getCA");
        GrmCAImpl ret = grmCAImpl.get();
        if(ret == null) {
            GrmCAException ex = new GrmCAException("grmCAServiceImpl.ex.notActive", BUNDLE);
            log.throwing(getClass().getName(), "getCA", ex);
        }
        log.entering(getClass().getName(), "getCA", ret.getCA());
        return ret.getCA();
    }
    
    /**
     * Start the CA component
     * 
     * @param config  the new configuration of the CA component
     * @throws com.sun.grid.grm.GrmException if the configuration is not valid
     */
    public void startComponent(CAComponentConfig config) throws GrmException {
        log.entering(getClass().getName(), "startComponent", config);
        
        GrmCAServiceConfigHelper helper = new GrmCAServiceConfigHelper(getExecutionEnv(), config);

        File tmpDir = helper.getTmpDir();
        if (tmpDir != null && !tmpDir.exists()) {
            if (!tmpDir.mkdir()) {
                GrmCAException ex = new GrmCAException("grmCAServiceImpl.error.tmpDir", BUNDLE, tmpDir);
                log.throwing(getClass().getName(), "startComponent", ex);
                throw ex;
            }
        }

        GrmCAImpl tmpImpl = new GrmCAImpl(getExecutionEnv(), helper);
        grmCAImpl.set(tmpImpl);
        
        log.exiting(getClass().getName(), "startComponent");
    }
    
    /**
     * Stop the CA component.
     * @param forced has not effect
     */
    public void stopComponent(boolean forced) {
        log.entering(getClass().getName(), "stopComponent", forced);
        grmCAImpl.set(null);
        log.exiting(getClass().getName(), "stopComponent");
    }

    /**
     * Reload the CA component.
     * @param config  the new configuration
     * @param forced  has no effect
     * @throws com.sun.grid.grm.GrmException
     */
    public void reloadComponent(CAComponentConfig config, boolean forced) throws GrmException {
        if(log.isLoggable(Level.FINER)) {
            log.entering(getClass().getName(), "reloadComponent", new Object [] {config, forced});
        }
        startComponent(config);
        log.exiting(getClass().getName(), "reloadComponent");
    }
    
    
    /**
     *  Create private key and certificate for a user.
     *
     *  @param username  name of the user
     *  @param group     group of the user
     *  @param email     email address of the user
     *  @throws GrmCAException if the creation of the private key or the certificate fails
     *  @throws com.sun.grid.grm.GrmRemoteException on any communication error
     */
    public void createUser(final String username, final String group, final String email) throws GrmCAException, GrmRemoteException {
        if(log.isLoggable(Level.FINER)) {
            log.entering(getClass().getName(), "createUser", new Object [] {username, group, email});
        }
        getCA().createUser(username, group, email);
        
        log.exiting(getClass().getName(), "createUser");
    }
    
    /**
     * Create private key and certificate for a grm daemon.
     *
     * @param daemon name of the daemon
     * @param user   username of the daemon (owner of the process)
     * @param email  email address of the process owner
     * @throws com.sun.grid.grm.GrmRemoteException on any communication error
     */
    public void createDaemon(final String daemon, final String user, final String email) throws GrmCAException, GrmRemoteException {
        if(log.isLoggable(Level.FINER)) {
            log.entering(getClass().getName(), "createDaemon", new Object [] {daemon, user, email});
        }
        getCA().createDaemon(daemon, user, email);
        
        log.exiting(getClass().getName(), "createDaemon");
    }
    
    
    /**
     *  Get the X.509 certificate of a user.
     *
     *  @param username  name of the user
     *  @return X.509 certificate
     *  @throws GrmCAException if the certificate does not exist
     *  @throws com.sun.grid.grm.GrmRemoteException on any communication error
     */
    public X509Certificate getCertificate(final String username) throws GrmCAException, GrmRemoteException {
        log.entering(getClass().getName(), "getCertificate", username);
        
        X509Certificate ret = getCA().getCertificate(username);
        
        log.exiting(getClass().getName(), "getCertificate", ret);
        return ret;
    }
    
    /**
     *  Get the X.509 certificate of a daemon.
     *
     *  @param daemon  name of the daemon
     *  @return X.509 certificate
     *  @throws GrmCAException if the certificate does not exist
     *  @throws com.sun.grid.grm.GrmRemoteException on any communication error
     */
    public X509Certificate getDaemonCertificate(final String daemon) throws GrmCAException, GrmRemoteException {
        log.entering(getClass().getName(), "getDaemonCertificate", daemon);
        
        X509Certificate ret = getCA().getDaemonCertificate(daemon);
        
        log.exiting(getClass().getName(), "getDaemonCertificate", ret);
        return ret;
    }
    
    /**
     * Get the certificate of the Certificate Authority
     * @return the CA certificate
     * @throws com.sun.grid.grm.GrmRemoteException on any communication error
     * @throws com.sun.grid.grm.security.ca.GrmCAException if the certificate does not exist
     */
    public X509Certificate getCACertificate() throws GrmRemoteException, GrmCAException {
        log.entering(getClass().getName(), "getCACertificate");
        
        X509Certificate ret = getCA().getCACertificate();
        
        log.exiting(getClass().getName(), "getCACertificate", ret);
        return ret;
    }

    
    /**
     *  Create a keystore which contains the private key and
     *  certificate of an user.
     *
     *  @return the keystore of the user
     *  @param privateKeyPassword password for the private key
     *  @param username name of the user
     *  @param keystorePassword password used for encrypt the keystore
     *  @throws GrmCAException if the keystore could not be created
     *  @throws com.sun.grid.grm.GrmRemoteException on any communication error
     */
    public KeyStoreWrapper createKeyStore(final String username, final char[] keystorePassword, final char[] privateKeyPassword) throws GrmCAException, GrmRemoteException {
        // Do not write password into the log system
        log.entering(getClass().getName(), "createKeyStore", username);
        
        KeyStoreWrapper ret = getCA().createKeyStore(username, keystorePassword, privateKeyPassword);
        
        // Do not write the keystore into the log system
        log.exiting(getClass().getName(), "createKeyStore");
        return ret;
    }
    
    /**
     * Get the keystore for a daemon.
     *
     * This method can be used be the installation to create keystore for
     * the daemon of a grm system.
     *
     * @param daemon name of the daemon
     * @throws GrmCAException if the daemon keystore could not be created
     * @throws com.sun.grid.grm.GrmRemoteException on any communication error
     * @return the keystore of the daemon
     */
    public KeyStoreWrapper createDaemonKeyStore(final String daemon) throws GrmCAException, GrmRemoteException {
        log.entering(getClass().getName(), "createDaemonKeyStore", daemon);
        
        KeyStoreWrapper ret = getCA().createDaemonKeyStore(daemon);
        
        log.exiting(getClass().getName(), "createDaemonKeyStore", daemon);
        return ret;
    }
    
    
    /**
     *  Renew the certificate of a user.
     *
     *  @param username  name of the user
     *  @param days      validity of the new certificate in days
     *  @return the renewed certificate
     *  @throws GrmCAException if the certificate can not be renewed
     *  @throws com.sun.grid.grm.GrmRemoteException on any communication error
     */
    public X509Certificate renewCertificate(final String username, final int days) throws GrmCAException, GrmRemoteException {
        if (log.isLoggable(Level.FINER)) {
            log.entering(getClass().getName(), "renewCertificate", new Object [] { username, days});
        }
        
        X509Certificate ret = getCA().renewCertificate(username, days);
        
        log.exiting(getClass().getName(), "renewCertificate", ret);
        return ret;
    }
    
    /**
     *  Renew the certificate of a daemon.
     *
     *  @param daemon  name of the daemon
     *  @param days    validity of the new certificate in days
     *  @return the renewed certificate
     *  @throws GrmCAException if the certificate can not be renewed
     *  @throws com.sun.grid.grm.GrmRemoteException on any communication error
     */
    public X509Certificate renewDaemonCertificate(final String daemon, final int days) throws GrmCAException, GrmRemoteException {
        if (log.isLoggable(Level.FINER)) {
            log.entering(getClass().getName(), "renewDaemonCertificate", new Object [] { daemon, days});
        }
         X509Certificate ret = getCA().renewDaemonCertificate(daemon, days);
         
        log.exiting(getClass().getName(), "renewDaemonCertificate", ret);
        return ret;
    }
    
    /**
     *  Renew the certificate of the ca
     *
     *  @param days    validity of the new certificate in days
     *  @throws GrmCAException if the certificate can not be renewed
     *  @throws com.sun.grid.grm.GrmRemoteException on any communication error
     */
    public void renewCaCertificate(final int days) throws GrmCAException, GrmRemoteException {
        log.entering(getClass().getName(), "renewCaCertificate", days);
        getCA().renewCaCertificate(days);
        SecurityPathUtil.updateCerts(getExecutionEnv());
        log.exiting(getClass().getName(), "renewCaCertificate");
    }
    
}

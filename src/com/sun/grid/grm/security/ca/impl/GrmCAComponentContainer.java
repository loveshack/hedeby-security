/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.security.ca.impl;

import com.sun.grid.grm.ComponentNotActiveException;
import com.sun.grid.grm.impl.AbstractComponentContainer;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.PathUtil;
import com.sun.grid.grm.config.security.CAComponentConfig;
import com.sun.grid.grm.GrmRemoteException;
import com.sun.grid.grm.impl.AbstractDynamicComponentAdapterFactory;
import com.sun.grid.grm.impl.ComponentExecutors;
import com.sun.grid.grm.security.ca.GrmCA;
import com.sun.grid.grm.security.ca.GrmCAException;
import com.sun.grid.grm.security.ca.KeyStoreWrapper;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.cert.X509Certificate;
import java.util.concurrent.ExecutorService;

/**
 * <code>ComponentContainer</code> for the CA component.
 * 
 * The classpath for the <code>ComponentAdapter</code> must contain
 * <ul>
 *    <li>juti.jar (derived from the path to the sge_ca script)</li>
 *    <li>sdm-service-impl.jar from hedeby dist lib directory</li> 
 * </ul>
 */
public class GrmCAComponentContainer
        extends AbstractComponentContainer<GrmCAComponentAdapter, ExecutorService, CAComponentConfig>
        implements GrmCA {

    private static final String BUNDLE = "com.sun.grid.grm.security.security";

    /**
     * Creates a new instance of GrmCAComponentContainer
     *
     * @param env  the execution env
     * @param name name of the service
     * @throws com.sun.grid.grm.GrmException if the delegate can not be loaded
     */
    public GrmCAComponentContainer(ExecutionEnv env, String name) throws GrmException {
        super(env, name,
              ComponentExecutors.<GrmCAComponentContainer, CAComponentConfig>newSingleThreadedExecutorServiceFactory(),
              new MyComponentAdapterFactory());
    }

    /**
     * Get the classpath for the CA component.
     * 
     * @param config the configuration of the CA component
     * @return the necessary classpath for the CA component
     * @throws com.sun.grid.grm.GrmException if juti.jar has not been found in the sge_root directory where sge_ca script comes from
     *                     or if sdm-security-impl.jar has not been found in the distribution directory
     */
    private static class MyComponentAdapterFactory
            extends AbstractDynamicComponentAdapterFactory<GrmCAComponentContainer, GrmCAComponentAdapter, ExecutorService, CAComponentConfig> {

        @Override
        protected URL[] getClasspath(GrmCAComponentContainer component, CAComponentConfig config) throws GrmException {
            File sgeCAScript = new File(config.getSgeCaScript());
            File sgeRoot = sgeCAScript.getAbsoluteFile().getParentFile().getParentFile().getParentFile();
            File jutiJar = new File(sgeRoot, "lib" + File.separatorChar + "juti.jar");

            if (!jutiJar.exists()) {
                throw new GrmException("GrmCAComponentContainer.ex.jutiNotFound", BUNDLE, jutiJar, sgeCAScript);
            }

            File serviceImplJar = new File(PathUtil.getDistLibPath(component.getExecutionEnv()), "sdm-security-impl.jar");

            if (!serviceImplJar.exists()) {
                throw new GrmException("GrmCAComponentContainer.ex.implJarNotFound", BUNDLE, serviceImplJar);
            }

            URL urls[];
            try {

                urls = new URL[]{
                            serviceImplJar.toURI().toURL(),
                            jutiJar.toURI().toURL()
                        };
            } catch (MalformedURLException ex) {
                throw new IllegalStateException("File.toURL throwed exception", ex);
            }
            return urls;
        }

        /**
         * Get the classname of the adapter.
         * 
         * @param component  the owning component
         * @param config     the configuration of the component
         * @return  the classname of the adapter
         */
        @Override
        protected String getAdapterClassname(GrmCAComponentContainer component, CAComponentConfig config) {
            return getClass().getPackage().getName() + ".GrmCAComponentAdapterImpl";
        }
    }

    /**
     * Create the private key and the X509 certificate of a user and
     * sign this certificate for the CA certificate.
     * 
     * @param username  the username
     * @param group     the group of the user
     * @param email     the email address of the user
     * @throws com.sun.grid.grm.security.ca.GrmCAException thrown by the delegate
     * @throws com.sun.grid.grm.GrmRemoteException on any communication problem (should not happen since this method
     *                    makes no remote call)
     */
    public void createUser(String username, String group, String email) throws GrmCAException, GrmRemoteException {
        try {
            getAdapter().createUser(username, group, email);
        } catch (ComponentNotActiveException ex) {
            throw new GrmCAException(ex.getLocalizedMessage(), ex);
        }
    }

    /**
     * Get a user certificate
     * @param username the name of the user
     * @return the user certificate
     * @throws com.sun.grid.grm.security.ca.GrmCAException if the certificate of the user has not been found
     * @throws com.sun.grid.grm.GrmRemoteException on any communication problem (should not happen since this method
     *                    makes no remote call)
     */
    public X509Certificate getCertificate(final String username) throws GrmCAException, GrmRemoteException {
        try {
            return getAdapter().getCertificate(username);
        } catch (ComponentNotActiveException ex) {
            throw new GrmCAException(ex.getLocalizedMessage(), ex);
        }
    }

    /**
     * Get a daemon certificate
     * @param daemon the name of the daemon
     * @return the daemon certificate
     * @throws com.sun.grid.grm.security.ca.GrmCAException if the certificate of the daemon has not been found
     * @throws com.sun.grid.grm.GrmRemoteException on any communication problem (should not happen since this method
     *                    makes no remote call)
     */
    public X509Certificate getDaemonCertificate(String daemon) throws GrmCAException, GrmRemoteException {
        try {
            return getAdapter().getDaemonCertificate(daemon);
        } catch (ComponentNotActiveException ex) {
            throw new GrmCAException(ex.getLocalizedMessage(), ex);
        }
    }

    /**
     * Create keystore of an user.
     * 
     * @param username   the name of the user
     * @param keystorePassword  password for the keystore
     * @param privateKeyPassword password for encrypting the private key in the keystore
     * @return the keystore
     * @throws com.sun.grid.grm.security.ca.GrmCAException if the keystore could not be created
     * @throws com.sun.grid.grm.GrmRemoteException on any communication problem (should not happen since this method
     *                    makes no remote call)
     */
    public KeyStoreWrapper createKeyStore(String username, char[] keystorePassword, char[] privateKeyPassword) throws GrmCAException, GrmRemoteException {
        try {
            return getAdapter().createKeyStore(username, keystorePassword, privateKeyPassword);
        } catch (ComponentNotActiveException ex) {
            throw new GrmCAException(ex.getLocalizedMessage(), ex);
        }
    }

    /**
     * Renew the certificate of an user
     * @param username  the user name
     * @param days number of day (-1 if the certificate should be revoked)
     * @return  the renewed certificate of the user
     * @throws com.sun.grid.grm.security.ca.GrmCAException if the certificate could not be renewed
     * @throws com.sun.grid.grm.GrmRemoteException on any communication problem (should not happen since this method
     *                    makes no remote call)
     */
    public X509Certificate renewCertificate(String username, int days) throws GrmCAException, GrmRemoteException {
        try {
            return getAdapter().renewCertificate(username, days);
        } catch (ComponentNotActiveException ex) {
            throw new GrmCAException(ex.getLocalizedMessage(), ex);
        }
    }

    /**
     * Renew the certificate of a daemon
     * @param daemon  the user daemon
     * @param days number of days (-1 if the certificate should be revoked)
     * @return  the renewed certificate of the daemon
     * @throws com.sun.grid.grm.security.ca.GrmCAException if the certificate could not be renewed
     * @throws com.sun.grid.grm.GrmRemoteException on any communication problem (should not happen since this method
     *                    makes no remote call)
     */
    public X509Certificate renewDaemonCertificate(String daemon, int days) throws GrmCAException, GrmRemoteException {
        try {
            return getAdapter().renewDaemonCertificate(daemon, days);
        } catch (ComponentNotActiveException ex) {
            throw new GrmCAException(ex.getLocalizedMessage(), ex);
        }
    }

    /**
     * Renew the CA certificate.
     * @param days number of days
     * @throws com.sun.grid.grm.security.ca.GrmCAException if the CA certificate could not be renewed
     * @throws com.sun.grid.grm.GrmRemoteException on any communication problem (should not happen since this method
     *                    makes no remote call)
     */
    public void renewCaCertificate(int days) throws GrmCAException, GrmRemoteException {
        try {
            getAdapter().renewCaCertificate(days);
        } catch (ComponentNotActiveException ex) {
            throw new GrmCAException(ex.getLocalizedMessage(), ex);
        }
    }

    /**
     * Create the certificate for a daemon.
     * @param daemon  name of the daemon
     * @param user    name of the user running the daemon (process owner)
     * @param email   email address of the user
     * @throws com.sun.grid.grm.security.ca.GrmCAException if the daemon certificate could not be created
     * @throws com.sun.grid.grm.GrmRemoteException on any communication problem (should not happen since this method
     *                    makes no remote call)
     */
    public void createDaemon(String daemon, String user, String email) throws GrmCAException, GrmRemoteException {
        try {
            getAdapter().createDaemon(daemon, user, email);
        } catch (ComponentNotActiveException ex) {
            throw new GrmCAException(ex.getLocalizedMessage(), ex);
        }
    }

    /**
     * Create a keystore for a daemon
     * @param daemon name of the daemon
     * @return the keystore of the daemon
     * @throws com.sun.grid.grm.security.ca.GrmCAException if the keystore could not be created
     * @throws com.sun.grid.grm.GrmRemoteException on any communication problem (should not happen since this method
     *                    makes no remote call)
     */
    public KeyStoreWrapper createDaemonKeyStore(String daemon) throws GrmCAException, GrmRemoteException {
        try {
            return getAdapter().createDaemonKeyStore(daemon);
        } catch (ComponentNotActiveException ex) {
            throw new GrmCAException(ex.getLocalizedMessage(), ex);
        }
    }

    /**
     * Get the CA certificate.
     * @return the CA certificate
     * @throws com.sun.grid.grm.security.ca.GrmCAException if the CA certificate has not been found
     * @throws com.sun.grid.grm.GrmRemoteException on any communication problem (should not happen since this method
     *                    makes no remote call)
     */
    public X509Certificate getCACertificate() throws GrmRemoteException, GrmCAException {
        try {
            return getAdapter().getCACertificate();
        } catch (ComponentNotActiveException ex) {
            throw new GrmCAException(ex.getLocalizedMessage(), ex);
        }
    }
}

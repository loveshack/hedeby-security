/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.security.ca.impl;

import com.sun.grid.ca.GridCA;
import com.sun.grid.ca.GridCAException;
import com.sun.grid.ca.GridCAFactory;
import com.sun.grid.ca.InitCAParameters;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.security.SecurityPathUtil;
import com.sun.grid.grm.security.ca.GrmCA;
import com.sun.grid.grm.security.ca.GrmCAException;
import com.sun.grid.grm.security.ca.KeyStoreWrapper;
import com.sun.grid.grm.util.I18NManager;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.security.KeyStore;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Simple implementation of the <code>GrmCA</code> interface.
 *
 * Delegates the method calls for <code>GrmCA</code> to <code>GridCA</code>.
 */
public class GrmCAImpl implements InvocationHandler {

    private static final String BUNDLE = "com.sun.grid.grm.security.security";
    private final static Logger log = Logger.getLogger(GrmCAImpl.class.getName(), BUNDLE);
    
    private final GridCA gridCa;
    private final GrmCA ca;
    private final ExecutionEnv env;
    
    /**
     * Creates a new instance of GrmCAImpl
     * @param env the execution env
     * @param helper configuration helper
     * @throws com.sun.grid.grm.security.ca.GrmCAException if the configuration is invalid
     */
    public GrmCAImpl(ExecutionEnv env, GrmCAServiceConfigHelper helper) throws GrmCAException {
        try {
            this.env = env;
            this.gridCa = GridCAFactory.newInstance(helper.getGridCAConfig());
            this.ca = (GrmCA)Proxy.newProxyInstance(getClass().getClassLoader(),
                                                    new Class[] { GrmCA.class },
                                                    this);
        } catch(GridCAException ex) {
            /* 
             * Do not path root cause to client, because client cannot load
             * GridCAException
             */ 
            throw new GrmCAException(ex.getLocalizedMessage());
        }
    }
    
    /**
     * Get the GrmCA component
     * @return the GrmCA component
     */
    public GrmCA getCA() {
        return ca;
    }
    
    /**
     *  Initialize the Hedeby CA
     * 
     * @param  params  parmeters for the CA
     * @throws com.sun.grid.grm.security.ca.GrmCAException 
     */
    public void init(InitCAParameters params) throws GrmCAException {        
        try {
            gridCa.init(params);
        } catch (GridCAException ex) {
            /* 
             * Do not path root cause to client, because client cannot load
             * GridCAException
             */ 
            throw new GrmCAException(ex.getLocalizedMessage());
        }
    }
    
    /**
     * Get the certificate of the Certificate Authority
     * @return the CA certificate
     * @throws com.sun.grid.grm.security.ca.GrmCAException if the certificate does not exist
     */
    private X509Certificate getCACertificate() throws GrmCAException {
        FileInputStream in = null;
        try {
            File certFile = SecurityPathUtil.getCaCertFile(env);
            in = new FileInputStream(certFile);
            try {
                return (X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(in);
            } finally {
                in.close();
            }
        } catch (IOException ex) {
           throw new GrmCAException("GrmCAImpl.ex.caCert", ex, BUNDLE, ex.getLocalizedMessage());
        } catch (CertificateException ex) {
           throw new GrmCAException("GrmCAImpl.ex.caCert", ex, BUNDLE, ex.getLocalizedMessage());
        }
    }

    /**
     * Invoke a method on the grmCA.
     *
     * @param proxy   the proxy object
     * @param method  the method
     * @param args    params for the method call
     * @throws com.sun.grid.grm.security.ca.GrmCAException 
     * @return the result of the method
     */
    public synchronized Object invoke(Object proxy, Method method, Object[] args) throws GrmCAException {
        try {
            if (method.getName().equals("getCACertificate")) {
                return getCACertificate();
            } else {
                // We have to map the method from GrmCA to GridCA, both classes implements
                // the same methods, but have different class loaderes. So GrmCA can not
                // extends GridCA.
                Method realMethod = GridCA.class.getMethod(method.getName(), method.getParameterTypes());

                Object ret = realMethod.invoke(gridCa, args);

                if (method.getReturnType().equals(KeyStoreWrapper.class)) {
                    return new KeyStoreWrapper((KeyStore) ret);
                } else {
                    return ret;
                }
            }
        } catch(IllegalAccessException ex) {
            // Can not happen, GridCAImpl implements all methods
            throw new IllegalStateException(ex.getLocalizedMessage(), ex);
        } catch(NoSuchMethodException ex) {
            // Can not happen. GridCAImpl implements all methods
            throw new UnsupportedOperationException("Method " + method.getName() + " is not supported by Dummy Grm CA");
        } catch(InvocationTargetException ex) {
            if(log.isLoggable(Level.WARNING)) {
                log.log(Level.WARNING, 
                        I18NManager.formatMessage("grmCAImpl.error.method", BUNDLE, method.getName(), ex.getTargetException().getLocalizedMessage()),
                        ex.getTargetException());
            }
            if(ex.getTargetException() instanceof GridCAException) {
                // We can not add a GridCAException as root cause, clients
                // does not known anything about this class
                throw new GrmCAException(ex.getTargetException().getLocalizedMessage());
            } else {
                throw new GrmCAException(ex.getTargetException().getLocalizedMessage(), ex.getTargetException());
            }
            
        }
    }

}

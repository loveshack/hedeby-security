/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.security.ca.impl;

import com.sun.grid.ca.GridCAConfiguration;
import com.sun.grid.ca.GridCAException;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.PathUtil;
import com.sun.grid.grm.config.ConfigurationException;
import com.sun.grid.grm.config.security.CAComponentConfig;
import com.sun.grid.grm.security.SecurityPathUtil;
import java.io.File;
import java.util.Calendar;

/** Helper class that hides the GridCA config object
 *
 */

public class GrmCAServiceConfigHelper {

    private static final String BUNDLE = "com.sun.grid.grm.security.security";
    
    private final GridCAConfiguration gridCAConfig = new GridCAConfiguration();
    private CAComponentConfig config;
    private ExecutionEnv env;
    private File sgeRoot;

    /**
     * Creates a new instance of GrmCAImplConfiguration
     * @param env  the execution env
     * @param config the configuration of the CA component
     * @throws com.sun.grid.grm.GrmException if the system configuration is invalid
     */
    public GrmCAServiceConfigHelper(ExecutionEnv env, CAComponentConfig config) throws GrmException {

        this.env = env;
        this.config = config;
        init();
    }

    /**
     * Set a new configuration
     * @param config the new configuration
     */
    public void setConfig(CAComponentConfig config) throws ConfigurationException {
        this.config = config;
        

        init();
    }
    
    /**
     * Get the configuration object 
     * @return the configuration object
     */
    public CAComponentConfig getConfig() {        
        return config;
    }
    
    
    /**
     * Validate GridCA configuration object.
     *
     * @throws com.sun.grid.grm.config.ConfigurationException if the configuration object
     *           is invalid
     */
    public void validate() throws ConfigurationException {
        try {
            gridCAConfig.validate();
        } catch (GridCAException ex) {
            /* 
             * Do not path root cause to client, because client cannot load
             * GridCAException
             */ 
            throw new ConfigurationException(ex.getLocalizedMessage());
        }

        File tmpDir = gridCAConfig.getTmpDir();

        if ((tmpDir != null) && !tmpDir.exists()) {
            if (!tmpDir.mkdirs()) {
                throw new ConfigurationException("grmCA.error.tmpDir", BUNDLE,
                                                 tmpDir);
            }
        }
    }

    /**
     *Initialize GridCA with current env and config
     */
    private void init() throws ConfigurationException {

        gridCAConfig.setCaTop(SecurityPathUtil.getCaTop(env));
        gridCAConfig.setCaLocalTop(SecurityPathUtil.getCaLocalTop(env));
        gridCAConfig.setAdminUser(config.getAdminUser());
        gridCAConfig.setCaHost(config.getCaHost());

        if(config.getSgeCaScript() == null) {
            throw new ConfigurationException("sgeCaScript not set");
        }
        File caScript = new File(config.getSgeCaScript());
        if(!caScript.exists()) {
            throw new ConfigurationException("sgeCaScript " + caScript + " does not exist");
        }
        
        gridCAConfig.setSgeCaScript(caScript);
        
        //TODO: Put proper value
        //gridCAConfig.setConfigDir(sgeCADir);

        gridCAConfig.setTmpDir(PathUtil.getTmpDirForComponent(env, "ca"));
        
        
        // Per default the validity of the certificates is 5 years
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.YEAR, 5);
        
        long now = System.currentTimeMillis();
        long fiveYearsInMillis = cal.getTimeInMillis() - now;
        int days = (int)(fiveYearsInMillis / (1000L * 60 * 60 * 24));
        
        gridCAConfig.setDaysValid(days);
        validate();
        setSgeRoot();
    }

    /**
     * Get the name of the admin user of the grm ca.
     * @return the name of the admin user
     */
    public String getAdminUser() {
        return gridCAConfig.getAdminUser();
    }
    
    private void setSgeRoot() {
        File sgeCAScript = gridCAConfig.getSgeCaScript();
        sgeCAScript = sgeCAScript.getAbsoluteFile();
        
        //Remove 3 levels from $SGEROOT/util/ca/sge_ca to get SGEROOT
        sgeRoot = sgeCAScript.getParentFile().getParentFile().getParentFile();
    }
    
    /**
     * Return the sge_root for current configuration
     * @return sgeRoot
     */
    public File getSgeRoot() {
       
        return sgeRoot;
    }
    /**
     * Set the name of the admin user of the grm ca
     * @param adminUser name of the admin user
     */
    public void setAdminUser(String adminUser) {
        gridCAConfig.setAdminUser(adminUser);
    }

    /**
     * Get the name of the ca host
     * @return name of the ca host
     */
    public String getCaHost() {
        return gridCAConfig.getCaHost();
    }

    /**
     * Set the name of the ca host
     * @param caHost name of the ca host
     */
    public void setCaHost(String caHost) {
        gridCAConfig.setCaHost(caHost);
    }



    /*
     *  Get the configuartion object which can be used to create a 
     *  <code>GridCA</code> instance.
     *
     *  @return the configuration object
     */
    GridCAConfiguration getGridCAConfig() {
        return gridCAConfig;
    }

    
    /**
     * get the temp directory of the grm ca component.
     *
     * @return the tmp directory of the grm ca component
     */
    public File getTmpDir() {
        return gridCAConfig.getTmpDir();
    }
}

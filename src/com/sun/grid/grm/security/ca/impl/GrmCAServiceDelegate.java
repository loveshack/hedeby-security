/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2008 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.security.ca.impl;

import com.sun.grid.grm.ComponentState;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.GrmRemoteException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.event.ComponentEventListener;
import com.sun.grid.grm.security.ca.GrmCA;
import com.sun.grid.grm.security.ca.GrmCAException;
import com.sun.grid.grm.security.ca.KeyStoreWrapper;
import com.sun.grid.grm.util.Hostname;
import java.security.cert.X509Certificate;

/**
 *  This class delegates all calls to an instance of <code>GrmCAComponentContainer</code>.
 * 
 * <p><b>Attention!!:</b> This class is not longer used by the SDM system. However existing
 *    installations may reference this class. For the sake of compatibility we keep it in
 *    the system.</p>
 * 
 * @see GrmCAComponentContainer
 * @deprecated Has been replaced by {@link GrmCAComponentContainer}
 */
public class GrmCAServiceDelegate implements GrmCA {

    private final GrmCAComponentContainer container;
    
    /**
     * Create a new instance of GrmCAServiceDelegate. 
     * @param env   the execution env
     * @param name  the name
     * @throws com.sun.grid.grm.GrmException
     */
    public GrmCAServiceDelegate(ExecutionEnv env, String name) throws GrmException {
        this.container = new GrmCAComponentContainer(env, name);
    }    
    
    // -------------------------------------------------------------------------
    // Note:
    // The following method do not have jdocs, they all forwards the calls only
    // to the container
    // -------------------------------------------------------------------------
    
    public void start() throws GrmException {
        container.start();
    }

    public void stop(boolean isForced) throws GrmException {
        container.stop(isForced);
    }

    public void reload(boolean isForced) throws GrmException {
        container.reload(isForced);
    }

    public ComponentState getState() throws GrmRemoteException {
        return container.getState();
    }

    public void addComponentEventListener(ComponentEventListener componentEventListener) throws GrmRemoteException {
        container.addComponentEventListener(componentEventListener);
    }

    public void removeComponentEventListener(ComponentEventListener componentEventListener) throws GrmRemoteException {
        container.removeComponentEventListener(componentEventListener);
    }

    public String getName() throws GrmRemoteException {
        return container.getName();
    }

    public Hostname getHostname() throws GrmRemoteException {
        return container.getHostname();
    }

    public void createUser(String username, String group, String email) throws GrmRemoteException, GrmCAException {
        container.createUser(username, group, email);
    }

    public X509Certificate getCACertificate() throws GrmRemoteException, GrmCAException {
        return container.getCACertificate();
    }

    public X509Certificate getCertificate(String username) throws GrmRemoteException, GrmCAException {
        return container.getCertificate(username);
    }

    public X509Certificate getDaemonCertificate(String daemon) throws GrmRemoteException, GrmCAException {
        return container.getDaemonCertificate(daemon);
    }

    public KeyStoreWrapper createKeyStore(String username, char[] keystorePassword, char[] privateKeyPassword) throws GrmRemoteException, GrmCAException {
        return container.createKeyStore(username, keystorePassword, privateKeyPassword);
    }

    public X509Certificate renewCertificate(String username, int days) throws GrmRemoteException, GrmCAException {
        return container.renewCertificate(username, days);
    }

    public X509Certificate renewDaemonCertificate(String daemon, int days) throws GrmRemoteException, GrmCAException {
        return container.renewDaemonCertificate(daemon, days);
    }

    public void renewCaCertificate(int days) throws GrmRemoteException, GrmCAException {
        container.renewCaCertificate(days);
    }

    public void createDaemon(String daemon, String user, String email) throws GrmRemoteException, GrmCAException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public KeyStoreWrapper createDaemonKeyStore(String daemon) throws GrmRemoteException, GrmCAException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}

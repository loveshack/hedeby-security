/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.security.cli;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ComponentService;
import com.sun.grid.grm.cli.AbstractCli;
import com.sun.grid.grm.cli.AbstractCliCommand;
import com.sun.grid.grm.security.ca.GrmCA;

/**
 * Abstract class for security Cli commands that provides CA component
 */
public abstract class AbstractSecurityCliCommand extends AbstractCliCommand  {
    
    private final static String BUNDLE = "com.sun.grid.grm.security.cli.messages";
    
    /**
     * Returns a proxy for the CA.
     * @param cli the cli object
     * @return a proxy for the CA
     * @throws com.sun.grid.grm.GrmException if the ca component has not been found
     */
    public static GrmCA getCA(AbstractCli cli) throws GrmException {
        
        GrmCA ret = ComponentService.<GrmCA>getComponentByType(cli.getExecutionEnv(), GrmCA.class);
        if(ret == null) {
            throw new GrmException("AbstractSecurityCliCommand.error.no_ca", BUNDLE);
        } else {
            return ret;
        }
    }
    
    
}

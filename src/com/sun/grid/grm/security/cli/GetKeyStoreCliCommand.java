/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.security.cli;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.cli.AbstractCli;
import com.sun.grid.grm.cli.AbstractCliCommand;
import com.sun.grid.grm.cli.CliCategory;
import com.sun.grid.grm.cli.CliCommandDescriptor;
import com.sun.grid.grm.cli.CliOptionDescriptor;
import com.sun.grid.grm.cli.UsageException;
import com.sun.grid.grm.security.ConsoleCallbackHandler;
import com.sun.grid.grm.security.UserPrivilege;
import com.sun.grid.grm.security.SecurityPathUtil;
import com.sun.grid.grm.security.ui.GetDaemonKeyStoreCommand;
import com.sun.grid.grm.security.ui.GetUserKeyStoreCommand;
import com.sun.grid.grm.security.ui.StoreKeyStoreCommand;
import java.io.File;
import java.io.IOException;
import java.security.KeyStore;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.callback.UnsupportedCallbackException;

/**
 *  This class implements "sdmadm get_keystore".
 */
@CliCommandDescriptor(
    name = "client.option.get_keystore",
    hasShortcut = true,
    category = CliCategory.SECURITY,
    bundle = "com.sun.grid.grm.security.cli.messages",
    requiredPrivileges= { UserPrivilege.READ_BOOTSTRAP_CONFIG, UserPrivilege.ADMINISTRATOR, UserPrivilege.WRITE_KEYSTORE }
)
public class GetKeyStoreCliCommand extends AbstractCliCommand {
    /*
     * Optional parameter that specifies the name of file to which keystore should
     * be written
     */
    @CliOptionDescriptor(
        name = "client.option.get_keystore.f",
        numberOfOperands = 1
    )
    private String file;
    
    /*
     * Required parameter for specifying if certificate is for user or name
     */
    @CliOptionDescriptor(
        name = "client.option.get_keystore.t",
        numberOfOperands = 1,
        required = true
    )
    private String type;
    
    /*
     * Name of the user or daemon for which the keystore should be retrieved
     */ 
    @CliOptionDescriptor(
        name = "client.option.get_keystore.g",
        numberOfOperands = 1,
        required = true
    )
    private String name;
    
    /*
     * Optional parameter defining if the keystore should be with password or 
     * without, if yes the user will be prompted for password
     */
    @CliOptionDescriptor(
        name = "client.option.get_keystore.p",
        numberOfOperands = 0,
        required = false
    )
    private boolean promptPassword;
    
    /*
     * Optional parameter defining whether the keystore should be in Hex
     */
    @CliOptionDescriptor(
        name = "client.option.get_keystore.hex",
        numberOfOperands = 0,
        required = false
    )
    private boolean hex = false;
    
        
    public void execute(AbstractCli cli) throws GrmException {
        KeyStore keystore = null;
        File outFile = null;
        char[] password = null;
        
        ExecutionEnv env = cli.getExecutionEnv();
        
        if (type.equals("daemon")) {
            GetDaemonKeyStoreCommand command = new GetDaemonKeyStoreCommand(name);
            //Info for exception message
            keystore = env.getCommandService().execute(command).getReturnValue().getKeyStore();

            if(file == null) {
                outFile = SecurityPathUtil.getDaemonKeystoreFile(env, name);
            } else if (file.equals("-")) {
                outFile = null;
            } else {
                outFile = new File(file);
            }
            password = new char[0];


        } else if (type.equals("user")) {

            if(file == null) {
                outFile = SecurityPathUtil.getUserKeystoreFile(env, name);
            } else if (file.equals("-")) {
                outFile = null;
            } else {
                outFile = new File(file);
            }

            PasswordCallback keystorePWCallback =
                    new PasswordCallback("keystore password:", false);
            PasswordCallback keyPWCallback =
                    new PasswordCallback("private key password:", false);
            
            // Retrieve password if requested if not use empty char
            if(promptPassword) {
                ConsoleCallbackHandler callback =
                        new ConsoleCallbackHandler();
                try {
                    callback.handle(new Callback [] {
                        keystorePWCallback,
                        keyPWCallback
                    });
                } catch (IOException e) {
                    throw new GrmException("GetKeyStoreCliCommand.reading_input", e,
                            getBundleName(), e.getLocalizedMessage());
                } catch (UnsupportedCallbackException e) {
                    throw new AssertionError("CallbackHandler does not support callback: " +
                                             e.getMessage());
                }
            } else {
                keystorePWCallback.setPassword(new char[0]);
                keyPWCallback.setPassword(new char[0]);
            }

            try {
                char [] ksPW = keystorePWCallback.getPassword();
                char [] pkPW = keyPWCallback.getPassword();

                if (ksPW == null) {
                    ksPW = new char[0];
                }

                if (pkPW == null) {
                    pkPW = new char[0];
                }
                GetUserKeyStoreCommand command = new GetUserKeyStoreCommand(name, ksPW, pkPW);
                //Info for exception
                keystore = env.getCommandService().execute(command).getReturnValue().getKeyStore();
                password = ksPW;

            } finally {
                keystorePWCallback.clearPassword();
                keyPWCallback.clearPassword();
            }

        } else {
            throw new UsageException("cli.invalid_keystore_type",
                                     getBundleName(), type);
        }
        // Store the keystore in file
        StoreKeyStoreCommand com = new StoreKeyStoreCommand(keystore, password, outFile, null, hex);

        env.getCommandService().execute(com);        
        if (outFile != null) {
            cli.out().println("client.keystore_stored", getBundleName(), outFile);
        }
    }
}

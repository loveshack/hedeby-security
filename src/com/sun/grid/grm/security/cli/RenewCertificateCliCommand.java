/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.security.cli;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.cli.AbstractCli;
import com.sun.grid.grm.cli.CliCategory;
import com.sun.grid.grm.cli.CliCommandDescriptor;
import com.sun.grid.grm.cli.CliOptionDescriptor;
import com.sun.grid.grm.cli.UsageException;
import com.sun.grid.grm.cli.table.AbstractDefaultTableModel;
import com.sun.grid.grm.cli.table.AbstractTableCliCommand;
import com.sun.grid.grm.cli.table.Table;
import com.sun.grid.grm.security.CertificateType;
import com.sun.grid.grm.security.UserPrivilege;
import com.sun.grid.grm.security.ui.RenewCompleteCACommand;
import com.sun.grid.grm.security.ui.RenewCertificatResult;
import com.sun.grid.grm.security.ui.RenewDaemonCommand;
import com.sun.grid.grm.security.ui.RenewUserCommand;
import com.sun.grid.grm.util.I18NManager;
import java.util.Collections;
import java.util.List;

/**
 *  This class implements "sdmadm renew_cert".
 */
@CliCommandDescriptor(
    name = "client.option.renew_cert",
    hasShortcut = true,
    category = CliCategory.SECURITY,
    bundle = "com.sun.grid.grm.security.cli.messages",
    requiredPrivileges = { UserPrivilege.READ_BOOTSTRAP_CONFIG, UserPrivilege.ADMINISTRATOR },
    optionalPrivileges = { UserPrivilege.SUPER_USER, UserPrivilege.WRITE_LOCAL_SPOOL_DIR }
)
public class RenewCertificateCliCommand extends AbstractTableCliCommand {
    
    /*
     * Required option
     * Type of certificate to be renewed: user, daemon, ca
     */
    @CliOptionDescriptor(
        name = "client.option.renew_cert.t",
        numberOfOperands = 1,
        required = true
    )
    private String type = null;
    
    /*
     * Optional parameter for name of user or daemon,
     */
    @CliOptionDescriptor(
        name = "client.option.renew_cert.n",
        numberOfOperands = 1,
        required = false
    )
    private String name = null;
    
    /*
     * Mandatory option fore defining for how long the certificate
     * should be renewed
     */
    @CliOptionDescriptor(
        name = "client.option.renew_cert.d",
        numberOfOperands = 1,
        required = true
    )
    private int days = 0;

    public void execute(AbstractCli cli) throws GrmException {
        ExecutionEnv env = cli.getExecutionEnv();
        
        CertificateType certType = null;
        try {
            certType = CertificateType.valueOf(type.toUpperCase());
        } catch(Exception ex) {
            throw new UsageException("cli.invalid_keystore_type",
                    getBundleName(), type);
        }
        
        List<RenewCertificatResult> result = null;
        switch (certType) {
            case USER:
                 {
                    if (name == null) {
                        throw new UsageException("client.option.renew_cert.n.missing", getBundleName());
                    }
                    RenewUserCommand command = new RenewUserCommand(name, days);
                    env.getCommandService().execute(command);
                    
                    result = Collections.singletonList(new RenewCertificatResult(certType, name, 
                                  I18NManager.formatMessage("client.cert_renewed", getBundleName())));
                }
                break;
            case DAEMON:
                 {
                    if (name == null) {
                        throw new UsageException("client.option.renew_cert.n.missing", getBundleName());
                    }

                    RenewDaemonCommand command = new RenewDaemonCommand(name, days);
                    env.getCommandService().execute(command);
                    result = Collections.singletonList(new RenewCertificatResult(certType, name, 
                                  I18NManager.formatMessage("client.cert_renewed", getBundleName())));
                }
                break;
            case CA:
                 {
                    // We can only renew the CA certificate on the master_host. All jvms
                    // must be stopped and the command must be executed as super user
                    // The RenewCompleteCACommand will renew the CA certificate and update
                    // all daemon keystores and the admin user keystore
                    RenewCompleteCACommand command = new RenewCompleteCACommand(days);
                    result = env.getCommandService().executeLocally(command).getReturnValue();
                }
                break;
            default:
                throw new IllegalStateException("Unknown certificate type " + certType);
        }
        
        // Print the result
        RenewCertTableModel tm = new RenewCertTableModel(cli, result);
        Table table = createTable(tm, cli);
        table.print(cli);
        
    }
    
    class RenewCertTableModel extends AbstractDefaultTableModel {

        private final static long serialVersionUID = -200807150L;
        
        private final List<RenewCertificatResult> result;
        private final AbstractCli cli;
        
        public RenewCertTableModel(AbstractCli cli, List<RenewCertificatResult> result) {
            super(false,true,false,cli.getDebug());
            this.result = result;
            this.cli = cli;
        }
        public int getRowCount() {
            return result.size();
        }

        public int getColumnCount() {
            return 3;
        }

        public Object getValueAt(int row, int col) {
            RenewCertificatResult res = result.get(row);
            switch (col) {
                case 0: return res.getType().toString().toLowerCase();
                case 1:
                    if (res.getType().equals(CertificateType.CA)) {
                        return "";
                    } else {
                        return res.getName();
                    }
                case 2:
                    return res.getMessage();
                default:
                    throw new IllegalArgumentException("Unknown col " + col);
            }
        }

        @Override
        public String getColumnName(int col) {
            switch(col) {
                case 0: return getBundle().getString("RenewCertificateCliCommand.col.type");
                case 1: return getBundle().getString("RenewCertificateCliCommand.col.name");
                case 2: return getBundle().getString("RenewCertificateCliCommand.col.message");
                default:
                    throw new IllegalArgumentException("Unknown col " + col);
            }
        }

        @Override
        public String getValueForDebugColumn(int row) {
            RenewCertificatResult r = result.get(row);
            return errorStackTraceToString(r.getError());
        }       
    }
}


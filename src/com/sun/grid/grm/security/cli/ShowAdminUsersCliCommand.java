/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.security.cli;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.cli.AbstractCli;
import com.sun.grid.grm.cli.CliCategory;
import com.sun.grid.grm.cli.CliCommandDescriptor;
import com.sun.grid.grm.cli.table.AbstractDefaultTableModel;
import com.sun.grid.grm.cli.table.AbstractSortedTableCliCommand;
import com.sun.grid.grm.cli.table.Table;
import com.sun.grid.grm.security.UserPrivilege;
import com.sun.grid.grm.security.ui.GetAdminUsersCommand;
import java.util.ArrayList;
import java.util.List;

/**
 *  This class implements "sdmadm show_admin_users".
 */
@CliCommandDescriptor(
    name = "client.option.show_admin_users",
    hasShortcut = true,
    category = CliCategory.SECURITY,
    bundle = "com.sun.grid.grm.security.cli.messages",
    requiredPrivileges= { UserPrivilege.READ_BOOTSTRAP_CONFIG, UserPrivilege.ADMINISTRATOR }
)
public class ShowAdminUsersCliCommand extends AbstractSortedTableCliCommand {

    public void execute(AbstractCli cli) throws GrmException {

        GetAdminUsersCommand com = new GetAdminUsersCommand();
        
        List<String> adminUsers = new ArrayList<String>(cli.getExecutionEnv().getCommandService().execute(com).getReturnValue());

        TableModel tm = new TableModel(adminUsers);
        Table table = createTable(tm, cli);
        table.print(cli);
    }
    private class TableModel extends AbstractDefaultTableModel {

        private List<String> adminUsers;

        public TableModel(List<String> adminUsers) {
            this.adminUsers = adminUsers;
        }

        public String getRow(int row) {
            return adminUsers.get(row);
        }

        public int getRowCount() {
            return adminUsers.size();
        }

        public int getColumnCount() {
            return 1;
        }

        public Object getValueAt(int rowIndex, int columnIndex) {
            String elem = getRow(rowIndex);
            switch (columnIndex) {
                case 0:
                    return elem;                            
                default:
                    throw new IllegalArgumentException("unknown column " + columnIndex);
            }
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            return String.class;
        }

        @Override
        public String getColumnName(int column) {
            switch (column) {
                case 0:
                    return getBundle().getString("ShowAdminUsersCliCommand.col.admin_user");               
                default:
                    throw new IllegalArgumentException("unknown column " + column);
            }
        }
    }
}

/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.security.cli;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.cli.AbstractCli;
import com.sun.grid.grm.cli.CliCategory;
import com.sun.grid.grm.cli.CliCommandDescriptor;
import com.sun.grid.grm.cli.CliOptionDescriptor;
import com.sun.grid.grm.cli.UsageException;
import com.sun.grid.grm.security.AskingX509TrustManager;
import com.sun.grid.grm.security.CertificateType;
import com.sun.grid.grm.security.UserPrivilege;
import com.sun.grid.grm.security.ui.GetCertificateCommand;
import com.sun.grid.grm.util.Printer;
import java.io.StringWriter;
import java.security.cert.X509Certificate;

/**
 *  This class implements "sdmadm show_cert".
 */
@CliCommandDescriptor(
    name = "ShowCertificateCliCommand",
    hasShortcut = true,
    category = CliCategory.SECURITY,
    bundle = "com.sun.grid.grm.security.cli.messages",
    requiredPrivileges = { UserPrivilege.READ_BOOTSTRAP_CONFIG, UserPrivilege.ADMINISTRATOR }
)
public class ShowCertificateCliCommand extends AbstractSecurityCliCommand {
    
    /*
     * Option for defining type deamon or user
     */
    @CliOptionDescriptor(
        name = "ShowCertificateCliCommand.t",
        numberOfOperands = 1,
        required = true
    )
    private String type = null;

    /*
     * name of the user or name
     */
    @CliOptionDescriptor(
        name = "ShowCertificateCliCommand.n",
        numberOfOperands = 1,
        required = false
    )
    private String name = null;

    public void execute(AbstractCli cli) throws GrmException {

        ExecutionEnv env = cli.getExecutionEnv();

        CertificateType certType = null;
        try {
           certType = CertificateType.valueOf(type.toUpperCase());  
        } catch(Exception ex) {
            throw new UsageException("cli.invalid_keystore_type",
                                     getBundleName(), type);
        }

        if (certType != CertificateType.CA 
            && (name == null || name.length() == 0)) {
            throw new UsageException("ShowCertificateCliCommand.n.required");
        }
        
        GetCertificateCommand command = new GetCertificateCommand(name, certType);

        X509Certificate cert = env.getCommandService().execute(command).getReturnValue();
        StringWriter sw = new StringWriter();
        
        Printer pw = new Printer(sw);
        try {
            AskingX509TrustManager.printCertificate(cert, pw);
        } finally {
            pw.close();
        }
        
        cli.out().printlnDirectly(sw.getBuffer().toString());
    }
}

/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.security.cli;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.cli.AbstractCli;
import com.sun.grid.grm.cli.AbstractCliCommand;
import com.sun.grid.grm.cli.CliCategory;
import com.sun.grid.grm.cli.CliCommandDescriptor;
import com.sun.grid.grm.security.UserPrivilege;
import com.sun.grid.grm.security.ui.UpdateCACertCommand;

/**
 *  This CLI command get the current CA certificate from
 *  CS and stores in in the local spooldir
 */
@CliCommandDescriptor(
    name = "client.option.update_ca_cert",
    hasShortcut = true,
    category = CliCategory.SECURITY,
    bundle = "com.sun.grid.grm.security.cli.messages",
    requiredPrivileges= { UserPrivilege.READ_BOOTSTRAP_CONFIG, UserPrivilege.ADMINISTRATOR, UserPrivilege.WRITE_LOCAL_SPOOL_DIR }
)
public class UpdateCACertCliCommand extends AbstractCliCommand {
    

    /**
     * Execute to command
     * @param cli  the cli object
     * @throws com.sun.grid.grm.GrmException if the command is failed
     */
    public void execute(AbstractCli cli) throws GrmException {

        UpdateCACertCommand updateCertCmd = new UpdateCACertCommand();

        cli.getExecutionEnv().getCommandService().execute(updateCertCmd);
    }
    
}

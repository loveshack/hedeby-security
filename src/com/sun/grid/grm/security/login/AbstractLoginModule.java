/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.security.login;

import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.security.auth.Subject;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.login.LoginException;
import javax.security.auth.spi.LoginModule;

/**
 *  Base class of LoginModule in the Hedeby system.
 * 
 *  <p>This class implements an performance improvement for authentication. In 
 *  issue 83 (user/password LoginModule is always executed) this problem has
 *  been reported.</p>
 *  
 *  <p>Normally the jaas.config file contains three login modulues:
 *  <ul>
 *     <li><code>GrmCATrustManagerLoginModule</code> for authentication against the X509 certificate</li>
 *     <li><code>DelegatingLoginModule</code> for username/password authentication (by delegating
 *         the login to the <code>UnixLoginModule</code> from juti.jar)</li>
 *     <li><code>RoleLoginModule</code> for adding the the roles to the subject</li>
 *  </ul>
 * 
 *  <p>The RoleLoginModule must be always executed, while GrmCATrustManagerLoginModule and
 *     the DelegatingLoginModule are optional. If GrmCATrustManagerLoginModule was succesfully
 *     it makes no sence doing a username/password authentication.</p>
 * 
 *  <p><CODE>GrmCATrustManagerLoginModule</CODE> and <CODE>DelegatingLoginModule</CODE> are extended 
 *     from <CODE>AbstractLoginModule</CODE>. Both increments the value of <code>SUCCEEDED_LOGIN_COUNT</code> 
 *     in the shared state of the login process. If <code>SUCCEEDED_LOGIN_COUNT</code> is greater 
 *     then <code>0</code> the login for this <CODE>LoginModule</CODE>s is not executed.</p>
 */
public abstract class AbstractLoginModule implements LoginModule {
    
    /** Name of the resource bundle for this class */
    private static final String BUNDLE = "com.sun.grid.grm.security.login.messages";
    
    private final static Logger log = Logger.getLogger(AbstractLoginModule.class.getName(), BUNDLE);
    
    private Map<String, Object> sharedState;
    
    /**
     * Key for the SUCCEEDED_LOGIN_COUNT field in the shared state
     */
    protected final static String SUCCEEDED_LOGIN_COUNT =  "SUCCEEDED_LOGIN_COUNT";
    
    /**
     * Initialize this login module.
     * 
     * <p>Stores the <CODE>sharedState</CODE> in as member variable and calls <CODE>doIntialize</CODE>.
     * @param subject The subject
     * @param callbackHandler the callback handler
     * 
     * @param sharedState the sharedState
     * @param options options for the login
     */
    @SuppressWarnings("unchecked")
    public final void initialize(Subject subject, CallbackHandler callbackHandler, Map<String, ?> sharedState, Map<String, ?> options) {
        this.sharedState = (Map<String,Object>)sharedState;
        doInitialize(subject, callbackHandler, sharedState, options);
    }
    
    /**
     * Initialize the login module.
     * @param subject the subject
     * @param callbackHandler the callback handler
     * @param sharedState the shared state
     * @param options the options
     */
    protected abstract void doInitialize(Subject subject, CallbackHandler callbackHandler, Map<String, ?> sharedState, Map<String, ?> options);
    /**
     * Perform the login.
     * 
     * This method is only executed if the <CODE>SUCCEEDED_LOGIN_COUNT</CODE> in the sharedState
     * is <CODE>0</CODE>.
     * @throws javax.security.auth.login.LoginException if the login failed
     * @return <code>true</code> if the login was succesfull.
     */
    protected abstract boolean doLogin() throws LoginException;
    
    /**
     * 
     * Peform the login.
     * 
     * <p>Checks if the SUCCEEDED_LOGIN_COUNT is 0 and calls <code>doLogin</code>.</p>
     * @return <code>true</code> if <CODE>doLogin</CODE> has been executed and if <CODE>doLogin</CODE> 
     * returned <code>true</code>.
     * @throws javax.security.auth.login.LoginException the exception from <code>doLogin</code>
     */
    public final boolean login() throws LoginException {
        log.entering(getClass().getSimpleName(), "login");
        boolean ret = false;
        int count = getSucceededLoginCount();
        if( count == 0) {
            ret = doLogin();
            if(ret) {
                sharedState.put(SUCCEEDED_LOGIN_COUNT, new Integer(getSucceededLoginCount() + 1));
            }
        } else if (log.isLoggable(Level.FINE)) {
            log.log(Level.FINE, "alm.skip", getClass().getName());
        }
        log.exiting(getClass().getSimpleName(), "login", ret);
        return ret;
    }
    
    /**
     * Get the number of succeed logins from the <CODE>sharedState</CODE>.
     * @return number of succeeded logins
     */
    protected int getSucceededLoginCount() {
        int ret = 0;
        if(sharedState != null ) {
            Integer value = (Integer)sharedState.get(SUCCEEDED_LOGIN_COUNT);
            if(value != null) {
                ret = value;
            }
        }
        return ret;
    }
    
}

/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.security.login;

import com.sun.grid.grm.util.GrmClassLoaderFactory;
import java.security.Principal;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import javax.security.auth.Subject;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.login.LoginException;
import javax.security.auth.spi.LoginModule;

/**
 * Delegation Login Module
 *
 * provides the functionality to delegate authentication to a LoginModule
 * which is not available in the system classpath.
 *
 * This login module allows the following parameters:
 *
 * <table border="1">
 *    <tr>
 *       <th>Name</th><th>Description</th>
 *    </tr>
 *    <tr>
 *       <td><code>classname</code></td>
 *       <td>Name of the delegate login module.</td>
 *    </tr>
 *    <tr>
 *       <td><code>classpath</code></td>
 *       <td>Classpath which is used to construct the delegate login module (
 *           path seperator is a black)</td>
 *    </tr>
 * </table>
 *
 * <p>All other options are passed through the delegate login module</p>
 *
 * <H3>Example jaas.config file</H3>
 *
 * <pre>
 *
 * system1 {
 *  com.sun.grid.grm.security.login.DelegatingLoginModule sufficient
 *       classpath="file:/opt/sdm/lib/hedeby-security-impl.jar /opt/sge/lib/juti.jar"
 *       classname="com.sun.grid.security.login.UnixLoginModule"
 *       sge_root="/opt/sge"
 *       auth_method="system";
 * };
 * </pre>
 */
public class DelegatingLoginModule extends AbstractLoginModule {

    public static final String BUNDLE = "com.sun.grid.grm.security.login.messages";
    
    private final static Logger log = Logger.getLogger(DelegatingLoginModule.class.getName(), BUNDLE);
    
    private final static Map<CacheElem,Class<? extends LoginModule>> cache = new HashMap<CacheElem,Class<? extends LoginModule>>(10);
    private final static Lock lock = new ReentrantLock();
    
    private LoginModule delegate;
    private Subject  subject;

    /**
     * This method tries to get a class of the delegate login module from
     * the cache.
     * If it is not stored in the cache it tries to create it.
     * @param classname
     * @param classpath
     * @return the class of the delegate login module or <code>null</code> of the class
     *         could not be loaded
     */
    private Class<? extends LoginModule> getDelegateClass(String classname, String classpath) {
        CacheElem elem = new CacheElem(classname, classpath);
        Class<? extends LoginModule> ret = null;
        
        lock.lock();
        try {
            // If the class of a cached elem could not be created
            // the map contains null values
            // We have to check that the key exists
            if(cache.containsKey(elem)) {
                ret = cache.get(elem);
            } else {
                ret = elem.getDelegateClass();
                cache.put(elem, ret);
            }
        } finally {
            lock.unlock();
        }
        return ret;
    }
    /**
     * Initialize the login module.
     *
     * @param subject  the subject
     * @param callbackHandler the callbackHandler
     * @param sharedState the shared state
     * @param options options for the login module
     */
    protected void doInitialize(Subject subject, CallbackHandler callbackHandler, Map<String, ?> sharedState, Map<String, ?> options) {
        
        log.entering("DelegatingLoginModule", "doInitialize");
        
        this.subject  = subject;
        
        String classname = (String)options.get("classname");
        
        if(classname == null) {
            log.log(Level.WARNING, "dlm.mo", "classname");
            return;
        }
        
        String classpath = (String)options.get("classpath");
        
        Class<? extends LoginModule> cls = getDelegateClass(classname, classpath);
        
        if(cls != null) {
            try {
                delegate = cls.newInstance();
                if(log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "dlm.delegate", new Object[] { "initialize", delegate.getClass().getName() });
                }
                delegate.initialize(subject, callbackHandler, sharedState, options);
            } catch (Exception ex) {
                LogRecord lr = new LogRecord(Level.WARNING, "dlm.ci");
                lr.setParameters(new Object[] { classname, ex.getLocalizedMessage()});
                lr.setThrown(ex);
                log.log(lr);
            }
        } else {
            // We have an invalid configuration
            // log messages has already been written
            delegate = null;
        }
        
        log.exiting("DelegatingLoginModule", "doInitialize");
        
    }

    /*
     *  see interface
     */
    protected boolean doLogin() throws LoginException {
        log.entering("DelegatingLoginModule", "doLogin");
        boolean ret = false;
        if(delegate != null) { 
            try {
                if(log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "dlm.delegate", new Object[] { "login", delegate.getClass().getName() });
                }
                ret = delegate.login();
            } catch(Throwable ex) {
                LogRecord lr = new LogRecord(Level.WARNING, "dlm.ue");
                lr.setParameters(new Object[] { "doLogin" });
                lr.setThrown(ex);
                log.log(lr);
            }
        }
        log.exiting("DelegatingLoginModule", "doLogin", ret);
        return ret;
    }

    /*
     *  see interface
     */
    public boolean commit() throws LoginException {
        log.entering("DelegatingLoginModule", "commit");
        boolean ret = false;
        if(delegate != null) { 
            try {
                Set<Principal> orgPrincipals = null;
                if(log.isLoggable(Level.FINE)) {
                    orgPrincipals = new HashSet<Principal>(subject.getPrincipals());
                    log.log(Level.FINE, "dlm.delegate", new Object[] { "commit", delegate.getClass().getName() });
                }
                ret = delegate.commit();
                if(log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "dlm.delegated", new Object[] { "commit", delegate.getClass().getName(), ret });
                    if(ret ) {
                        for(Principal p : subject.getPrincipals()) {
                            if(!orgPrincipals.contains(p)) {
                                log.log(Level.FINE, "dlm.add", new Object [] { p.getClass(), p.getName() });
                            }
                        }
                    }
                }

            } catch(Throwable ex) {
                LogRecord lr = new LogRecord(Level.WARNING, "dlm.ue");
                lr.setParameters(new Object[] { "commit" });
                lr.setThrown(ex);
                log.log(lr);
            }
        } else {
            log.log(Level.FINER, "do not have a delegate");
        }
        log.exiting("DelegatingLoginModule", "commit", ret);
        return ret;
    }

    /*
     *  see interface
     */
    public boolean abort() throws LoginException {
        log.entering("DelegatingLoginModule", "abort");
        boolean ret = false;
        if(delegate != null) { 
            try {
                if(log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "dlm.delegate", new Object[] { "abort", delegate.getClass().getName() });
                }
                ret = delegate.abort();
            } catch(Throwable ex) {
                LogRecord lr = new LogRecord(Level.WARNING, "dlm.ue");
                lr.setParameters(new Object[] { "abort" });
                lr.setThrown(ex);
                log.log(lr);
            }
        }
        log.exiting("DelegatingLoginModule", "abort", ret);
        return ret;
    }

    /*
     *  see interface
     */
    public boolean logout() throws LoginException {
        log.entering("DelegatingLoginModule", "logout");
        boolean ret = false;
        if(delegate != null) { 
            try {
                if(log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "dlm.delegate", new Object[] { "logout", delegate.getClass().getName() });
                }
                ret = delegate.logout();
            } catch(Throwable ex) {
                LogRecord lr = new LogRecord(Level.WARNING, "dlm.ue");
                lr.setParameters(new Object[] { "logout" });
                lr.setThrown(ex);
                log.log(lr);
            }
        }
        log.exiting("DelegatingLoginModule", "logout", ret);
        return ret;
    }
    
    private static class CacheElem {
        
        private final String classname;
        private final String classpath;
        private ClassLoader classLoader;
        private Class<? extends LoginModule> delegateClass;
        private boolean hasError;
        private final int hash;
        
        public CacheElem(String aClassname, String aClasspath) {
            if(aClassname == null) {
                throw new NullPointerException();
            }
            classname = aClassname;            
            classpath = aClasspath;
            int h = classname.hashCode();
            if(classpath != null) {
                h = h * 31 + classpath.hashCode();
            }
            hash = h;
        }
        
        private ClassLoader getClassLoader() {
            if (classpath == null) {
                return getClass().getClassLoader();
            } else {
                if (classLoader == null) {
                    classLoader = GrmClassLoaderFactory.getInstance(classpath, " ", getClass().getClassLoader());
                }
                return classLoader;
            }
        }
        
        @SuppressWarnings("unchecked")
        private Class<? extends LoginModule> getDelegateClass() {
            if (hasError == false && delegateClass == null) {
                ClassLoader cl = getClassLoader();

                Class<?> ret = null;
                try {
                    ret = Class.forName(classname, true, cl);
                    if (!LoginModule.class.isAssignableFrom(ret)) {
                        log.log(Level.WARNING, "dlm.ic", classname);
                        ret = null;
                    }
                } catch (ClassNotFoundException ex) {
                    LogRecord lr = new LogRecord(Level.WARNING, "dlm.cnf");
                    lr.setParameters(new Object[]{classname, classpath});
                    lr.setThrown(ex);
                    log.log(lr);
                } catch (NoClassDefFoundError ex) {
                    LogRecord lr = new LogRecord(Level.WARNING, "dlm.cnf");
                    lr.setParameters(new Object[]{classname, classpath});
                    lr.setThrown(ex);
                    log.log(lr);
                }
                if(ret == null) {
                    hasError = true;
                } else {
                    delegateClass = (Class<? extends LoginModule>)ret;
                }
            }
            return delegateClass;
        }
        
        @Override
        public boolean equals(Object obj) {
            if(obj instanceof CacheElem) {
                CacheElem elem = (CacheElem)obj;
                if(!classname.equals(elem.classname)) {
                    return false;
                }
                if(classpath == null) {
                    return elem.classpath == null;
                } else {
                    return classpath.equals(elem.classpath);
                }
            }
            return false;
        }

        @Override
        public int hashCode() {
            return hash;
        }
        
    }

}

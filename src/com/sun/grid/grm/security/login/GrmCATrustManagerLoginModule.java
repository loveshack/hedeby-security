/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.security.login;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.ExecutionEnvFactory;
import com.sun.grid.grm.bootstrap.SystemPropertyNotAvailableException;
import com.sun.grid.grm.security.*;
import com.sun.grid.grm.security.ca.GrmCATrustManager;
import com.sun.grid.grm.util.I18NManager;
import java.io.IOException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.management.remote.JMXServiceURL;
import javax.security.auth.Subject;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;
import javax.security.auth.login.LoginException;
import javax.security.auth.x500.X500Principal;

/** TODO: Update description
 * <p>This <code>LoginModule</code> verifies a <code>X509Certificate</code> chain
 * with a {@link com.sun.grid.grm.security.ca.GrmCATrustManager}. If the chain
 * is trusted it adds the <code>X500Principal</code> of the certificate
 * chain to the security <code>Subject</code>.</p>
 *
 * <p>With the path information a <code>GrmCATrustManagerLoginModule</code> object
 *    is created. These objects are cached in a static map. Futher login to the same
 *    system uses the cached <code>GrmCATrustManagerLoginModule</code>.
 *
 * <p>If a <code>GrmCATrustManagerLoginModule</code> object for the system is available
 *    this class uses a <code>X509CertificateChainCallback</code> to
 *    retrieve the <code>X509Certificate</code> chain . If <code>GrmCATrustManager</code>
 *    trusts the certificate chain the corresponding <code>X500Principal</code> is added
 *    the current subject.
 *
 * <p>This class uses a <code>Logger</code> with name "com.sun.grid.grm.security.GrmCATrustManagerLoginModule"
 *    to write log messages.</p>
 *
 * <H3>Example for a JAAS configuration file</H3>
 *
 * <pre>
 * jaas_sample {
 *
 *   com.sun.grid.grm.security.GrmCATrustManagerLoginModule required;
 *
 * };
 * </pre>
 *
 * <H3>Example <code>CallbackHandler</code></H3>
 *
 * <pre>
 *   class SampleCallbackHandler implements CallbackHandler {
 *
 *      private final X509Certificate [] chain;
 *
 *      public SampleCallbackHandler(final X509Certificate[] chain) {
 *          this.chain = chain
 *      }
 *
 *      public void handle(final Callback[] callbacks)
 *        throws IOException, UnsupportedCallbackException {
 *          for (int i = 0; i < callbacks.length; i++) {
 *              if (callbacks[i] instanceof X509CertificateChainCallback) {
 *
 *                  X509Certificate [] certs = (X509Certificate[])credentials.get(CERTIFICATE);
 *
 *                  X509CertificateChainCallback pkc = (X509CertificateChainCallback)callbacks[i];
 *                  pkc.setChain(certs);
 *
 *              } else {
 *                  throw new UnsupportedCallbackException(callbacks[i],
 *                                                         "Unrecognized Callback");
 *              }
 *
 *          }
 *
 *      }
 *  }
 *  </pre>
 *
 *  <H3>Example Login method</H3>
 *
 *  <pre>
 *  GrmName systemName = ...;
 *  X509Certificate [] chain = ...;
 *
 *  SampleCallbackHandler cb = new SampleCallbackHandler(systemName, chain);
 *
 *  LoginContext lc = null;
 *  try {
 *      lc = new LoginContext("jaas_sample", cb);
 *  } catch (LoginException le) {
 *      throw new SecurityException("Can not create LoginContext", le);
 *  }
 *  </pre>
 *
 */
public class GrmCATrustManagerLoginModule extends AbstractLoginModule {

    private static final String BUNDLE = "com.sun.grid.grm.security.security";
    private static final Logger LOGGER = Logger.getLogger(GrmCATrustManagerLoginModule.class.getName(),
                                                          BUNDLE);
    private CallbackHandler callbackHandler;
    private Subject subject;

    private Set<java.security.Principal> principals = new HashSet<java.security.Principal>();
    private X509Certificate [] chain;

    private boolean loginSuccessed;
    private boolean commitSuccessed;


    /**
     * Initialize this </code>LoginModule</code>.
     *
     * @param subject         the current subject
     * @param callbackHandler callbackHandler for retrieving system name and X509 certificate chain
     * @param sharedState     shared state (not used)
     * @param options         options (not used)
     */
    protected void doInitialize(Subject subject, CallbackHandler callbackHandler, Map<String, ?> sharedState, Map<String, ?> options) {
        LOGGER.entering("GrmCATrustManagerLoginModule", "doInitialize");
        
        this.subject = subject;
        this.callbackHandler = callbackHandler;
        
        LOGGER.exiting("GrmCATrustManagerLoginModule", "doInitialize");
    }
    
    private final static Map<JMXServiceURL, GrmCATrustManager> trustManagerMap = new HashMap<JMXServiceURL, GrmCATrustManager>();

    /* get the trust manager for a system */
    private static GrmCATrustManager getTrustManager() throws LoginException {

        LOGGER.entering("GrmCATrustManagerLoginModule", "getTrustManager");
        ExecutionEnv env = null;
        try {
            env = ExecutionEnvFactory.getInstanceFromSystemProperties();
        } catch (SystemPropertyNotAvailableException ex) {
            LoginException ex1 = new LoginException(
                    I18NManager.formatMessage("GrmCATrustManagerLoginModule.error.system", BUNDLE, env.getSystemName()));
            ex1.initCause(ex);
            throw ex1;
        } catch (GrmException ex) {
            LoginException ex1 = new LoginException(
                    I18NManager.formatMessage("GrmCATrustManagerLoginModule.error.system", BUNDLE, env.getSystemName()));
            ex1.initCause(ex);
            throw ex1;
        }
        GrmCATrustManager ret = null;
        synchronized(trustManagerMap) {
            ret = trustManagerMap.get(env.getCSURL());
            if(ret == null) {
                ret = new GrmCATrustManager(env);
                trustManagerMap.put(env.getCSURL(), ret);
            }
        }
        LOGGER.exiting("GrmCATrustManagerLoginModule", "getTrustManager", ret);
        return ret;
    }


    /**
     * Try to login into a Grm system with a x509 certificate chain. 
     * 
     * The systemname is retrieved with a <code>GrmNameCallback</code>. The 
     * x509 certificate chain is retriebed with a <code>X509CertificateChainCallback</code>.
     *
     * @throws javax.security.auth.login.LoginException if the <code>CallbackHandler</code> does not
     *                support the required <code>Callback</code>s or if an <code>Callback</code> throws
     *                an <code>IOException</code>.
     * @return <code>true</code> if the x509 certificate is trusted by the certificate
     *         authority of the system.
     */
    protected boolean doLogin() throws LoginException {

        LOGGER.entering("GrmCATrustManagerLoginModule", "doLogin");

        X509CertificateChainCallback certCallback = new X509CertificateChainCallback();

        try {
            callbackHandler.handle(new Callback [] {                
                certCallback
            });            
        } catch (UnsupportedCallbackException ex) {
            LoginException le =  new LoginException(
                                      I18NManager.formatMessage("error.invalidCallback", BUNDLE));
            le.initCause(ex);
            throw le;
        } catch (IOException ex) {
            LoginException le =  new LoginException(
                                      I18NManager.formatMessage("error.io", BUNDLE, ex.getLocalizedMessage()));
            le.initCause(ex);
            throw le;
        }

        if(certCallback.getChain() != null) {
            GrmCATrustManager trustManager = getTrustManager();

            try {
                trustManager.checkClientTrusted(certCallback.getChain(), "RSA");
                this.chain = certCallback.getChain();
                loginSuccessed = true;
            } catch (CertificateException ex) {
                if (LOGGER.isLoggable(Level.FINE)) {
                    LOGGER.log(Level.FINE, "GrmCATrustManagerLoginModule.loginFailed", ex);
                }

                loginSuccessed = false;
            }
        } else {
            loginSuccessed = false;
        }

        LOGGER.exiting("GrmCATrustManagerLoginModule", "doLogin", loginSuccessed);
        return loginSuccessed;
    }

    /**
     * If the login method had success the commit method adds the <code>X500Principal</code>
     * of the subject of the x509 certicate chain to the current subject.
     *
     * @return <code>true</code> if <code>X500Principal</code> has been added to the subject
     * @todo use <code>com.sun.grid.ca.GridCAX500Name to parse the commonn name of the certificate
     */
    public boolean commit() {
        LOGGER.entering("GrmCATrustManagerLoginModule", "commit");

        if (loginSuccessed) {
            X500Principal x500p = chain[0].getSubjectX500Principal();
            principals.add(x500p);

            String name = x500p.getName();
            int index = name.indexOf("UID=");

            if (index >= 0) {
                int end = name.indexOf(',', index+1);

                if (end >= 0) {
                    name = name.substring(index+4, end);

                    if (name.startsWith("sdm_daemon_")) {

                       name = x500p.getName();
                       index = name.indexOf("CN=");

                       if (index >= 0 ) {
                           end = name.indexOf(',', index+1);
                           if (end >= 0) {
                               name = name.substring(index+3, end);
                               principals.add(new GrmDaemonPrincipal(name.trim()));
                           }
                       }
                    } else {
                        principals.add(new UserPrincipal(name.trim()));
                    }
                }
            }

            subject.getPrincipals().addAll(principals);
            commitSuccessed = true;
        }

        LOGGER.exiting("GrmCATrustManagerLoginModule", "commit", commitSuccessed);
        return commitSuccessed;
    }

    /**
     * Abort the login.
     *
     * @return always <code>true</code>
     */
    public boolean abort() {
        LOGGER.entering("GrmCATrustManagerLoginModule", "abort");
        logout();
        LOGGER.exiting("GrmCATrustManagerLoginModule", "abort");
        return true;
    }

    /**
     * logout the current subject
     *
     * @return always <code>true</code>
     */
    public boolean logout() {
        LOGGER.entering("GrmCATrustManagerLoginModule", "logout");

        if (commitSuccessed) {
            subject.getPrincipals().removeAll(principals);
        }

        principals.clear();
        subject = null;
        chain = null;
        callbackHandler = null;
        commitSuccessed = false;
        loginSuccessed = false;
        LOGGER.exiting("GrmCATrustManagerLoginModule", "logout");
        return true;
    }

}

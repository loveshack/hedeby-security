/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.security.role;

/**
 *  Constants for the role package
 */
public interface RoleConstants {
    
    /** Name of the adminstrator role. */
    public static final String ADMIN_ROLE  = "administrator";
    
    /** Name of the daemon roles. */
    public static final String DAEMON_ROLE = "daemon";
    
    /** Namoe of the nobody role */
    public static final String NOBODY_ROLE = "nobody";
    
    /** Name of the Principal class which is added by the username/password 
     *  LoginModule.
     */
    public static final String UNIX_LOGIN_MODULE_PRINCIPAL = "com.sun.grid.security.login.UserPrincipal";
    
    /** Name of the Principal class which is added by the X509 certificate
     *  LoginModule.
     */
    public static final String GRM_CA_LOGIN_MODULE_PRINCIPAL = "com.sun.grid.grm.security.login.UserPrincipal";
    
    
}

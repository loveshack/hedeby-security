/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.security.role;

import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.ExecutionEnvFactory;
import com.sun.grid.grm.config.security.Role;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import javax.security.auth.Subject;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.login.LoginException;
import javax.security.auth.spi.LoginModule;

/**
 *  The <code>RoleLoginModule</code> searches the roles of the currents subject principals
 *  and adds the corresponding <code>RolePrincipal</code> to the current subject.
 *
 *  <p>This LoginModule does not need any configuration. The connection parameter to
 *     Hedeby Configuration Service is taken from the system properties. This means
 *     that this <code>LoginModule</code> can only be used inside a Hedeby Child JVM.</p>
 *
 *  <h3>Example JAAS config</h3>
 *
 *  <pre>
 *   sample {
 *      com.sun.security.auth.module.UnixLoginModule required;
 *
 *      com.sun.grid.grm.security.role.RoleLoginModule optional;
 *                                                     
 *   };
 *  </pre>
 *
 */
public class RoleLoginModule  implements LoginModule {

    private static final String BUNDLE = "com.sun.grid.grm.security.role.messages";
    private static final Logger LOGGER =  Logger.getLogger(RoleLoginModule.class.getName(), BUNDLE);

    private Subject subject;
    private CallbackHandler callbackHandler;
    private boolean succeeded;
    private boolean commitSucceeded;
    private List<Role> roles;
    private final List<Role> principals = new ArrayList<Role>();



    /**
     * initialize the LoginModule.
     *
     * @param subject          the current subject
     * @param callbackHandler  the callback handler
     * @param sharedState      map for storing shared states
     * @param options          options for the LoginModules
     * @throws java.lang.IllegalArgumentException if the role file can not be read
     */
    public final void initialize(final Subject subject,
            final CallbackHandler callbackHandler,
            final Map<String, ? > sharedState,
            final Map<String, ? > options)
            throws IllegalArgumentException {
        LOGGER.entering("RoleLoginModule", "initialize");
        
        try {
            this.subject = subject;
            this.callbackHandler = callbackHandler;

            ExecutionEnv env = ExecutionEnvFactory.getInstanceFromSystemProperties();
            roles = RolesUtil.getRoles(env);

        } catch (Throwable ex) {
            LogRecord lr = new LogRecord(Level.WARNING, "roleLoginModule.unexpectedError");
            lr.setParameters(new Object [] { "initialize", ex.getLocalizedMessage() });
            lr.setThrown(ex);
            lr.setResourceBundle(ResourceBundle.getBundle(BUNDLE));
            LOGGER.log(lr);
            roles = Collections.<Role>emptyList();
        } 
        
        LOGGER.exiting("RoleLoginModule", "initialize");
        
    }

    /** Helper method that returns the Role instance in which the 
     * principal (java.security.Principal) is matched
     */ 
    private Role getRole(final java.security.Principal p) {

        Role ret = null;
        for (Role role : roles) {
            if(RolesUtil.isMember(role, p)){
                ret = role;
                break;
            }
        }
        return ret;
    }

    /**
     * Method to authenticate a <code>Subject</code> (phase 1).
     *
     * @return <code>true</code> if the prinicipals of the current subject matches
     *         to at least one role.
     */
    public final boolean login() {
        LOGGER.entering("RoleLoginModule", "login");
        succeeded = true;
        LOGGER.exiting("RoleLoginModule", "login", succeeded);
        return succeeded;
    }

    /**
     * Method to commit the authentication process (phase 2).
     *
     * This method adds the <code>RolePrincipals</code> to the current subject.
     *
     * @exception LoginException if the commit fails
     * @return true if this method succeeded, or false if this
     *              <code>LoginModule</code> should be ignored.
     */
    public final boolean commit() throws LoginException {
        LOGGER.entering("RoleLoginModule", "commit");
        try {
            if (succeeded) {
                try {
                    List<java.security.Principal> plist = new ArrayList<java.security.Principal>(subject.getPrincipals());

                    for (java.security.Principal p : plist) {
                        Role role = getRole(p);
                        if (role != null) {
                            if (LOGGER.isLoggable(Level.FINE)) {
                                LOGGER.log(Level.FINE, "roleLoginModule.prinicipalInRole",
                                        new Object [] {
                                    p, role.getName()
                                });
                            }
                            principals.add(role);
                        }
                    }
                    boolean hasNoRoles = true;
                    for (Role role : principals) {
                        RolePrincipal rolePrincipal = new RolePrincipal(role.getName());
                        if (!subject.getPrincipals().contains(rolePrincipal)) {
                            LOGGER.log(Level.FINE, "roleLoginModule.addRole", role.getName());
                            subject.getPrincipals().add(rolePrincipal);
                            hasNoRoles = false;
                        }
                    }
                    if(hasNoRoles) {
                        // We add the nobody role to the subject
                        LOGGER.log(Level.FINE, "roleLoginModule.addRole", RoleConstants.NOBODY_ROLE);                        
                        subject.getPrincipals().add(new RolePrincipal(RoleConstants.NOBODY_ROLE));
                    }
                    commitSucceeded = true;
                } catch(RuntimeException ex) {
                    LOGGER.throwing("RoleLoginModule", "commit", ex);
                    throw ex;
                }
            } else {
                commitSucceeded = false;
            }
            LOGGER.exiting("RoleLoginModule", "commit", commitSucceeded);
            return commitSucceeded;
        } catch(Throwable ex) {
            LogRecord lr = new LogRecord(Level.WARNING, "roleLoginModule.unexpectedError");
            lr.setParameters(new Object [] { "commit", ex.getLocalizedMessage() });
            lr.setThrown(ex);
            lr.setResourceBundle(ResourceBundle.getBundle(BUNDLE));
            LOGGER.log(lr);
            return false;
        }
    }

    /**
     * Method to abort the authentication process (phase 2).
     *
     * @exception LoginException if the abort fails
     *
     * @return true if this method succeeded, or false if this
     *         <code>LoginModule</code> should be ignored.
     */
    public final boolean abort() throws LoginException {
        LOGGER.entering("RoleLoginModule", "abort");
        if (!succeeded) {
            return false;
        } else if (succeeded && commitSucceeded) {
            logout();
        } else {
            succeeded = false;
        }
        LOGGER.exiting("RoleLoginModule", "abort");
        return true;
    }

    /**
     * Method which logs out a <code>Subject</code>.
     *
     * @exception LoginException if the logout fails
     *
     * @return true if this method succeeded, or false if this
     *         <code>LoginModule</code> should be ignored.
     */
    public final boolean logout() throws LoginException {
        LOGGER.entering("RoleLoginModule", "logout");
        for (Role role : principals) {
            if (LOGGER.isLoggable(Level.FINE)) {
                LOGGER.log(Level.FINE, "roleLoginModule.removeRole", role);
            }
            RolePrincipal rolePrincip = new RolePrincipal(role.getName());
            subject.getPrincipals().remove(rolePrincip);
        }
        succeeded = false;
        commitSucceeded = false;
        principals.clear();
        LOGGER.entering("RoleLoginModule", "exiting");
        return true;
    }

}

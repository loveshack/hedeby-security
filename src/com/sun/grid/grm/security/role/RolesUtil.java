/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.security.role;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.config.security.HedebySecurity;
import com.sun.grid.grm.config.security.Principal;
import com.sun.grid.grm.config.security.Role;
import com.sun.grid.grm.security.ui.GetSecurityCommand;
import com.sun.grid.grm.ui.Result;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.management.remote.JMXServiceURL;

/**
 * Helper Utility class that contains static methods for operating
 * on Roles.
 *
 * <p>This class implements a cache for the roles definition of CS. The life time
 *    of the information is one minute.</p>
 *
 */
public class RolesUtil {

    private static final String BUNDLE = "com.sun.grid.grm.security.role.messages";
    private static final Logger log = Logger.getLogger(RolesUtil.class.getName(), BUNDLE);
    /**
     *   Timeout for the cache elements in milliseconds
     *   (1 minute)
     */
    public static final long CACHE_TIMEOUT = 1 * 60 * 1000;
    private static final Map<JMXServiceURL, CacheElem> cache = new HashMap<JMXServiceURL, CacheElem>();

    /** Method for retrieving Roles information from CS
     * @param env Execution environment of system
     * @throws GrmException when the operation of retrieving data
     * from CS occurs
     * @return list of roles
     */
    public static List<Role> getRoles(ExecutionEnv env) throws GrmException {
        log.entering(RolesUtil.class.getName(), "getRoles", env);
        List<Role> ret = getCacheElem(env).getRoles();
        log.exiting(RolesUtil.class.getName(), "getRoles", ret);
        return ret;
    }
    
    private static CacheElem getCacheElem(ExecutionEnv env) {
        CacheElem elem = null;
        synchronized (cache) {
            elem = cache.get(env.getCSURL());
            if (elem == null) {
                elem = new CacheElem(env);
                cache.put(env.getCSURL(), elem);
            }
        }
        return elem;
    }
    
    /**
     * Invalidates the roles cache for an execution environment
     * 
     * Package private for testing
     * 
     * @param env the execution environment
     */
    static void invalidate(ExecutionEnv env) {
        getCacheElem(env).invalidate();
    }
    
    /**
     * Clear the complete roles cache.
     * 
     * Package private for testing
     */
    static void clear() {
        synchronized(cache) {
            cache.clear();
        }
    }

    private static class CacheElem {

        private long lastUpdate;
        private List<Role> roles;
        private final ExecutionEnv env;

        public CacheElem(ExecutionEnv env) {
            this.env = env;
        }
        
        private void invalidate() {
            synchronized(this) {
                roles = null;
            }
        }

        private static boolean equals(Principal p1, Principal p2) {
            
            if(p1.getClassname().equals(p2.getClassname())) {
                
                if(p1.isSetName()) {
                    if(p2.isSetName()) {
                        if(!p1.getName().equals(p2.getName())) {
                            return false;
                        }
                    } else {
                        return false;
                    }
                } else {
                    if(p1.isSetName()) {
                        return false;
                    }
                }
                
                if(p1.isSetRegexp()) {
                    if(p2.isSetRegexp()) {
                        if(!p1.getRegexp().equals(p2.getRegexp())) {
                            return false;
                        }
                    } else {
                        return false;
                    }
                } else {
                    if(p1.isSetRegexp()) {
                        return false;
                    }
                }
            } else {
                return false;
            }
            return true;
        }
        
        private static boolean equals(Role r1, Role r2) {
            if(r1.getName().equals(r2.getName())) {
                for(Principal p1: r1.getPrincipal()) {
                    Principal p2 = null;
                    for(Principal tmpp: r2.getPrincipal()) {
                        if(equals(p1, tmpp)) {
                            p2 = tmpp;
                            break;
                        }
                    }
                    if(p2 == null) {
                        return false;
                    }
                }
            } else {
                return false;
            }
            return true;
        }
        
        private static boolean equals(List<Role> rl1, List<Role> rl2) {
            
            if(rl1 == null) {
                if(rl2 == null) {
                    return true;
                } else {
                    return false;
                }
            } else if (rl2 == null) {
                return false;
            }
            
            for(Role r1: rl1) {
                Role r2 = null;
                for(Role tr: rl2) {
                    if(r1.getName().equals(tr)) {
                        r2 = tr;
                        break;
                    }
                }
                if(r2 == null) {
                    return false;
                }
                if(!r1.equals(r2)) {
                    return false;
                }
            }
            return true;
        }
        

        public List<Role> getRoles() throws GrmException {
            log.entering(CacheElem.class.getName(), "getRoles");
            List<Role> ret = null;
            boolean needUpdate = false;
            synchronized(this) {
                if(roles == null) {
                    needUpdate = true;
                    log.log(Level.FINE, "roleUtil.init");
                } else if (lastUpdate + CACHE_TIMEOUT < System.currentTimeMillis() ) {
                    needUpdate  = true;
                    log.log(Level.FINE, "roleUtil.reinit");
                } else {
                    ret = roles;
                }
            }
            if (needUpdate) {
                GetSecurityCommand cmd = new GetSecurityCommand();
                Result<HedebySecurity> res = null;
                List<Role> oldRoles = null;
              
                synchronized(this) {
                    oldRoles = roles;
                }
                try {
                    res = env.getCommandService().execute(cmd);
                    synchronized(this) {
                        roles = res.getReturnValue().getRole();                    
                        ret = roles;
                        lastUpdate = System.currentTimeMillis();
                    }
                    // We log the roles definition if it has changed
                    if (log.isLoggable(Level.FINE) && !equals(oldRoles, ret)) {
                        for (Role role : roles) {
                            log.log(Level.FINE, "roleUtil.roleHeader", role.getName());
                            for (Principal pr : role.getPrincipal()) {
                                log.log(Level.FINE, "roleUtil.roleMember",
                                        new Object [] {
                                    pr.getClassname(), pr.getName(), pr.getRegexp()
                                });
                            }
                        }
                    }
                    
                } catch (GrmException ex) {
                    synchronized(this) {
                        roles = null;
                    }
                    throw ex;
                }

            }
            log.exiting(CacheElem.class.getName(), "getRoles", ret);
            return ret;
        }
        
    }

    /** Checker method that checks if principal (java.security.Principal) is a member
     * of current roles
     * @param role It's Role object in which check if Principal is member
     * @param principal is a principal Object - java.security.Principal
     * @return true if principal is member otherwise false
     */
    public static boolean isMember(Role role, java.security.Principal principal) {
        for (Principal pr : role.getPrincipal()) {
            if (matches(pr, principal)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Helper method checking if principal (java.security.Principal)
     * matches Hedeby system defined principal (com.sun.grid.grm.config.security.Principal)
     * @param princip com.sun.grid.grm.config.security.Principal
     * @param principal java.security.Principal
     * @return true if matches, false if doesn't match
     */
    private static boolean matches(Principal princip, java.security.Principal principal) {

        if (principal.getClass().getName().equals(princip.getClassname())) {
            String principalName = principal.getName();
            String principName = princip.getName();
            Pattern regexp = null;
            if (princip.getRegexp() != null) {
                regexp = Pattern.compile(princip.getRegexp());
            }
            if (principName != null) {
                return principName.equals(principalName);
            } else if (regexp != null) {
                return regexp.matcher(principalName).matches();
            }
        }

        return false;
    }
}

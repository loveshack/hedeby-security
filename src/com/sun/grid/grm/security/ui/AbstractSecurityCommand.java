/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.security.ui;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.PathUtil;
import com.sun.grid.grm.config.security.CAComponentConfig;
import com.sun.grid.grm.security.SecurityConstants;
import com.sun.grid.grm.ui.AbstractSystemCommandDelegator;
import com.sun.grid.grm.ui.component.GetConfigurationCommand;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

/**
 *  Abstract base class for commands which has to deal with the CA component.
 *
 *  The <code>AbstractSecurityCommand</code> is a <code>AbstractSystemCommandDelegator</code>.
 *  It provides already the classpath for the new class loader has access
 *  to the following jar files:
 *
 *  <pre>
 *     &lt;dist_dir&gt;/lib/sdm-security-impl.jar
 *     $SGE_ROOT/lib/juti.jar
 *  </pre>
 * @param T The result type of the command
 */
public abstract class AbstractSecurityCommand<T> extends AbstractSystemCommandDelegator<T> {
    
    /**
     * Get the classpath which contains <code>sdm-security-impl.jar</code>
     * and <code>juti.jar</code> from the Sun Grid Engine distribution.
     * 
     * To get the path to <code>juti.jar</code> this method loads the CA
     * CA component configuration from the configuration service. The path
     * to <code>juti.jar</code> is derived from the path the the sge_ca script.
     * @param env the execution env.
     *
     * @return The classpath for the security command
     * @throws com.sun.grid.grm.GrmException If the ca component configuration can not be loaded from the
     * configuration service
     */
    protected URL[] getClasspath(ExecutionEnv env) throws GrmException {
        
        GetConfigurationCommand<CAComponentConfig> cmd = new GetConfigurationCommand<CAComponentConfig>(SecurityConstants.CA_KEY);
        CAComponentConfig caConfig = env.getCommandService().<CAComponentConfig>execute(cmd).getReturnValue();
        
        String sgeCAScript = caConfig.getSgeCaScript();
        
        if (sgeCAScript == null) {
            throw new GrmException("Error: sgeCAScript cannot be null");
        }
        File script = new File(sgeCAScript);
        if(!script.exists()) {
            throw new GrmException("Error: sge_ca script not found (" + script + ")");
            
        }
        File sgeRoot = script.getAbsoluteFile().getParentFile().getParentFile().getParentFile();
        
        try {
            return new URL[] {
                new URL(PathUtil.getDistLibURL() + "/sdm-security-impl.jar"),
                new File(sgeRoot, "lib" + File.separatorChar + "juti.jar").toURI().toURL()
            };
        } catch (MalformedURLException ex) {
            throw new IllegalStateException("File.toURL reported error", ex);
        }
    }
    
}

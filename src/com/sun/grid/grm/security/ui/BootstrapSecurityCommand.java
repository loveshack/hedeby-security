/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.security.ui;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.PreferencesType;
import com.sun.grid.grm.config.security.CAComponentConfig;
import com.sun.grid.grm.security.ca.impl.GrmCAComponentContainer;
import com.sun.grid.grm.ui.LocalCommand;
import com.sun.grid.grm.ui.MacroCommand;
import com.sun.grid.grm.ui.component.AddComponentWithConfigurationCommand;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.Platform;
import java.io.File;
import java.util.Map;

/**
 * The <CODE>BoostrapSecurity</CODE> command initializes the certificate authority
 * for the Hedeby system or creates the security relevant files.
 *
 * <p>This command is a macro command which includes the following sub commands:</p>
 *
 * <ul>
 *    <li>{@link  com.sun.grid.grm.security.ui.CreateSecurityDirectoriesCommand CreateSecurityDirectoriesCommand}</li> 
 *    <li>{@link  com.sun.grid.grm.security.ui.CreateJAASConfigCommand          CreateJAASConfigCommand}</li> 
 *    <li>{@link  com.sun.grid.grm.security.ui.CreateSecurityDirectoriesCommand CreateSecurityDirectoriesCommand}</li> 
 *    <li>{@link  com.sun.grid.grm.security.ui.InitCACommand                    InitCACommand}</li> 
 *    <li>{@link  com.sun.grid.grm.security.ui.CreateSecurityConfigCommand      CreateSecurityConfigCommand}</li> 
 *    TODO (RH) add missing commands
 * </ul>
 *
 *
 * <p>To be pluggable the <code>BootstrapSecurityCommand</code> if configured with
 * a map of parameters. This map must contain the following elements:</p>
 *
 * <table border="1">
 *   <tr>
 *     <th>Key</th><th>Value</th>
 *   </tr>
 *   <tr>
 *     <td><code>SGE_ROOT</code></td>
 *     <td>String which contains the path to the Sun Grid Engine distribution.
 *         It necessary to find <code>juti.jar</code> an the <code>sge_ca</code>
 *         script.
 *    </td>
 *   </tr>
 *   <tr>
 *     <td><code>ADMIN_USER</code></td>
 *     <td>username of the admin user. This user will be the owner of all
 *         directories which are created by this command.
 *     </td>
 *   </tr>
 * </table>
 *
 * <p>Additionally parameters for the 
 *   {@link com.sun.grid.grm.security.ui.InitCACommand InitCACommand}
 *   are necessary.</p>
 *
 * <H3>Usage</H3>
 *
 * <pre>
 *    Map&lt;String, Object&gt; params = new HashMap&lt;String, Object&gt;();
 *          
 *    params.put("SGE_ROOT", sgeRoot);
 *    params.put("CA_ADMIN_EMAIL", caAdminMail);
 *    params.put("CA_COUNTRY", caCountry);
 *    params.put("CA_LOCATION", caLocation);
 *    params.put("CA_ORG", caOrg);
 *    params.put("CA_ORG_UNIT", caOrgUnit);
 *    params.put("CA_STATE", caState);
 *
 *    BoostrapSecurityCommand cmd = new BoostrapSecurityCommand(params);
 *   
 *    CommandService.execute(env, cmd);
 *
 * </pre>
 */
public class BootstrapSecurityCommand extends MacroCommand<Void> implements LocalCommand<Void> {
    
    private final static long serialVersionUID = -2009042301L;
    
    private final File sgeCAScript;
    private final File sgeRoot;
    private final String adminUser;
    
    final static String BUNDLE = "com.sun.grid.grm.security.ui.messages";
    
    /**
     * Creates a new instance of BootstrapSecurityCommand.
     *
     * @param params map with the configuration parameters.
     * @throws com.sun.grid.grm.GrmException If the configuration parameters are not correct
     */
    public BootstrapSecurityCommand(Map<String, Object> params) throws GrmException {
        
        String sgeRootStr = (String)params.get("SGE_ROOT");
        if(sgeRootStr == null) {
            throw new GrmException("BootstrapSecurityCommand.error.sge_root_mandatory", BUNDLE);
        }
        sgeRoot = new File(sgeRootStr);
        if(!getSgeRoot().isDirectory()) {
            throw new GrmException("BootstrapSecurityCommand.error.sge_root_not_dir", BUNDLE, sgeRootStr);
        }
        sgeCAScript = new File(getSgeRoot(), "util/sgeCA/sge_ca".replace('/', File.separatorChar));
        if(!getSgeCAScript().exists()) {
            throw new GrmException("BootstrapSecurityCommand.error.sge_ca_script_not_found", BUNDLE, getSgeCAScript());
        }
        String aAdminUser = (String)params.get("ADMIN_USER");
        if(Platform.getPlatform().isSuperUser()) {
            if(aAdminUser == null) {
                throw new GrmException("BootstrapSecurityCommand.error.admin_user_mandatory", BUNDLE);
            }
        } else if(aAdminUser == null) {
            aAdminUser = System.getProperty("user.name");
        }
        this.adminUser = aAdminUser;
        
        if (!params.containsKey("COMP_NAME") || !params.containsKey("JVM_NAME")) {
            throw new GrmException("BootstrapSecurityCommand.error.missing_comp_jvm", BUNDLE);
        }
        String name = (String)params.get("COMP_NAME");
        String jvm  = (String)params.get("JVM_NAME");
        
        CAComponentConfig config = new CAComponentConfig();
        config.setAdminUser(aAdminUser);
        config.setCaHost(Hostname.getLocalHost().getHostname());
        config.setSgeCaScript(sgeCAScript.getAbsolutePath());

        
        String [] adminUsers;
        
        // On non windows platform root is always admin user
        if (Platform.isWindowsOs() || "root".equals(aAdminUser) 
            // If system is installed in user preferences we can
            // not create the keystore for user root
            || PreferencesType.USER.equals(params.get("PREFS_TYPE"))) {
            adminUsers = new String [] { aAdminUser };
        } else {
            adminUsers = new String []  { aAdminUser, "root" };
        }
        
        CreateSecurityConfigCommand secConfCmd = new CreateSecurityConfigCommand();
        
        for(String user: adminUsers) {
            secConfCmd.addAdminUser(user);
        }
       
        commands.add(new CreateSecurityDirectoriesCommand(aAdminUser));
        commands.add(new CreateJAASConfigCommand(sgeRoot, aAdminUser));
        commands.add(new InitCACommand(config, sgeRoot, params));
        commands.add(secConfCmd);

        // Add the ca component
        AddComponentWithConfigurationCommand addCACompCommand = 
                AddComponentWithConfigurationCommand.newInstanceForSingleton()
                .classname(GrmCAComponentContainer.class.getName())
                .componentName(name)
                .config(config)
                .hostname(config.getCaHost())
                .jvmName(jvm);


        commands.add(addCACompCommand);

        // Create the keystores for the admin users
        for(String user: adminUsers) {
            commands.add(new CreateUserKeystoreCommand(user));
        }
        
        // create private keys and certificates for all daemons
        commands.add(new CreateAllDaemonsCommand());
        
        // store the keystores of all daemons
        commands.add(new StoreAllDaemonKeyStoresCommand());
    }
    
    /**
     * Get the path to the sge_ca script.
     *
     * @return The path to the sge_ca script
     */
    public File getSgeCAScript() {
        return sgeCAScript;
    }

    /**
     * Get the path to <code>SGE_ROOT</code>.
     *
     * @return path to sge distribution directory (<CODE>$SGE_ROOT</CODE>)
     */
    public File getSgeRoot() {
        return sgeRoot;
    }
    
}

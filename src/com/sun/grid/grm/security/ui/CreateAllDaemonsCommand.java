/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.security.ui;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.config.common.GlobalConfig;
import com.sun.grid.grm.config.common.JvmConfig;
import com.sun.grid.grm.ui.AbstractSystemCommand;
import com.sun.grid.grm.ui.MacroCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.component.GetGlobalConfigurationCommand;
import com.sun.grid.grm.ui.impl.CommandResult;
import java.util.ArrayList;
import java.util.List;

/**
 *  This Command craetes the keystores for all jvms.
 */
public class CreateAllDaemonsCommand extends AbstractSystemCommand<List<JvmConfig>> {
    
    private MacroCommand<Void> macro;
    
    /**
     * Execute this command.
     *
     * @param env   the execution env
     * @throws com.sun.grid.grm.GrmException if not all daemons could be created
     * @return result with the list of jvm configs
     */
    public Result<List<JvmConfig>> execute(ExecutionEnv env) throws GrmException {
        
        macro = new MacroCommand<Void>();
        
        GetGlobalConfigurationCommand cmd = new GetGlobalConfigurationCommand();
        GlobalConfig global = env.getCommandService().<GlobalConfig>execute(cmd).getReturnValue();
        List<JvmConfig> configs = new ArrayList<JvmConfig>(global.getJvm());
        for(JvmConfig jvmConfig: configs) {
            macro.addCommand(new CreateDaemonCommand(jvmConfig));
        }
        
        macro.execute(env);
        
        return new CommandResult<List<JvmConfig>>(configs);
    }

    /**
     *  Undo this command.
     * @param env the execution env 
     */
    public void undo(ExecutionEnv env) {
        if(macro != null) {
            macro.undo(env);
            macro = null;
        }
    }
    
    
    
}

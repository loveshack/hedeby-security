/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.security.ui;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.PathUtil;
import com.sun.grid.grm.config.security.Principal;
import com.sun.grid.grm.ui.AbstractLocalCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import com.sun.grid.grm.util.FileUtil;
import com.sun.grid.grm.util.Platform;
import com.sun.grid.grm.util.Printer;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.util.HashMap;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *  This command creates the jaas config file for a hedeby system.
 *
 *  <p>This command copies the jaas.config.template 
 *  (<codo>&lt;dist&gt;/util/jaas.config.template</code>) to
 *  <code>&lt;local_spool_dir&gt/security/jaas.config</code> and
 *  replaces the following patterns:</p>
 *
 *  <table border="1">
 *     <tr>
 *         <th>Pattern</th><th>Description</th>
 *     </tr>
 *     <tr> 
 *         <td><code>@@@SDM_SYSTEM_NAME@@@</code></td>
 *         <td>Will be replaced by the name of the hedeby system</td>
 *     </tr>
 *     <tr>
 *         <td>@@@ADDITIONAL_LOGIN_MODULES@@@</td>
 *         <td>Will be replaces by the login module definitions which has been
 *             added with the <code>addLoginModule</code> method.
 *     </tr>
 *  </table>
 *
 *  <p>Per default the <code>com.sun.grid.security.login.UnixLoginModule</code> is
 *  included into the jaas.config. Parameters for this login module are</p>
 *
 *  <ul>
 *    <li><code>authmethod="system"</code></li>
 *    <li><code>sge_root=&ltpath to sge_root&gt;</code></li>
 *  </ul>
 *
 *  <p>The <code>UnixLoginModule</code> comes from <code>juti.jar</code> it has to wrapped
 *     by a <code>DelegatingLoginModule</code></p>.
 *
 *  @see com.sun.grid.grm.security.login.DelegatingLoginModule
 */
public class CreateJAASConfigCommand extends AbstractLocalCommand<Void> {

    private final static String BUNDLE = "com.sun.grid.grm.security.ui.messages";
    private final static Logger log = Logger.getLogger(CreateJAASConfigCommand.class.getName(), BUNDLE);
    
    
    private final File sgeRoot;
    private final String adminUser;
    
    
    /** Stores the list of login modules */
    private final List<LoginModuleDefinition> loginModules = new LinkedList<LoginModuleDefinition>();
    
    
    /**
     * Creates a new instance of CreateJAASConfigCommand.
     *
     * @param sgeRoot    path to the Sun Grid Engine distribution
     * @param adminUser  name of the admin user of the CA
     */
    public CreateJAASConfigCommand(File sgeRoot, String adminUser) {
        this.sgeRoot = sgeRoot;
        this.adminUser = adminUser;
        addSystemLoginModule(LoginModuleFlag.requisite);
    }
    
    
    private LoginModuleDefinition createDelegationLoginModule(LoginModuleFlag flag) {
        LoginModuleDefinition lm = new LoginModuleDefinition("com.sun.grid.grm.security.login.DelegatingLoginModule", flag);
        
        lm.params().put("classname", "com.sun.grid.security.login.UnixLoginModule");
        try {
            lm.params().put("classpath", new File(sgeRoot, "lib" + File.separatorChar + "juti.jar").toURI().toURL().toString());
        } catch (MalformedURLException ex) {
            throw new IllegalStateException("File.toURL reported error", ex);
        }
        return lm;
    }
    
    /**
     *  Add a system login module to jaas config.
     *  @param flag the <code>LoginModuleFlag</code>
     */
    private void addSystemLoginModule(LoginModuleFlag flag) {
        LoginModuleDefinition lm = createDelegationLoginModule(flag);
        lm.params().put("auth_method", "system");
        lm.params().put("sge_root", sgeRoot.getPath());
        loginModules.add(lm);
    }
    
    /**
     * Add a generic login module. This login module will be written into
     * jaas.config.
     *
     * @param classname classname of the LoginModule
     * @param flag    flag for the login module (required, requisite, ..)
     * @param params  parameters for the login module
     */
    public void addLoginModule(String classname, LoginModuleFlag flag, Map<String, String> params) {
        LoginModuleDefinition lm = new LoginModuleDefinition(classname, flag);
        if(params != null) {
            lm.params().putAll(params);
        }
        loginModules.add(lm);
    }
    
    
    /**
     * Creates the <code>jaas.config</code> file in <code>&lt;local_spool_dir&gt/security</code>.
     *
     * @param env the execution env
     * @throws com.sun.grid.grm.GrmException if the <code>jaas.config</code> file can not be created
     * @return Void result
     */
    public Result<Void> execute(ExecutionEnv env) throws GrmException {
        File templateDir = PathUtil.getTemplatePath(env);  
        
        File jaasTemplate = new File(templateDir, "jaas.config.template");
        File jaasFile = PathUtil.getSecurityJaasConfig(env);
        
        // Define the list of additional login modules
        StringWriter sw = new StringWriter();
        Printer p = new Printer(sw);
        try {
            p.indent();
            for(LoginModuleDefinition lm: loginModules) {
                p.print(lm.getClassName());
                p.print(" ");
                p.println(lm.getFlag().toString());
                p.indent();
                if(lm.params() != null) {
                    boolean firstParam = true;
                    for(String param: lm.params().keySet()) {
                        if(firstParam) {
                            firstParam = false;
                        } else {
                            p.println();
                        }
                        p.print(param);
                        p.print("=\"");
                        p.print(lm.params().get(param));
                        p.print("\"");
                    }
                }
                p.println(";");
                p.deindent();
            }
        } finally {
            p.close();
        }
        Map<String, String> patterns = new HashMap<String, String>();
        patterns.put("@@@SDM_SYSTEM_NAME@@@", env.getSystemName());
        patterns.put("@@@ADDITIONAL_LOGIN_MODULES@@@", sw.getBuffer().toString());
        try {
            FileUtil.replace(jaasTemplate, jaasFile, patterns);
        } catch (IOException ex) {
            throw new GrmException("CreateJAASConfigCommand.ex.jaas", ex, BUNDLE,
                                   jaasFile, ex.getLocalizedMessage());
        }
        
        try {
            Platform.getPlatform().chown(jaasFile, adminUser, false);
        } catch (Exception ex) {
            throw new GrmException("CreateJAASConfigCommand.ex.chown", ex, BUNDLE, jaasFile, ex.getLocalizedMessage());
        }
        
        return CommandResult.VOID_RESULT;
    }
    
    /**
     *  Flag for login modules.
     */
    public enum LoginModuleFlag {
        required,
        requisite,
        sufficient,
        optional
    }
    
    
    private class LoginModuleDefinition {
        private final String className;
        private final LoginModuleFlag flag;
        private final Map<String, String>  params = new HashMap<String, String>();
        
        
        public LoginModuleDefinition(String classname, LoginModuleFlag flag) {
            this.className = classname;
            this.flag  = flag;
        }
        
        
        public Map<String,String> params() {
            return params;
        }
        
        public String getClassName() {
            return className;
        }
        
        public LoginModuleFlag getFlag() {
            return flag;
        }
    }
    
    
}

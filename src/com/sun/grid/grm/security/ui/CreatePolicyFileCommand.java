/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.security.ui;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.PathUtil;
import com.sun.grid.grm.ui.AbstractLocalCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import com.sun.grid.grm.util.FileUtil;
import com.sun.grid.grm.util.I18NManager;
import com.sun.grid.grm.util.Platform;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This UI command creates the java policy file &lt;local_spool_dir&gt;/security/java.policy
 * by copying the java.policy.template.
 */
public class CreatePolicyFileCommand extends AbstractLocalCommand<Void> {

    private final String owner;
    private final static String BUNDLE = "com.sun.grid.grm.security.ui.messages";
    private final static Logger log = Logger.getLogger(CreatePolicyFileCommand.class.getName(), BUNDLE);
    private String orgFileContent;
    private String orgOwner;
    private File createdFile;

    /**
     * Create a new instance of CreatePolicyFileCommand
     *
     * @param owner username of the owner of the java policy file
     */
    public CreatePolicyFileCommand(String owner) {
        this.owner = owner;
    }

    /**
     * Copies the java.policy.template file from the dist directory
     * to &lt;local_spool_dir&gt;/security/java.policy.
     * If the java.policy file already exists, a backup of the file is made in
     * memory (for undo).
     *
     * @param env the execution env
     * @return void result
     * @throws com.sun.grid.grm.GrmException of the java.policy file could not be created
     */
    public Result<Void> execute(ExecutionEnv env) throws GrmException {
        File policyFileTemplate = new File(PathUtil.getTemplatePath(env), "java.policy.template");
        File policyFile = PathUtil.getSecurityJavaPolicy(env);

        if (policyFile.exists()) {
            try {
                orgFileContent = FileUtil.read(policyFile);
                orgOwner = Platform.getPlatform().getFileOwner(policyFile);
            } catch (IOException ex) {
                throw new GrmException("CreatePolicyFileCommand.error.backup.policy", ex, BUNDLE, ex.getLocalizedMessage());
            }
        }
        try {
            Platform.getPlatform().copy(policyFileTemplate, policyFile);
            createdFile = policyFile;
        } catch (Exception ex) {
            throw new GrmException("CreatePolicyFileCommand.error.create.policy", ex, BUNDLE, ex.getLocalizedMessage());
        }

        try {
            Platform.getPlatform().chown(policyFile, owner);
        } catch (Exception ex) {
            throw new GrmException("CreatePolicyFileCommand.error.chown.policy", ex, BUNDLE, ex.getLocalizedMessage());
        }

        return CommandResult.VOID_RESULT;
    }

    /**
     * Delete the java.policy file if it has been created in the execute method.
     * If a java.policy file exited the content will be restored
     * @param env the execution env
     */
    @Override
    public void undo(ExecutionEnv env) {
        if (createdFile != null) {
            if (createdFile.exists() && !createdFile.delete()) {
                log.log(Level.WARNING, "CreatePolicyFileCommand.undo.del.failed", createdFile);
            }
            createdFile = null;
            if (orgFileContent != null) {
                File policyFile = PathUtil.getSecurityJavaPolicy(env);
                try {
                    log.log(Level.FINE, "CreatePolicyFileCommand.undo.restore", policyFile);
                    FileUtil.write(orgFileContent, policyFile);
                } catch (IOException ex) {
                    log.log(Level.WARNING, I18NManager.formatMessage("CreatePolicyFileCommand.undo.restore.failed", BUNDLE, ex.getLocalizedMessage()), ex);
                }

                if (orgOwner != null) {
                    try {
                        String newOwner = Platform.getPlatform().getFileOwner(policyFile);
                        if (!orgOwner.equals(newOwner)) {
                            if (log.isLoggable(Level.FINE)) {
                                log.log(Level.FINE, "CreatePolicyFileCommand.undo.chown", new Object [] { policyFile, orgOwner });
                            }
                            Platform.getPlatform().chown(policyFile, orgOwner, false);
                        }
                    } catch (Exception ex) {
                        log.log(Level.WARNING, I18NManager.formatMessage("CreatePolicyFileCommand.undo.restore.nochown", BUNDLE, ex.getLocalizedMessage()), ex);
                    }
                }
            }
        }
    }

}

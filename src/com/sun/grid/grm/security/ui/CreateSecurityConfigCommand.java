/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.security.ui;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.BootstrapConstants;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.config.security.HedebySecurity;
import com.sun.grid.grm.config.security.Principal;
import com.sun.grid.grm.config.security.Role;
import com.sun.grid.grm.security.GrmDaemonPrincipal;
import com.sun.grid.grm.security.SecurityPathUtil;
import com.sun.grid.grm.security.role.RoleConstants;
import com.sun.grid.grm.ui.AbstractSystemCommand;
import com.sun.grid.grm.ui.CSModifyCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import javax.naming.Context;
import javax.naming.NamingException;

/**
 *  This command creates the default hedeby security configuration and stores in
 *  in CS.
 *
 *  <p>The following information will be included into the security configuration:</p>
 * 
 *  <ul>
 *     <li>The ca certificate</li>
 *     <li>The certificate revocation list (only if one exists)</li>
 *     <li>The definition of the daemon role</li>
 *     <li>The definition of the administrator role</li>
 *  </ul>
 *
 *  <p>Per default the adminstrator role contains only the admin user (parameter
 *  of the constructor of this command). The daemon role contains any 
 *  principal object of class <code>GrmDaemonPrincipal</code>.</p>
 *
 *  <p><b>Attention:</b> Despite this command is a <code>SystemCommand</code> it <b>must
 *  not</b> used in a running system. It can only be invoked during the installation.</p>
 *
 *  @see com.sun.grid.grm.security.GrmDaemonPrincipal
 */
public class CreateSecurityConfigCommand extends AbstractSystemCommand<Void> implements CSModifyCommand{
    
    private static final long serialVersionUID = -2007080301L;
    
    private final static String BUNDLE = "com.sun.grid.grm.security.ui.messages";
    
    /** Stores principals that are aministrators */
    private final Set<Principal> admins = new HashSet<Principal>();
    
    /**
     *   Add an admin user to the list of users.
     *   @param  username of the admin user
     */
    public void addAdminUser(String username) {
        
        // check if user is already in the list and don't add if so
        for(Principal p: admins) {
            if(p.getName().equals(username)) {
                return;
            }
        }
        
        // UnixLoginModules adds com.sun.grid.security.login.UserPrincipal
        // to subject
        Principal princip = new Principal();
        princip.setClassname(RoleConstants.UNIX_LOGIN_MODULE_PRINCIPAL);
        princip.setName(username);
        admins.add(princip);
        princip = new Principal();
        // GrmCATrustManagerLoginModule adds com.sun.grid.grm.security.login.UserPrincipal
        // to subject
        princip.setClassname(RoleConstants.GRM_CA_LOGIN_MODULE_PRINCIPAL);
        princip.setName(username);
        admins.add(princip);
    }
    

    /**
     * execute this command
     * @param env the execution env
     * @throws com.sun.grid.grm.GrmException on any error
     * @return void result
     */
    public Result<Void> execute(ExecutionEnv env) throws GrmException {
        
        HedebySecurity secConfig = new HedebySecurity();
        
        //Here we have the security object present in CS
        List<Role> newRoles = new LinkedList<Role>();
        
        Principal princip = new Principal();
        princip.setClassname(GrmDaemonPrincipal.class.getName());
        princip.setRegexp(".*");
        Role daemonRole = new Role();
        daemonRole.setName(RoleConstants.DAEMON_ROLE);
        daemonRole.getPrincipal().add(princip);
        newRoles.add(daemonRole);

        Role adminRole = new Role();
        adminRole.setName(RoleConstants.ADMIN_ROLE);
        adminRole.getPrincipal().addAll(admins);
        
        newRoles.add(adminRole);
        //Reset Roles in HedebySecurity and set new ones
        secConfig.getRole().addAll(newRoles);
        
        // Update certificates
        SecurityPathUtil.updateCerts(env, secConfig);
        
        Context ctx = env.getContext();
        try {
            ctx.bind(BootstrapConstants.CS_SECURITY, secConfig);
        } catch (NamingException ex) {
            throw new GrmException("createSecurity.error.bind", BUNDLE, ex);
        }
        
        return CommandResult.VOID_RESULT;
    }

    public List<String> getListOfModifyNames() {
        return Collections.<String>singletonList(BootstrapConstants.CS_SECURITY);
    }
    
}

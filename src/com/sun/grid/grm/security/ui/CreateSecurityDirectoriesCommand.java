/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.security.ui;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.PathUtil;
import com.sun.grid.grm.security.SecurityPathUtil;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import com.sun.grid.grm.ui.install.AbstractCreateDirectoriesCommand;
import com.sun.grid.grm.util.Platform;
import java.io.File;

/**
 *  This command creates all directories for the Hedeby security module.
 *
 *  <p>In detail the following directories are created in <code>&lt;local_spool_dir&gt;</code>:</p>
 *
 *  <pre>
 *      security/ca
 *      security/ca/ca_top
 *      security/ca/ca_local_top
 *      security/daemons
 *      security/users
 *  </pre>
 *
 *
 *  <p>Additionally the template for the java policy files is copied from
 *  <code>&lt;dist_dir&gt;/util/templates/java.policy.template</code> to
 *  <code>&lt;local_spool_dir&gt;/security/java.policy</code>.</p>
 *
 *  <p>The owner of the files and directories can be specified in the constructor.</p>
 *
 */
public class CreateSecurityDirectoriesCommand extends AbstractCreateDirectoriesCommand {

    private CreatePolicyFileCommand createPolicyFileCmd;
    
    /**
     * Creates a new instance of CreateSecurityDirectoriesCommand.
     * 
     * @param owner the owner of the files and directories
     */
    public CreateSecurityDirectoriesCommand(String owner) {
        super(owner);
    }
    
    /**
     * Creates directories and files for the Hedeby security.
     * 
     * @param env the execution env
     * @throws com.sun.grid.grm.GrmException if not all files or directories can be created
     * @return Void result
     */
    public Result<Void> execute(ExecutionEnv env) throws GrmException {
        
        // Create all necessary directories for security
        File caTop = SecurityPathUtil.getCaTop(env);
        mkdir(caTop.getParentFile());
        mkdir(caTop);
        mkdir(SecurityPathUtil.getCaLocalTop(env));
        mkdir(PathUtil.getTmpDirForComponent(env, "ca"));
        mkdir(SecurityPathUtil.getUserKeyStorePath(env));
        mkdir(SecurityPathUtil.getDaemonKeystorePath(env));

        CreatePolicyFileCommand policyFileCmd = new CreatePolicyFileCommand(getOwner());
        policyFileCmd.execute(env);

        this.createPolicyFileCmd = policyFileCmd;
        
        return CommandResult.VOID_RESULT;
    }

    /**
     * Deletes all created directories and the java.policy file
     * @param env the execution env
     */
    @Override
    public void undo(ExecutionEnv env) {
        if (createPolicyFileCmd != null) {
            createPolicyFileCmd.undo(env);
            createPolicyFileCmd = null;
        }
        super.undo(env);
    }


    
}

/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.security.ui;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;

/*
 * This command uses delegate class to perform operation of creation of certificates 
 * and private keys for user
 * For delegate class @see com.sun.grid.grm.security.ui.CreateUserCommandDelegate
 */
public class CreateUserCommand extends AbstractSecurityCommand<Void> {

    private final static long serialVersionUID = -200803060L;
    private String userName;
    private String email;

    /**
     * Creates a new instance of CreateUserCommand.
     *
     * @param userName name of the user
     * @param email of the user
     */
    public CreateUserCommand(String userName, String email) {
        this.userName = userName;
        this.email = email;
    }

    /**
     *  Get the name of the user
     *  @return the name of the user
     */
    public String getUserName() {
        return userName;
    }

    /**
     *  Get the email of the user
     *  @return the email of the user
     */
    public String getEmail() {
        return email;
    }

    /**
     * Get the name of the delegate class.
     * @param Env the execution env
     * @return the name of the delegate class
     */
    protected String getDelegateClassname(ExecutionEnv Env) throws GrmException {
        return getClass().getName() + "Delegate";
    }
}


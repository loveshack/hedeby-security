/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.security.ui;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.security.ca.GrmCA;
import com.sun.grid.grm.security.ca.GrmCAException;
import com.sun.grid.grm.ui.CommandDelegate;
import com.sun.grid.grm.ui.CommandDelegator;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *   Command delegate which create the private key of a user.
 *
 *   Requires a class loader which has access to sdm-security-impl.jar and juti.jar
 */

public class CreateUserCommandDelegate extends SecurityCommandDelegate implements CommandDelegate<Void> {
    
    private final static String BUNDLE="com.sun.grid.grm.security.ui.messages";
    private final static Logger log = Logger.getLogger(CreateUserCommandDelegate.class.getName(), BUNDLE);
    
    /**
     * Execute this command.
     * @param env   the execution env
     * @param delegator  the delegator
     * @throws com.sun.grid.grm.GrmException  on any error
     * @return void result
     */
    public Result<Void> execute(ExecutionEnv env, CommandDelegator<Void> delegator) throws GrmException {

        GrmCA ca = getCA(env);
        CreateUserCommand command = (CreateUserCommand)delegator;
        
        String email = command.getEmail();
        String username = command.getUserName();

        try {
            ca.createUser(username, username, email);
        } catch (GrmCAException e) {
            GrmException ce = new GrmException("command.error.remote_error", e,
                    BUNDLE, e.getLocalizedMessage());

            throw ce;
        } catch (Exception e) {
            GrmException ce =
                    new GrmException("command.error.unexpected_error", e,
                    BUNDLE, e.getLocalizedMessage());
   
            throw ce;
        }
        return CommandResult.VOID_RESULT;
    }
    
    /**
     * Undo all changes.
     *
     * @param env     the execution env
     * @param delegator  the delegator
     */
    public void undo(ExecutionEnv env, CommandDelegator<Void> delegator) {
        log.log(Level.WARNING, "undo of command " + getClass().getName() + " not implemented");
    }
    
    
}


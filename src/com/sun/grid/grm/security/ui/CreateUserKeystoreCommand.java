/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.security.ui;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.security.SecurityPathUtil;
import com.sun.grid.grm.security.ca.KeyStoreWrapper;
import com.sun.grid.grm.ui.AbstractLocalCommand;
import com.sun.grid.grm.ui.Command;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;

/**
 * This command creates the keystore of a user.
 *
 * <p>This command uses <code>GetUserKeyStoreCommand</code> to get the keystore
 * of a user and <code>StoreKeyStoreCommand</code> to store it in a file.</p>
 *
 * <p>The filename of the keystore is evaluated with 
 * {@link com.sun.grid.grm.security.SecurityPathUtil#getUserKeystoreFile 
 * SecurityPathUtil.getUserKeystoreFile}.</p>
 *
 * @see GetUserKeyStoreCommand
 * @see StoreKeyStoreCommand
 */
public class CreateUserKeystoreCommand extends AbstractLocalCommand<Void> {
    
    private final String username;
    private Command undo = null;
    
    /** 
     * Creates a new instance of CreateUserKeystoreCommand 
     *  @param username  the name of the user
     */
    public CreateUserKeystoreCommand(String username) {
        this.username = username;
    }

    /**
     * Reads the user keystore and stores it in a file.
     *
     * @param env the execution env
     * @throws com.sun.grid.grm.GrmException on any error
     * @return void result
     */
    public Result<Void> execute(ExecutionEnv env) throws GrmException {
        
        GetUserKeyStoreCommand cmd = new GetUserKeyStoreCommand(username);
        
        Result<KeyStoreWrapper> res = env.getCommandService().execute(cmd);
        
        StoreKeyStoreCommand storeCmd = new StoreKeyStoreCommand(res.getReturnValue().getKeyStore(),
                                                                new char[0],
                                                                SecurityPathUtil.getUserKeystoreFile(env, username),
                                                                username,
                                                                false);

        storeCmd.execute(env);
        undo = storeCmd;
        return CommandResult.VOID_RESULT;
    }

    @Override
    public void undo(ExecutionEnv env) {
        if (undo != null) {
            undo.undo(env);
            undo = null;
        }
    }
}

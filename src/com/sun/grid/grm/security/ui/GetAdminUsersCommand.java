/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.security.ui;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.config.security.HedebySecurity;
import com.sun.grid.grm.config.security.Principal;
import com.sun.grid.grm.config.security.Role;
import com.sun.grid.grm.security.role.RoleConstants;
import com.sun.grid.grm.ui.AbstractLocalCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import java.util.HashSet;
import java.util.Set;

/**
 *  This command gets the admin user set from CS.
 *
 */
public class GetAdminUsersCommand extends AbstractLocalCommand<Set<String>> {
    
    private final static String BUNDLE = "com.sun.grid.grm.security.ui.messages";
    
    public Result<Set<String>> execute(ExecutionEnv env) throws GrmException {
        
        GetSecurityCommand gsCmd = new GetSecurityCommand();
        
        HedebySecurity sec = env.getCommandService().execute(gsCmd).getReturnValue();
        
        Role adminRole = null;
        
        for(Role tmpRole: sec.getRole()) {
            if(tmpRole.getName().equals(RoleConstants.ADMIN_ROLE)) {
                adminRole = tmpRole;
                break;
            }
        }
        
        if(adminRole == null) {
            // Strange no admin role available, this should not happen
            throw new GrmException("adminUser.roleNotFound", BUNDLE, RoleConstants.ADMIN_ROLE);
        }
        
        Set<String> ret = new HashSet<String>(adminRole.getPrincipal().size());
        
        for(Principal p: adminRole.getPrincipal()) {
            if(p.getClassname().equals(RoleConstants.GRM_CA_LOGIN_MODULE_PRINCIPAL)
               && p.isSetName() ) {
               ret.add(p.getName());
            }
        }
        
        return new CommandResult<Set<String>>(ret);
    }
    
}

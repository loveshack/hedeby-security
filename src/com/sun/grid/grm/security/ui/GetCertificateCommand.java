/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.security.ui;

import com.sun.grid.grm.security.CertificateType;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import java.security.cert.X509Certificate;

/*
 * This command uses delegate class to perform operation of getting the certificates.
 * For delegate class @see com.sun.grid.grm.security.ui.GetCertificateCommandDelegate
 */
public class GetCertificateCommand extends AbstractSecurityCommand<X509Certificate> {
    
    private final static long serialVersionUID = -200807150L;
    
    private String name;
    private CertificateType type;
    /**
     * Creates a new instance of CreateDaemonCommand
     * @param name name of user or daemon
     * @param type user or daemon
     */
    public GetCertificateCommand(String name, CertificateType type) {
        this.setName(name);
        this.setType(type);
    }
    
    protected String getDelegateClassname(ExecutionEnv Env) throws GrmException {
        return getClass().getName()+"Delegate";
    }
    
    /* 
     * Getter for name
     * @return name - name of user or daemon
     */
    public String getName() {
        return name;
    }
    
    /* 
     * Getter for type
     * @return type - user or daemon
     */
    public CertificateType getType() {
        return type;
    }

    /*
     * Setter for name
     * @param name - name of user or daemon
     */
    public void setName(String name) {
        this.name = name;
    }

    /*
     * Setter for type
     * @param type - user or daemon
     */
    public void setType(CertificateType type) {
        this.type = type;
    }
    
}

/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.security.ui;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.security.ca.GrmCA;
import com.sun.grid.grm.ui.CommandDelegate;
import com.sun.grid.grm.ui.CommandDelegator;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import java.security.cert.X509Certificate;

/**
 *   Command delegate which get certificates for user or daemon.
 *
 *   Requires a class loader which has access to sdm-security-impl.jar and juti.jar
 */
public class GetCertificateCommandDelegate extends SecurityCommandDelegate implements CommandDelegate<X509Certificate> {

    /**
     * Execute this command.
     * @param env   the execution env
     * @param delegator  the delegator
     * @throws com.sun.grid.grm.GrmException  on any error
     * @return void result
     */
    public Result<X509Certificate> execute(ExecutionEnv env, CommandDelegator<X509Certificate> delegator) throws GrmException {
        
        GrmCA ca = getCA(env);

        GetCertificateCommand command = (GetCertificateCommand) delegator;
        
        String name = command.getName();
        X509Certificate cert = null;
        
        switch(command.getType()) {
            case DAEMON:
                cert = ca.getDaemonCertificate(name);
                break;
            case USER:
                cert = ca.getCertificate(name);
                break;
            case CA:
                cert = ca.getCACertificate();
                break;
            default:
                throw new IllegalStateException("Unknown certificate type " + command.getType());
        }
        return new CommandResult<X509Certificate>(cert);
    }
    
    /**
     * Undo all changes.
     *
     * @param env     the execution env
     * @param delegator  the delegator
     */
    public void undo(ExecutionEnv env, CommandDelegator<X509Certificate> delegator) {
        // Nothing to UNDO
    }
    
    
}



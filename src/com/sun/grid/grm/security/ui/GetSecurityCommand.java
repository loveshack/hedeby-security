/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.security.ui;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.config.security.HedebySecurity;
import com.sun.grid.grm.ui.AbstractSystemCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import javax.naming.Context;
import javax.naming.NamingException;

/*
 * Security command for obtaining security from CS
 */
public class GetSecurityCommand extends AbstractSystemCommand<HedebySecurity>{

    private final String BUNDLE = "com.sun.grid.grm.security.ui.messages";
    private final String SECURITY = "security";
    /**
     * Creates a new instance of SetSecurityCommand
     */
    public GetSecurityCommand() {
    }

    /**
     * Retrieve Security object from CS
     * @param env  the execution env
     * @throws com.sun.grid.grm.GrmException if the security cannot be obtained
     * @return The result of the operation <code>HedebySecurity</code>
     */
    public Result<HedebySecurity> execute(ExecutionEnv env) throws GrmException {
        HedebySecurity security = null;
        Context ctx = env.getContext();
        try {
            security = (HedebySecurity)ctx.lookup(SECURITY);
        } catch (NamingException ex) {
            this.undo(env);
            throw new GrmException("command.error.failed_load_security",ex, BUNDLE);
        }
        Result<HedebySecurity> res = new CommandResult<HedebySecurity>();
        res.setReturnValue(security);
        return res;
    }

    
    
}

/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.security.ui;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.security.ca.KeyStoreWrapper;
import com.sun.grid.grm.ui.AbstractLocalCommandDelegator;
import com.sun.grid.grm.util.GrmClassLoaderFactory;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

/*
 * This command uses delegate class to perform operation of getting the user keystore.
 * For delegate class @see com.sun.grid.grm.security.ui.GetUserKeyStoreCommandDelegate
 */
public class GetUserKeyStoreCommand extends AbstractSecurityCommand<KeyStoreWrapper>{
    
    private String name = null;
    private char[] ksPW = null;
    private char[] pkPW = null;
    
    private final static char [] NULL_PW = new char[0];
    
    /**
     * Creates a new instance of GetUserKeyStoreCommand.
     *
     * @param name name of the user
     */
    public GetUserKeyStoreCommand(String name) {
        this(name, NULL_PW, NULL_PW);
    }
    
    /**
     * Creates a new instance of GetUserKeyStoreCommand.
     *
     * @param username name of the user
     * @param ksPW password for keystore
     * @param pkPW password for private key for user
     */
    public GetUserKeyStoreCommand(String username, char[] ksPW, char[] pkPW) {
        this.name = username;
        this.ksPW = ksPW;
        this.pkPW = pkPW;
    }
    /**
     * Get the name of the delegate class.
     * @param env the execution env
     * @return the name of the delegate class
     */
    protected String getDelegateClassname(ExecutionEnv env) throws GrmException {
        return getClass().getName()+"Delegate";
    }
    
    /*
     * Getter for name of user
     * @return name of the user
     */
    public String getName() {
        return name;
    }
    
    /*
     * Setter for name of user
     * @param name - name of user
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /*
     * Getter for the keystore password
     * @return keystore password
     */
    public char[] getKsPW() {
        return ksPW;
    }
    
    /*
     * Setter for keystore password
     * @param ksPW - password for user keystore
     */
    public void setKsPW(char[] ksPW) {
        this.ksPW = ksPW;
    }
    
    /*
     * Getter for private key password
     * @return users private key password
     */
    public char[] getPkPW() {
        return pkPW;
    }
    
    /*
     * Setter for private key password
     * @param pkPW - user's private key password
     */
    public void setPkPW(char[] pkPW) {
        this.pkPW = pkPW;
    }
    
}

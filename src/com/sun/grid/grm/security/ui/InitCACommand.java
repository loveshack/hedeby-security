/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.security.ui;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.PathUtil;
import com.sun.grid.grm.config.security.CAComponentConfig;
import com.sun.grid.grm.ui.AbstractLocalCommandDelegator;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

/**
 * The <code>InitCACommand</code> initialzizes certificate authority for
 * a Hedeby system.
 *
 * <p>It use the <code>GridCA</code> class from juti.jar. The real work has to
 * be done in <code>InitCACommandDelegate</code>.</p>
 *
 * <p>To be plugable this command has to be configured with map. The following
 * entries must be defined in this map:</p>
 *
 * <table border="1">
 *    <tr>
 *        <th>Key</th><th>Value</th>
 *    </tr>
 *    <tr>
 *        <td><code>CA_ADMIN_EMAIL</code></td>
 *        <td>email address of the CA administrator</td>
 *    </tr>
 *    <tr>
 *        <td><code>CA_COUNTRY</code></td>
 *        <td>Country of the CA</td>
 *    </tr>
 *    <tr>
 *        <td><code>CA_LOCATION</code></td>
 *        <td>location of the CA (city)</td>
 *    </tr>
 *    <tr>
 *        <td><code>CA_ORG</code></td>
 *        <td>organization of the CA</td>
 *    </tr>
 *    <tr>
 *        <td><code>CA_ORG_UNIT</code></td>
 *        <td>organization unit of the CA</td>
 *    </tr>
 *    <tr>
 *        <td><code>CA_STATE</code></td>
 *        <td>state of the CA</td>
 *    </tr>
 * </table>
 *
 */
public class InitCACommand extends AbstractLocalCommandDelegator<Void> {
    
    private final static String BUNDLE = "com.sun.grid.grm.security.ui.messages";
    
    private final CAComponentConfig caComponentConfig;
    private final File sgeRoot;

    private final String adminEMail;
    private final String country;
    private final String location;
    private final String org;
    private final String orgUnit;
    private final String state;

    
    /** Creates a new instance of InitCACommand. 
     *
     *   @param caComponentConfig  configuration of the CA component
     *   @param sgeRoot            path the the Sun Grid Engine distribution
     *   @param params             map with the configuration parameters
     */
    public InitCACommand(CAComponentConfig caComponentConfig, File sgeRoot, Map<String, Object> params) throws GrmException {
        
        if(caComponentConfig == null) {
            throw new IllegalArgumentException("caComponentConfig cannot be null");
        }
        this.caComponentConfig = caComponentConfig;
        this.sgeRoot = sgeRoot;
        
        adminEMail = getParam(params, "CA_ADMIN_EMAIL");
        country = getParam(params, "CA_COUNTRY");
        location = getParam(params, "CA_LOCATION");
        org = getParam(params, "CA_ORG");
        orgUnit = getParam(params, "CA_ORG_UNIT");
        state = getParam(params, "CA_STATE");
        
    }
    
    private static String getParam( Map<String, Object> params, String name) throws GrmException {
        String ret = (String)params.get(name);
        if(ret == null) {
            throw new GrmException("{0} param is mandatory", BUNDLE, name);
        }
        return ret;
    }

    /**
     * Get the classpath for this command which contains the url
     * to <code>hedeby-security-impl.jar</code> and <code>juti.jar</code>.
     *
     * @param env  the execution env
     * @return arrays with the URLs for the classpath
     */
    protected URL[] getClasspath(ExecutionEnv env) {
        
        try {
            return new URL[] {
                new URL(PathUtil.getDistLibURL() + "/sdm-security-impl.jar"),
                new File(sgeRoot, "lib" + File.separatorChar + "juti.jar").toURI().toURL()
            };
        } catch (MalformedURLException ex) {
            throw new IllegalStateException("File.toURL reported error", ex);
        }
    }
    
    /**
     * Get the name of the delegate class.
     *
     * @param env the execution env (not used)
     * @return the name of the delegate class
     */
    protected String getDelegateClassname(ExecutionEnv env) {
        return getClass().getName()+"Delegate";
    }

    /**
     * Get the configuration of the CA component.
     *
     * @return the configuration of the CA component
     */
    public CAComponentConfig getCaComponentConfig() {
        return caComponentConfig;
    }

    /**
     * Get the path to sge root.
     *
     * @return path to sge root
     */
    public File getSgeRoot() {
        return sgeRoot;
    }

    /**
     * Get the email address of the CA administrator.
     *
     * @return email address of the CA administrator
     */    
    public String getAdminEMail() {
        return adminEMail;
    }

    /**
     * Get the country of the CA.
     *
     * @return country of the CA
     */    
    public String getCountry() {
        return country;
    }

    /**
     * Get the location of the CA.
     *
     * @return location of the CA
     */    
    public String getLocation() {
        return location;
    }

    /**
     * Get the organization of the CA.
     *
     * @return organization of the CA
     */    
    public String getOrg() {
        return org;
    }

    /**
     * Get the organization unit of the CA.
     *
     * @return organization unit of the CA
     */    
    public String getOrgUnit() {
        return orgUnit;
    }

    /**
     * Get the state of the CA.
     *
     * @return state of the CA
     */    
    public String getState() {
        return state;
    }

}

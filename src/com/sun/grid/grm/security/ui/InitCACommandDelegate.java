/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.security.ui;

import com.sun.grid.ca.GridCAException;
import com.sun.grid.ca.InitCAParameters;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.security.ca.impl.GrmCAImpl;
import com.sun.grid.grm.security.ca.impl.GrmCAServiceConfigHelper;
import com.sun.grid.grm.ui.CommandDelegate;
import com.sun.grid.grm.ui.CommandDelegator;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Delegate for the <code>InitCACommand</code>.
 *
 * <p>Has to run in a class loader which has access to <code>sdm-security-impl.jar</code>
 * and <code>juti.jar</code>.</p>
 *
 * <p>This delegate creates an instance of <code>GrmCAImpl</code> and calls the <code>init</code>
 * method of the object.</p>
 *
 * @see InitCACommand
 * @see com.sun.grid.grm.security.ca.impl.GrmCAImpl
 */
public class InitCACommandDelegate implements CommandDelegate<Void> {

    private final static String BUNDLE = "com.sun.grid.grm.security.ui.messages";
    private final static Logger log = Logger.getLogger(InitCACommandDelegate.class.getName(), BUNDLE);
    
    /**
     *  Initializes the CA.
     * 
     * @return void result
     * @param env the execution env
     * @param delegator must be instance of <code>InitCACommand</code>
     * @throws com.sun.grid.grm.GrmException if the CA can not be initialized
     */
    public Result<Void> execute(ExecutionEnv env, CommandDelegator<Void> delegator) throws GrmException {
        
        InitCACommand cmd = (InitCACommand)delegator;
        
        GrmCAServiceConfigHelper helper = new GrmCAServiceConfigHelper(env, cmd.getCaComponentConfig());
        
        GrmCAImpl ca = new GrmCAImpl(env, helper);
        
        InitCAParameters params = new InitCAParameters();
        
        try {
            params.setAdminEmailAddress(cmd.getAdminEMail());
            params.setCountry(cmd.getCountry());
            params.setLocation(cmd.getLocation());
            params.setOrganization(cmd.getOrg());
            params.setOrganizationUnit(cmd.getOrgUnit());
            params.setState(cmd.getState());
        } catch(GridCAException ex) {
            /* 
             * Do not path root cause to client, because client cannot load
             * GridCAException
             */           
            throw new GrmException(ex.getLocalizedMessage());
        }
        
        ca.init(params);
        
        return CommandResult.VOID_RESULT;
    }
    
    /**
     * Undo all changes.
     *
     * @param env     the execution env
     * @param delegator  the delegator
     */
    public void undo(ExecutionEnv env, CommandDelegator<Void> delegator) {
        log.log(Level.WARNING, "undo of command " + getClass().getName() + " not implemented");
    }
    
}

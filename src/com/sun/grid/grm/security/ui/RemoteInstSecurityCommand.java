/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.security.ui;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.config.security.CAComponentConfig;
import com.sun.grid.grm.security.SecurityConstants;
import com.sun.grid.grm.ui.AbstractLocalCommand;
import com.sun.grid.grm.ui.MacroCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.component.GetConfigurationCommand;
import java.io.File;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.sun.grid.grm.util.Platform;
import com.sun.grid.grm.bootstrap.PreferencesType;

/**
 * This command boostraps the security on a managed host.
 *
 * The following tasks are executed
 *
 * <ul>
 *    <li>The necessary directories for security are created</li>
 *    <li>The CA certificate and the CRL is copied from the master host</li>
 *    <li>The admin user keystore is stored in the local spool dir</li>
 *    <li>The keystores of all daemons are stored in the local spool dir</li>
 * </ul>
 */
public class RemoteInstSecurityCommand extends MacroCommand<Void> {
    
    private final static long serialVersionUID = -200807140L;
    private final static String BUNDLE = "com.sun.grid.grm.security.ui.messages";
    
    private final static Logger log = Logger.getLogger(RemoteInstSecurityCommand.class.getName(), BUNDLE);
    
    
    /**
     * Create a new instance of RemoteInstSecurityCommand
     * @param params the parameters for the remote security bootstrapping. This map must
     *               contain under the key ADMIN_USER the name of the user for the remote
     *               host
     * @throws com.sun.grid.grm.GrmException if the key ADMIN_USER has not been found in params
     *                       of the the value is not a string
     */
    public RemoteInstSecurityCommand(Map<String,Object> params) throws GrmException {

        String adminUser;
        try {
            adminUser = (String)params.get("ADMIN_USER");
        } catch(ClassCastException ex) {
            adminUser = null;
        }
        if (adminUser == null) {
            throw new GrmException("RemoteInstSecurityCommand.ex.missingAdminUser", BUNDLE);
        }
        commands.add(new PreCommand(adminUser));
        commands.add(new UpdateCACertCommand());
        //Fix for issue 688
        // On non windows platform root is always admin user
        if (Platform.isWindowsOs() || "root".equals(adminUser)
            // If system is installed in user preferences we can
            // not create the keystore for user root
            || PreferencesType.USER.equals(params.get("PREFS_TYPE"))) {
        } else {
            commands.add(new CreateUserKeystoreCommand("root"));
        }
        commands.add(new CreateUserKeystoreCommand(adminUser));
        commands.add(new StoreAllDaemonKeyStoresCommand());
        
    }

    /**
     *  Sub command for creating the necessary directories
     */
    public static class PreCommand extends AbstractLocalCommand<Void> {
        
        private final MacroCommand<Void> macro = new MacroCommand<Void>();
        private final String adminUser;
        
        public PreCommand(String adminUser) {
            this.adminUser = adminUser;
        }
        
        public Result<Void> execute(ExecutionEnv env) throws GrmException {
            
            GetConfigurationCommand<CAComponentConfig> cmd = new GetConfigurationCommand<CAComponentConfig>(SecurityConstants.CA_KEY);
            CAComponentConfig caConf = env.getCommandService().<CAComponentConfig>execute(cmd).getReturnValue(); 
            
            
            File caScript = new File(caConf.getSgeCaScript());
            
            // The SGE_ROOT directory is neccessary for username/password authentication
            // If the SGE_ROOT is different (e.g of managed host has SGE_ROOT not mounted)
            // username/password authentication will not work
            File sgeRoot = caScript.getAbsoluteFile().getParentFile().getParentFile().getParentFile();
            if (!sgeRoot.exists()) {
                File guessSgeRoot = new File("/opt/sge".replace('/', File.separatorChar));
                if (guessSgeRoot.exists()) {
                    log.log(Level.INFO, "RemoteInstSecurityCommand.guessSgeRoot", new Object [] { sgeRoot, guessSgeRoot});
                    sgeRoot = guessSgeRoot;
                } else {
                    log.log(Level.WARNING, "RemoteInstSecurityCommand.noSgeRoot", sgeRoot);
                }
            }
            
            macro.addCommand(new CreateSecurityDirectoriesCommand(adminUser));
            macro.addCommand(new CreateJAASConfigCommand(sgeRoot, adminUser));
            
            return macro.execute(env);
        }
        
        @Override
        public void undo(ExecutionEnv env) {
            if(macro != null) {
                macro.undo(env);
            }
        }
        
    }
}
    

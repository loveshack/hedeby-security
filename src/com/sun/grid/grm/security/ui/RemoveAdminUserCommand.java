/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.security.ui;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.BootstrapConstants;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.config.security.HedebySecurity;
import com.sun.grid.grm.config.security.Principal;
import com.sun.grid.grm.config.security.Role;
import com.sun.grid.grm.security.role.RoleConstants;
import com.sun.grid.grm.ui.AbstractSystemCommand;
import com.sun.grid.grm.ui.CSModifyCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

/**
 *  This command removes an administrator from the role definition in the security
 *  configuration.
 *
 */
public class RemoveAdminUserCommand extends AbstractSystemCommand<Void> implements CSModifyCommand{
    
    private final static String BUNDLE = "com.sun.grid.grm.security.ui.messages";
    private static final long serialVersionUID = -2007080301L;
    
    private String username;
    private UpdateSecurityCommand updateCommand;
    
    /** Creates a new instance of AddAdminUserCommand */
    public RemoveAdminUserCommand(String username) {
        this.username = username;
    }

    /**
     * Execute this command.
     *
     * @param env  the execution env
     * @throws com.sun.grid.grm.GrmException if the user can not be added.
     * @return void result
     */
    public Result<Void> execute(ExecutionEnv env) throws GrmException {
        
        GetSecurityCommand gsCmd = new GetSecurityCommand();
        
        HedebySecurity sec = gsCmd.execute(env).getReturnValue();
        
        Role adminRole = null;
        
        for(Role tmpRole: sec.getRole()) {
            if(tmpRole.getName().equals(RoleConstants.ADMIN_ROLE)) {
                adminRole = tmpRole;
                break;
            }
        }
        
        if(adminRole == null) {
            // Strange no admin role available, this should not happen
            throw new GrmException("adminUser.roleNotFound", BUNDLE, RoleConstants.ADMIN_ROLE);
        }
        
        // Check if the username is already defined as administrator
        ListIterator<Principal> iter = adminRole.getPrincipal().listIterator();
        int removeCount = 0;
        while(iter.hasNext() && removeCount < 2) {
           Principal p = iter.next();
           if((p.getClassname().equals(RoleConstants.GRM_CA_LOGIN_MODULE_PRINCIPAL)
              || p.getClassname().equals(RoleConstants.UNIX_LOGIN_MODULE_PRINCIPAL))
              && username.equals(p.getName())) {
               iter.remove();
               removeCount++;
           }
        }
        
        if(removeCount == 0) {
            throw new GrmException("adminUser.notDefined", BUNDLE, username);
        }
        
        updateCommand = new UpdateSecurityCommand(sec);

        updateCommand.execute(env);
        
        return CommandResult.VOID_RESULT;
    }

    /**
     * Undo the modifications in the security configuration
     * @param env the execution env
     */
    public void undo(ExecutionEnv env) {
        if(updateCommand != null) {
            updateCommand.undo(env);
            updateCommand = null;
        }
    }
    
    public List<String> getListOfModifyNames() {
        return Collections.<String>singletonList(BootstrapConstants.CS_SECURITY);
    }
}

/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.security.ui;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;

public class RenewCACommand extends AbstractSecurityCommand<Void>{
    
    private final static long serialVersionUID = -200807110L;
    
    private int days;
    
    /**
     * Creates a new instance of CreateDaemonCommand
     * @param days - for how long renew the ca cert
     */
    public RenewCACommand(int days) {
        this.setDays(days);
    }
    
    /*
     * Getter for days
     * @return days - for how long the ca cert will be renewed
     */
    public int getDays() {
        return days;
    }
    
    protected String getDelegateClassname(ExecutionEnv Env) throws GrmException {
        return getClass().getName()+"Delegate";
    }

    /*
     * Setter for days
     * @param days -  for how long the ca cert will be renewed
     */
    public void setDays(int days) {
        this.days = days;
    }
    
}

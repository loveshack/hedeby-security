/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.security.ui;

import com.sun.grid.grm.security.CertificateType;
import java.io.Serializable;

/**
 * Result a renewal of a certificate
 */
public class RenewCertificatResult implements Serializable {
    
    private final static long serialVersionUID = -200807150L;
    
    private final CertificateType type;
    private final String name;
    private final String message;
    private final Exception error;
    
    /**
     * Create a new instance of RenewCertificatResult.
     * 
     * @param type  the type of the certificate
     * @param name  the name of the certificate
     * @param message the message
     */
    public RenewCertificatResult(CertificateType type, String name, String message) {
        this(type, name, message, null);
    }
    
    /**
     * Create a new instance of RenewCertificatResult.
     * 
     * @param type  the type of the certificate
     * @param name  the name of the certificate
     * @param message the message
     * @param error the error
     */
    public RenewCertificatResult(CertificateType type, String name, String message, Exception error) {
        this.type = type;
        this.name = name;
        this.message = message;
        this.error = error;
    }

    /**
     * get the type of the certificate
     * @return the type of the certificate
     */
    public CertificateType getType() {
        return type;
    }

    /**
     * get the name of the certificate
     * @return the name of the certificate
     */
    public String getName() {
        return name;
    }

    /**
     * get the message
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * get the exception which has been occured during the renewal process
     * @return the error
     */
    public Exception getError() {
        return error;
    }

}

/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.security.ui;

import com.sun.grid.grm.security.CertificateType;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.PathUtil;
import com.sun.grid.grm.config.common.GlobalConfig;
import com.sun.grid.grm.config.common.JvmConfig;
import com.sun.grid.grm.config.security.CAComponentConfig;
import com.sun.grid.grm.security.SecurityConstants;
import com.sun.grid.grm.security.SecurityPathUtil;
import com.sun.grid.grm.security.ca.GrmCAException;
import com.sun.grid.grm.security.ca.KeyStoreWrapper;
import com.sun.grid.grm.ui.AbstractLocalCommand;
import com.sun.grid.grm.ui.CommandService;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.component.GetConfigurationCommand;
import com.sun.grid.grm.ui.component.GetGlobalConfigurationCommand;
import com.sun.grid.grm.ui.component.GetGlobalConfigurationCommand;
import com.sun.grid.grm.ui.impl.CommandResult;
import com.sun.grid.grm.util.Hostname;
import java.io.File;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This command renews the CA certificate and recreates all keystore for
 * the existing jvms and the admin user.
 * 
 * <p>The recreated keystore contains a certificate which is signed by the renewed
 * CA certificate.</p>
 * 
 * <p>This command should be executed locally on the master host. All jvms must be stopped</p>
 */
public class RenewCompleteCACommand extends AbstractLocalCommand<List<RenewCertificatResult>> {

    private final static String BUNDLE = "com.sun.grid.grm.security.ui.messages";
    private int days;

    /**
     * Create a new instanceof the RenewCompleteCACommand.
     * 
     * @param days number of days for the validity of the CA certificate
     */
    public RenewCompleteCACommand(int days) {
        this.days = days;
    }
    
    private int getValidityInDays(X509Certificate cert) {
        long diff = cert.getNotAfter().getTime() - System.currentTimeMillis();
        if (diff > 0) {
            return (int)(diff / (1000 * 60 * 60 * 24));
        } else {
            return 0;
        }
    }
    
    /**
     *   Renew the complete CA
     * 
     *   @param env the execution env
     *   @throws com.sun.grid.grm.GrmException if the command failed
     */
    public Result<List<RenewCertificatResult>> execute(ExecutionEnv env) throws GrmException {

        if(!env.getCSHost().equals(Hostname.getLocalHost())) {
            throw new GrmException("RenewCADaemonAndAdminUserCommand.ex.masterhost", BUNDLE, env.getCSHost());
        }
        CommandService cmdService = env.getCommandService();
        
        GetConfigurationCommand<CAComponentConfig> cacmd = new GetConfigurationCommand<CAComponentConfig>(SecurityConstants.CA_KEY);
        CAComponentConfig caConfig = caConfig = cmdService.<CAComponentConfig>execute(cacmd).getReturnValue();
        
        File caLocalTop = SecurityPathUtil.getCaLocalTop(env);
        File lockFile = new File(caLocalTop, "lock");
        if (!lockFile.canWrite()) {
           throw new  GrmException("RenewCADaemonAndAdminUserCommand.ex.perm", BUNDLE, System.getProperty("user.name"));
        }
        
        List<File> pidFiles = PathUtil.getPidFiles(env);
        if(!pidFiles.isEmpty()) {
            throw new GrmException("RenewCADaemonAndAdminUserCommand.ex.StopJVMs", BUNDLE, 
                                   PathUtil.getLocalSpoolRunPath(env));
        }
        
        GetGlobalConfigurationCommand gcc = new GetGlobalConfigurationCommand();
        GlobalConfig gc = null;
        try {
            gc = cmdService.execute(gcc).getReturnValue();
        } catch (GrmException ex) {
            throw new GrmCAException(ex.getLocalizedMessage(), ex);
        }
        
        GetCertificateCommand getAdminCertCmd = new GetCertificateCommand(caConfig.getAdminUser(), CertificateType.USER);
        // add command for getting daemon keystores
        X509Certificate adminCert = cmdService.execute(getAdminCertCmd).getReturnValue();
        
        int adminUserDays = getValidityInDays(adminCert);

        Map<String, Integer> daysMap = new HashMap<String, Integer>();

        for (JvmConfig jvm : gc.getJvm()) {
            GetCertificateCommand getDaemonCertCmd = new GetCertificateCommand(jvm.getName(), CertificateType.DAEMON);
            X509Certificate cert = cmdService.execute(getDaemonCertCmd).getReturnValue();
            daysMap.put(jvm.getName(), getValidityInDays(cert));
        }

        List<RenewCertificatResult> result = new ArrayList<RenewCertificatResult>(gc.getJvm().size()+2);
        
        RenewCACommand renewCACmd = new RenewCACommand(days);
        try {
            cmdService.execute(renewCACmd);
            result.add(new RenewCertificatResult(CertificateType.CA, null, "renewed"));
        } catch(GrmException ex) {
            result.add(new RenewCertificatResult(CertificateType.CA, null, ex.getLocalizedMessage(), ex));
            return new CommandResult<List<RenewCertificatResult>>(result);
        }
        
        for (JvmConfig jvm : gc.getJvm()) {
            Integer validDays = daysMap.get(jvm.getName());
            if (validDays != null && validDays.intValue() > 0) {
                try {
                    RenewDaemonCommand renewDaemonCmd = new RenewDaemonCommand(jvm.getName(), validDays.intValue());
                    cmdService.execute(renewDaemonCmd);
                    GetDaemonKeyStoreCommand getDaemonKsCmd = new GetDaemonKeyStoreCommand(jvm.getName());
                    KeyStoreWrapper ksw = cmdService.execute(getDaemonKsCmd).getReturnValue();
                    
                    StoreKeyStoreCommand storeDaemonKSCmd = new StoreKeyStoreCommand(ksw.getKeyStore(), new char[0],
                            SecurityPathUtil.getDaemonKeystoreFile(env, jvm.getName()), jvm.getUser(), false);
                    cmdService.execute(storeDaemonKSCmd);
                    result.add(new RenewCertificatResult(CertificateType.DAEMON, jvm.getName(), "certificate renewed and keystore updated"));
                } catch (GrmException ex) {
                    result.add(new RenewCertificatResult(CertificateType.DAEMON, jvm.getName(), ex.getLocalizedMessage(), ex));
                }
            }
        }

        try {
            RenewUserCommand renewUserCmd = new RenewUserCommand(caConfig.getAdminUser(), adminUserDays);
            cmdService.execute(renewUserCmd);
            
            GetUserKeyStoreCommand getUserKsCmd = new GetUserKeyStoreCommand(caConfig.getAdminUser());
            KeyStoreWrapper ksw = cmdService.execute(getUserKsCmd).getReturnValue();
            
            StoreKeyStoreCommand storeUserKsCmd = new StoreKeyStoreCommand(ksw.getKeyStore(), new char[0],
                    SecurityPathUtil.getUserKeystoreFile(env, caConfig.getAdminUser()), caConfig.getAdminUser(), false);
            cmdService.execute(storeUserKsCmd);
                    result.add(new RenewCertificatResult(CertificateType.USER, caConfig.getAdminUser(), "certificate renewed and keystore updated"));
        } catch (GrmException ex) {
            result.add(new RenewCertificatResult(CertificateType.USER, caConfig.getAdminUser(), ex.getLocalizedMessage(), ex));
        }
        
        return new CommandResult<List<RenewCertificatResult>>(result);
    }
    
    /**
     * get the number of days
     * @return the number of days
     */
    public int getDays() {
        return days;
    }

    /**
     * set the number of days
     * @param days  the number of days
     */
    public void setDays(int days) {
        this.days = days;
    }

}

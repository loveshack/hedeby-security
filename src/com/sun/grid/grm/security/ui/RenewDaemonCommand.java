/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.security.ui;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;

public class RenewDaemonCommand extends AbstractSecurityCommand<Void>{
    
    private final static long serialVersionUID = -200807110L;
    
    private int days = 0;
    private String name = null;
    
    /**
     * Creates a new instance of RenewDaemonCommand
     * @param name - name of the daemon
     * @param days - for how long renew daemon
     */
    public RenewDaemonCommand(String name, int days) {
        this.days = days;
        this.name = name;
    }
    
    /*
     * Getter for days
     * @return days - for how long the ca cert will be renewed
     */
    public int getDays() {
        return days;
    }
    
    /*
     * Setter for days
     * @param days -  for how long the ca cert will be renewed
     */
    public void setDays(int days) {
        this.days = days;
    }
    
     /*
     * Getter for name
     * @return name - name of daemon
     */
    public String getName() {
        return name;
    }
    
    /*
     * Setter for name
     * @param name - name of daemon
     */
    public void setName(String name) {
        this.name = name;
    }
    
    protected String getDelegateClassname(ExecutionEnv Env) throws GrmException {
        return getClass().getName()+"Delegate";
    }
}



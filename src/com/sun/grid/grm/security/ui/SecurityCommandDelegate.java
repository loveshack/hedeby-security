/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.security.ui;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ComponentService;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.JVMImpl;
import com.sun.grid.grm.config.security.CAComponentConfig;
import com.sun.grid.grm.security.SecurityConstants;
import com.sun.grid.grm.security.ca.GrmCA;
import com.sun.grid.grm.security.ca.impl.GrmCAImpl;
import com.sun.grid.grm.security.ca.impl.GrmCAServiceConfigHelper;
import com.sun.grid.grm.ui.component.GetConfigurationCommand;
import com.sun.grid.grm.util.Hostname;

/**
 *  Abstract base class for delegate classes which uses with
 *  the CA component.
 *
 */
public abstract class SecurityCommandDelegate {
    
    private final String BUNDLE="com.sun.grid.grm.security.ui.messages";
    
    
    private GrmCA ca;
    
    /**
     * Get the CA component
     * @param env  the execution env
     * @throws com.sun.grid.grm.GrmException if the CA component can not be
     *           contacted
     * @return the CA component
     */
    protected synchronized GrmCA getCA(ExecutionEnv env) throws GrmException {     
    
        if(ca == null) {
            if(JVMImpl.getInstance() == null) {
                GetConfigurationCommand<CAComponentConfig> cmd = new GetConfigurationCommand<CAComponentConfig>(SecurityConstants.CA_KEY);
                CAComponentConfig caConf = env.getCommandService().<CAComponentConfig>execute(cmd).getReturnValue();

                if(Hostname.getLocalHost().matches(caConf.getCaHost())) {
                    GrmCAServiceConfigHelper helper = new GrmCAServiceConfigHelper(env, caConf);
                    ca = new GrmCAImpl(env, helper).getCA();
                }
            } 
            if (ca == null) {
                ca = ComponentService.<GrmCA>getComponentByType(env, GrmCA.class);

                if(ca == null) {
                    throw new GrmException("command.error.no_ca", BUNDLE);
                }
            }
        }
        return ca;
    }
    
    /**
     * Set the ca component
     * @param ca the ca component
     */
    public void setCA(GrmCA ca) {
        this.ca = ca;
    }
     
}

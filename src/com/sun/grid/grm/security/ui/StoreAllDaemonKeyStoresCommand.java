/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.security.ui;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.config.common.JvmConfig;
import com.sun.grid.grm.security.SecurityPathUtil;
import com.sun.grid.grm.ui.AbstractLocalCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.component.GetJVMConfigurationsCommand;
import com.sun.grid.grm.ui.impl.CommandResult;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.List;

/**
 *  This command stores the keystores for a list of jvms.
 */
public class StoreAllDaemonKeyStoresCommand extends AbstractLocalCommand<Void> {
    
    private List<JvmConfig> jvms;
    private List<StoreKeyStoreCommand> finishedCommands;
    
    /** 
     *  Creates a new instance of StoreAllDaemonKeyStoresCommand
     *  The list of jvms will be read from the global configuration
     */
    public StoreAllDaemonKeyStoresCommand() {
        this(null);
    }
    
    
    /**
     * Create a new instance of StoreAllDaemonKeyStoresCommand.
     *
     * @param jvms for each jvm in this list a keystore will be created
     */
    public StoreAllDaemonKeyStoresCommand(List<JvmConfig> jvms) {
        this.jvms = jvms;
    }

    /**
     * Execute this command.
     *
     * @param env   the execution env
     * @throws com.sun.grid.grm.GrmException the execution env
     * @return void result
     */
    public Result<Void> execute(ExecutionEnv env) throws GrmException {
        
        if(jvms == null) {
            GetJVMConfigurationsCommand cmd = new GetJVMConfigurationsCommand();
            jvms = env.getCommandService().<List<JvmConfig>>execute(cmd).getReturnValue();
        }

        finishedCommands = new ArrayList<StoreKeyStoreCommand>(jvms.size());
        
        for(JvmConfig jvm: jvms) {
            
            GetDaemonKeyStoreCommand getCmd = new GetDaemonKeyStoreCommand(jvm.getName());

            KeyStore ks = env.getCommandService().execute(getCmd).getReturnValue().getKeyStore();
            
            StoreKeyStoreCommand cmd = new StoreKeyStoreCommand(ks, new char[0], 
                                           SecurityPathUtil.getDaemonKeystoreFile(env, jvm.getName()),
                                            jvm.getUser(),
                                            false);         
            cmd.execute(env);
            finishedCommands.add(cmd);
        }
        return CommandResult.VOID_RESULT;
    }

    /**
     * Undo this command.
     *
     * Deletes all previously created keystores
     *
     * @param env the execution env
     */
    public void undo(ExecutionEnv env) {
        if(finishedCommands != null) {
            for(StoreKeyStoreCommand cmd: finishedCommands) {
                cmd.undo(env);
            }
            finishedCommands = null;
        }
    }

    
}

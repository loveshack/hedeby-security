/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.security.ui;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.ui.AbstractLocalCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import com.sun.grid.grm.util.FileUtil;
import com.sun.grid.grm.util.I18NManager;
import com.sun.grid.grm.util.Platform;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.security.KeyStore;

/*
 * Local command that stores the given keystore in a file or prints it on the standart 
 * output.
 */
import java.util.logging.Level;
import java.util.logging.Logger;

public class StoreKeyStoreCommand extends AbstractLocalCommand<Void> {
    
    private final static String BUNDLE ="com.sun.grid.grm.security.ui.messages";
    private static final Logger log = Logger.getLogger(StoreKeyStoreCommand.class.getName(), BUNDLE);
    
    private KeyStore keystore = null;
    private char[] password = null;
    private File file = null;
    private boolean hex = false;
    private String owner;
    
    /* Creates a new instance of StoreKeyStoreCommand
     * @param keystore that will be stored
     * @param password that should be used for keystore
     * @param file to which the keystore should be written, if null the keystore is
     * written to standart output
     * @param owner of the file with keystore that should be set
     * @param hex if true, command writes the keystore as Hex
     */ 
    public StoreKeyStoreCommand(KeyStore keystore, char[] password, File file, String owner, boolean hex) {
        this.keystore = keystore;
        this.password = password;
        this.file = file;
        this.owner = owner;
        this.hex = hex;
    }

    /**
     * StoreKeyStore in file
     * @param env  the execution environment
     * @throws com.sun.grid.grm.GrmException if storing operation failed
     * @return void result
     */
    public Result<Void> execute(ExecutionEnv env) throws GrmException {
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        
        if (file != null && owner != null) {
            // The keystore should be stored into an file
            // If the owner is not known on the host print a warning
            // and do not create the keystore
            try {
                File tmpFile = File.createTempFile("testuser", "tmp");
                try {
                    Platform.getPlatform().chown(tmpFile, owner);
                } finally {
                    tmpFile.delete();
                }
            } catch (Exception ex) {
                log.log(Level.WARNING, I18NManager.formatMessage("StoreKeyStoreCommand.checkOwner", BUNDLE, file, ex.getLocalizedMessage()), ex);
                return CommandResult.VOID_RESULT;
            }
        }
        
        try {
            keystore.store(bout, password);
        } catch (Exception ex) {
            throw new GrmException("StoreKeyStoreCommand.error.serialize", ex, 
                                   BUNDLE, ex.getLocalizedMessage());
        }
        store(bout.toByteArray(), file);
        
        return CommandResult.VOID_RESULT;
    }

    @Override
    public void undo(ExecutionEnv env) {
        if (file != null && file.exists()) {
            if (!file.delete()) {
                log.log(Level.WARNING, "StoreKeyStoreCommand.delFile", file);
            }
        }
    }
    
    
    private void store(byte [] bytes, File file) throws GrmException {
        
        try {
            if(isHex()) {
                String str = FileUtil.byteArrayToHexString(bytes);
                if(file == null) {
                    System.out.println(str);
                } else {
                    FileWriter out = new FileWriter(file);
                    try {
                        out.write(str);
                    } finally {
                        out.close();
                    }
                }
            } else {
                OutputStream out = System.out;
                if(file != null) {
                    out = new FileOutputStream(file);
                }
                try {
                    out.write(bytes);
                } finally {
                    out.close();
                }
            }        
        } catch(IOException ex) {
            throw new GrmException("StoreKeyStoreCommand.error.store", ex, BUNDLE,
                    file, ex.getLocalizedMessage());
        }
        
        if(file != null) {
            try {
                Platform.getPlatform().chmod(file, "600");
            } catch (Exception ex) {
                throw new GrmException("StoreKeyStoreCommand.error.chmod", ex, BUNDLE, 
                                       file, ex.getLocalizedMessage());
            }
            try {
                if (owner != null) {
                    Platform.getPlatform().chown(file, owner);
                }
            } catch (Exception ex) {
                throw new GrmException("StoreKeyStoreCommand.error.chown", ex, BUNDLE, 
                                       file, ex.getLocalizedMessage());
            }
        }
    }

    /*
     * Getter for keystore
     * @return keystore - keystore given to command is returned
     */
    public KeyStore getKeystore() {
        return keystore;
    }

    /*
     * Setter for keystore
     * @param keystore - keystore to be set
     */
    public void setKeystore(KeyStore keystore) {
        this.keystore = keystore;
    }

    /*
     * Getter for password
     * @return password - password for keystore given to command is returned
     */
    public char[] getPassword() {
        return password;
    }

    /*
     * Setter for password
     * @param password - password for the keystore to be set
     */
    public void setPassword(char[] password) {
        this.password = password;
    }

    /*
     * Getter for file
     * @return file - file to which the kestore is written that was given to command
     */
    public File getFile() {
        return file;
    }

    /*
     * Setter for file
     * @param file - to which keystore should be written, if null keystore is printed
     * on standard output
     */
    public void setFile(File file) {
        this.file = file;
    }

    /*
     * Getter for hex
     * @return hex - if the keystore is going to be written in Hex return true
     */
    public boolean isHex() {
        return hex;
    }

    /*
     * Setter for hex
     * @param hex - if true keystore is witten in Hex
     */
    public void setHex(boolean hex) {
        this.hex = hex;
    }

    /*
     * Getter for owner
     * @return owner - owner given to command is returned, owner to which keystore
     * file should be set
     */
    public String getOwner() {
        return owner;
    }

    /*
     * Setter for owner
     * @param owner - to which keystore file should be set
     */
    public void setOwner(String owner) {
        this.owner = owner;
    }

}

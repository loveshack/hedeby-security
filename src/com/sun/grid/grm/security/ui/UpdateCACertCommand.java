/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.security.ui;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.security.SecurityPathUtil;
import com.sun.grid.grm.ui.AbstractLocalCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.util.FileUtil;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import com.sun.grid.grm.config.security.HedebySecurity;
import com.sun.grid.grm.ui.impl.CommandResult;


/**
 * This <code>Command</code> get the CA certificate from
 * CS and stores in at the local spool dir.
 */
public class UpdateCACertCommand extends AbstractLocalCommand<Object> {
    
    private static String BUNDLE = "com.sun.grid.grm.security.ui.messages";
    
    private static Logger log = Logger.getLogger(UpdateCACertCommand.class.getName(), BUNDLE);
    private File caCertFile;
    private String oldCert;
    
    /**
     * Update the CA certificate in the local spool dir
     * @param env  the execution env
     * @throws com.sun.grid.grm.GrmException if the CA certificate can not be written
     *                                       into the local spool dir
     * @return The result of the operation
     */
    public Result<Object> execute(ExecutionEnv env) throws GrmException {
        
        GetSecurityCommand getSecurityCmd = new GetSecurityCommand();
        
        HedebySecurity sec = env.getCommandService().execute(getSecurityCmd).getReturnValue();
        
        caCertFile = SecurityPathUtil.getCaCertFile(env);
        
        try {
            if(caCertFile.exists()) {
                oldCert = FileUtil.read(caCertFile);
            }
            FileUtil.write(sec.getCaCertificate(), caCertFile);
            log.log(Level.INFO, "UpdateCACertCommand.updated", caCertFile);            
        } catch(IOException ex) {
            throw new GrmException("UpdateCACertCommand.error.io", ex, BUNDLE, caCertFile, ex.getLocalizedMessage());
        }
        return new CommandResult<Object>();
    }

    /**
     *  Tries the restore the old content of the ca cert file.
     *
     */
    public void undo(ExecutionEnv env) {
        
        if(caCertFile != null && oldCert != null) {
            try {
                FileUtil.write(oldCert, caCertFile);
                log.log(Level.FINE,"UpdateCACertCommand.restored", caCertFile);
            } catch(IOException ex) {
                LogRecord lr = new LogRecord(Level.WARNING, "UpdateCACertCommand.error.undo");
                lr.setParameters( new Object [] { caCertFile, ex.getLocalizedMessage() });
                lr.setThrown(ex);
                log.log(lr);
            } 
        }
    }
}

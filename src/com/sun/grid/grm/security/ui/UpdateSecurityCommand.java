/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.security.ui;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.BootstrapConstants;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.config.security.HedebySecurity;
import com.sun.grid.grm.ui.AbstractSystemCommand;
import com.sun.grid.grm.ui.CSModifyCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import com.sun.grid.grm.ui.impl.FilterUtil;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.NamingException;

/*
 * Command for updating security object that is stored in CS,
 */

public class UpdateSecurityCommand extends AbstractSystemCommand<Void> implements CSModifyCommand {
    
    private final static long serialVersionUID = -200807151L;
    
    private HedebySecurity security;
    private final static String BUNDLE = "com.sun.grid.grm.security.ui.messages";
    private final String SECURITY = "security";
    private static final Logger log = Logger.getLogger(UpdateSecurityCommand.class.getName(), BUNDLE);
    private byte[] snapshot;
    /**
     * Creates a new instance of UpdateSecurityCommand
     *
     * @param security - <code>HedebySecurity</code> that should be used
     */
    public UpdateSecurityCommand(HedebySecurity security) {
        this.security = security;
    }
    
    /**
     * Getter for security object
     * @return security for this command
     */
    public HedebySecurity getSecurity() {
        return security;
    }
    
    /**
     * Setter for security
     * @param security <code>HedebySecurity</code> that should be used in command
     */
    public void setSecurity(HedebySecurity security) {
        this.security = security;
    }
    
    /**
     * Execute this command.
     * @param env   the execution env
     * @throws com.sun.grid.grm.GrmException on any error
     * @return void result
     */
    public Result<Void> execute(ExecutionEnv env) throws GrmException {
        snapshot = null;
        if (security == null) {
            throw new GrmException("command.error.parameter_not_set",BUNDLE, "security");
        }
        
        Context ctx = env.getContext();
        
        try {
            //save object for UNDO
            HedebySecurity sec = (HedebySecurity)ctx.lookup(SECURITY);
            snapshot = FilterUtil.<HedebySecurity>makeObjectSnapShot(sec);
            //update security
            ctx.rebind(SECURITY, security);
        } catch (NamingException ex) {
            this.undo(env);
            throw new GrmException("command.error.failed_update_security",ex, BUNDLE);
        }
        return CommandResult.VOID_RESULT;
    }  
    
    /**
     * Undo all changes.
     *
     * @param env     the execution env
     */
    public void undo(ExecutionEnv env) {
        if(snapshot!=null) {
            try {
                HedebySecurity sec = FilterUtil.<HedebySecurity>getObjectSnapShot(snapshot);
                Context ctx = env.getContext();
                //try to recreate object
                ctx.rebind(SECURITY, sec);
            } catch (NamingException ex) {
                log.log(Level.WARNING, "ui.command.undofailed");
            } catch (GrmException ex) {
                log.log(Level.WARNING, "ui.command.undofailed");
            }       
        }          
    }

    public List<String> getListOfModifyNames() {
        return Collections.<String>singletonList(BootstrapConstants.CS_SECURITY);
    }
}

/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.security.login;

import java.util.HashMap;
import java.util.Map;
import javax.security.auth.Subject;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.login.LoginException;
import junit.framework.*;

/**
 *
 */
public class AbstractLoginModuleTest extends TestCase {
    
    public AbstractLoginModuleTest(String testName) {
        super(testName);
    }

    public void testSucceededLoginCount() throws Exception {
        
        MyLoginModule lm1 = new MyLoginModule();
        MyLoginModule lm2 = new MyLoginModule();
        
        Map<String,Object> sharedState = new HashMap<String,Object>();
        
        lm1.initialize(null, null, sharedState, null);
        lm2.initialize(null, null, sharedState, null);

        assertTrue("login must be successfull", lm1.login());
        assertTrue("lm1 must add AbstractLoginModule.SUCCEEDED_LOGIN_COUNT to sharedState map"
                   , sharedState.containsKey(AbstractLoginModule.SUCCEEDED_LOGIN_COUNT));
        assertFalse("login must fail", lm2.login());
        
    }
    
    static class MyLoginModule extends AbstractLoginModule {
        
        protected void doInitialize(Subject subject, CallbackHandler callbackHandler, Map<String, ?> sharedState, Map<String, ?> options) {
        }

        protected boolean doLogin() throws LoginException {
            return true;
        }

        public boolean commit() throws LoginException {
            return true;
        }

        public boolean abort() throws LoginException {
            return true;
        }

        public boolean logout() throws LoginException {
            return true;
        }
        
        
        
    }
}

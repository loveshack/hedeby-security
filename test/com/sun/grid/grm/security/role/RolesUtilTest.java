/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.security.role;

import com.sun.grid.grm.CSMockupTestCase;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.BootstrapConstants;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.FactoryWithException;
import com.sun.grid.grm.config.security.HedebySecurity;
import com.sun.grid.grm.config.security.Principal;
import com.sun.grid.grm.config.security.Role;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.ResourceIdException;
import com.sun.grid.grm.security.SecurityContext;
import com.sun.grid.grm.security.ui.GetSecurityCommand;
import com.sun.grid.grm.security.ui.UpdateSecurityCommand;
import com.sun.grid.grm.ui.Command;
import com.sun.grid.grm.ui.CommandService;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.util.HostAndPort;
import com.sun.grid.grm.util.Hostname;
import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Logger;
import javax.management.remote.JMXServiceURL;
import javax.naming.Context;

/**
 * Tests the class RolesUtil
 */
public class RolesUtilTest extends CSMockupTestCase {
    
    private final static Logger log = Logger.getLogger(RolesUtilTest.class.getName());
    
    public RolesUtilTest(String testName) {
        super(testName);
    }            

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        
        start();

        Context ctx = getEnv().getContext();
        ctx.bind(BootstrapConstants.CS_SECURITY, new HedebySecurity());
        
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        RolesUtil.clear();
    }

    private void setRoles(String roleName, String principalClass, String principalName) throws GrmException {
        Role role = new Role();
        Principal p = new Principal();
        
        p.setClassname(principalClass);
        p.setName(principalName);
        role.setName(roleName);
        role.getPrincipal().add(p);
        
        GetSecurityCommand getCmd = new GetSecurityCommand();
        
        HedebySecurity sec = getEnv().getCommandService().execute(getCmd).getReturnValue();
        sec.getRole().add(role);
        
        UpdateSecurityCommand cmd = new UpdateSecurityCommand(sec);
        getEnv().getCommandService().execute(cmd);
    }
    
    private void clearRoles() throws GrmException {
        GetSecurityCommand getCmd = new GetSecurityCommand();
        
        HedebySecurity sec = getEnv().getCommandService().execute(getCmd).getReturnValue();

        sec.getRole().clear();

        UpdateSecurityCommand cmd = new UpdateSecurityCommand(sec);
        getEnv().getCommandService().execute(cmd);
        
    }
    
    public void testGetRoles() throws Exception {
        
        String principalClass = "com.sun.grid";
        String principalName  = "blubber";
        String roleName = "admin";
        
        setRoles(roleName, principalClass, principalName);
        
        List<Role> roles = RolesUtil.getRoles(getEnv());
        
        assertEquals("Expected exactly one role", 1, roles.size());
        assertEquals("Expected role name " + roleName, roleName, roles.get(0).getName());
        assertEquals("Expected one principal in the " + roleName + " role", 1, roles.get(0).getPrincipal().size());
        assertEquals("Expected principal class " + principalClass, principalClass, roles.get(0).getPrincipal().get(0).getClassname());
        
        clearRoles();
        RolesUtil.invalidate(getEnv());
        
        roles = RolesUtil.getRoles(getEnv());
        assertEquals("Expected exactly zero roles", 0, roles.size());
    }
    
    /**
     * Test that a second RolesUtil.getRoles call is possible of a first is blocked
     * @throws java.lang.Exception
     */
    public void testCSBlocked() throws Exception {
        final BlockingCommandService cmdService = new BlockingCommandService();
        
        final ExecutionEnv env = new MockupExecutionEnv(cmdService);
        
        final String principalClass = "com.sun.grid";
        final String principalName  = "blubber";
        final String roleName = "admin";
        
        setRoles(roleName, principalClass, principalName);
        
        cmdService.blockNextCommand();
        
        Callable<List<Role>> callable = new Callable<List<Role>>() {
            public List<Role> call() throws Exception {
                return RolesUtil.getRoles(env);
            }
        };
        
        ExecutorService exe = Executors.newSingleThreadExecutor();
        try {
            Future<List<Role>> future = exe.submit(callable);
            cmdService.waitUntilBlocked();
            try {
                List<Role> roles = RolesUtil.getRoles(env);
                assertEquals("Expected exactly one role", 1, roles.size());
                assertEquals("Expected role name " + roleName, roleName, roles.get(0).getName());
                assertEquals("Expected one principal in the " + roleName + " role", 1, roles.get(0).getPrincipal().size());
                assertEquals("Expected principal class " + principalClass, principalClass, roles.get(0).getPrincipal().get(0).getClassname());
            } finally {
                clearRoles();
                RolesUtil.invalidate(env);
                assertFalse("The command must still be blocked", future.isDone());
                cmdService.wakeupBlockedCommand();
                List<Role> roles = future.get();
                assertEquals("Expected exactly zero roles", 0, roles.size());
            }
        } finally {
            exe.shutdownNow();
        }        
    }
    
    private class MockupExecutionEnv implements ExecutionEnv {

        private final CommandService cmdService;
        
        private MockupExecutionEnv(CommandService cmdService) {
            this.cmdService = cmdService;
        }
        
        public String getSystemName() {
            return getEnv().getSystemName();
        }

        public Context getContext() throws GrmException {
            return getEnv().getContext();
        }

        public CommandService getCommandService() {
            return cmdService;
        }

        public JMXServiceURL getCSURL() {
            return getEnv().getCSURL();
        }

        public SecurityContext getSecurityContext() {
            return getEnv().getSecurityContext();
        }

        public Hostname getCSHost() {
            return getEnv().getCSHost();
        }

        public int getCSPort() {
            return getEnv().getCSPort();
        }

        public HostAndPort getCSInfo() {
            return getEnv().getCSInfo();
        }

        public File getDistDir() {
            return getEnv().getDistDir();
        }

        public File getLocalSpoolDir() {
            return getEnv().getLocalSpoolDir();
        }

        public Map<String, ?> getProperties() {
            return getEnv().getProperties();
        }

        public FactoryWithException<ResourceId,ResourceIdException> getResourceIdFactory() {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }
    
    private class BlockingCommandService implements CommandService {

        private final Lock lock = new ReentrantLock();
        private boolean blockNextCommand;
        private Command blockedCommand;
        private final Condition blockedCondition = lock.newCondition();
        private final Condition wakeupCondition = lock.newCondition();
        private volatile boolean nextCommandThrowsException;
        
        public <T> Result<T> execute(Command<T> command) throws GrmException {
            
            lock.lock();
            try {
                if (nextCommandThrowsException) {
                    nextCommandThrowsException = false;
                    throw new GrmException("next command throws exception");
                }
                if (blockNextCommand) {
                    blockedCommand = command;
                    try {
                        blockNextCommand = false;
                        blockedCondition.signalAll();
                        wakeupCondition.await();
                    } finally {
                        blockedCommand = null;
                        blockedCondition.signalAll();
                    }
                }
            } catch (InterruptedException ex) {
                throw new GrmException("interrupted", ex);
            } finally {
                lock.unlock();
            }
            return getEnv().getCommandService().execute(command);
        }

        public <T> Result<T> executeLocally(Command<T> command) throws GrmException {
            return execute(command);
        }
        
        public void blockNextCommand() {
            lock.lock();
            try {
                blockNextCommand = true;
            } finally {
                lock.unlock();
            }
        }
        
        public void wakeupBlockedCommand() {
            lock.lock();
            try {
                wakeupCondition.signalAll();
            } finally {
                lock.unlock();
            }
        }
        
        public void waitUntilBlocked() throws InterruptedException {
            lock.lock();
            try {
                while(blockedCommand == null) {
                    blockedCondition.await();
                }
            } finally {
                lock.unlock();
            }
        }

        public boolean isNextCommandThrowsException() {
            return nextCommandThrowsException;
        }

        public void setNextCommandThrowsException(boolean nextCommandThrowsException) {
            this.nextCommandThrowsException = nextCommandThrowsException;
        }
        
    }
}

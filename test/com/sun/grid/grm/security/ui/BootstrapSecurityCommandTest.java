/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2008 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.security.ui;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.TestUtil;
import com.sun.grid.grm.util.Platform;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import junit.framework.TestCase;

/**
 * Tests the BootstrapSecurityCommand class.
 * 
 * Tests the Exceptions thrown by the constructor of BootstrapSecurityCommand.
 * These test rely on the order in which the error conditions are checked
 * in BootstrapSecurityCommand.
 */
public class BootstrapSecurityCommandTest extends TestCase {

    private final static String BUNDLE = BootstrapSecurityCommand.BUNDLE;
    private final static Logger logger = Logger.getLogger(BootstrapSecurityCommandTest.class.getName());
    private File tmpDir = null;

    public BootstrapSecurityCommandTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        tmpDir = TestUtil.getTmpDir(this);
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        // get rid of all potentially created directories and files
        Platform.getPlatform().removeDir(tmpDir, true);
    }

    /**
     * Tests that BootstrapSecurityCommand.error.sge_root_mandatory exception is
     * thrown by the constructor if no sge_root directory is not given.
     * 
     * @throws java.lang.Exception
     */
    public void testConstructorExNoSgeRoot() throws Exception {
        final Map<String, Object> params = new HashMap<String, Object>();

        // no params set at all
        try {
            final BootstrapSecurityCommand bsc = new BootstrapSecurityCommand(params);
        } catch (GrmException ex) {
            logger.log(Level.INFO, "Caught expected exception:", ex);
            TestUtil.assertMatchingError("No sge_root given", "BootstrapSecurityCommand.error.sge_root_mandatory", BUNDLE, ex);
        }
    }

    /**
     * Tests that BootstrapSecurityCommand.error.sge_root_not_di exception is
     * thrown by the constructor if the given sge_root directory is non existant.
     * 
     * @throws java.lang.Exception
     */
    public void testConstructorExNonExistingSgeRoot() throws Exception {
        final Map<String, Object> params = new HashMap<String, Object>();

        // sge_root is a non existant path (issue 531)
        params.put("SGE_ROOT", "/xyz/foo/bar/baz/true/false");
        try {
            final BootstrapSecurityCommand bsc = new BootstrapSecurityCommand(params);
        } catch (GrmException ex) {
            logger.log(Level.INFO, "Caught expected exception:", ex);
            TestUtil.assertMatchingError("Non-existing sge_root given", "BootstrapSecurityCommand.error.sge_root_not_dir", BUNDLE, ex);
        }
    }

    /**
     * Tests that BootstrapSecurityCommand.error.sge_ca_script_not_found
     * exception is thrown by constructor if the sge_ca script cannot be found.
     * 
     * @throws java.lang.Exception
     */
    public void testConstructorExSgeCaScriptNotFound() throws Exception {
        final Map<String, Object> params = new HashMap<String, Object>();

        // sge_root is an existing path, but there is no sge_ca script
        tmpDir.mkdirs();
        params.put("SGE_ROOT", tmpDir.getAbsolutePath());
        try {
            final BootstrapSecurityCommand bsc = new BootstrapSecurityCommand(params);
        } catch (GrmException ex) {
            logger.log(Level.INFO, "Caught expected exception:", ex);
            TestUtil.assertMatchingError("sge_ca script not found", "BootstrapSecurityCommand.error.sge_ca_script_not_found", BUNDLE, ex);
        }
    }

    /**
     * Tests that no BootstrapSecurityCommand.error.sge_ca_script_not_found
     * exception or no exception at all is thrown by the constructor if the
     * sge_ca script can be found.
     * 
     * @throws java.lang.Exception
     */
    public void testConstructorExSgeCaScriptFound() throws Exception {
        final Map<String, Object> params = new HashMap<String, Object>();

        final File sgeCaDir = new File(tmpDir, "util/sgeCA");
        final File sge_ca = new File(sgeCaDir, "sge_ca");

        // now create the sge_ca file and make sure that no or another exception is thrown
        sgeCaDir.mkdirs();
        sge_ca.createNewFile();
        try {
            final BootstrapSecurityCommand bsc = new BootstrapSecurityCommand(params);
        } catch (Exception ex) {
            logger.log(Level.INFO, "Caught expected exception:", ex);
            TestUtil.assertNotMatchingError("sge_ca script found", "BootstrapSecurityCommand.error.sge_ca_script_not_found", BUNDLE, ex);
        }
    }
}

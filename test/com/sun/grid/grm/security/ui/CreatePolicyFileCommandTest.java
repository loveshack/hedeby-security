/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.security.ui;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.DummyExecutionEnvFactory;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.PathUtil;
import com.sun.grid.grm.util.FileUtil;
import com.sun.grid.grm.util.Platform;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import junit.framework.TestCase;

/**
 *
 */
public class CreatePolicyFileCommandTest extends TestCase {

    private final static Logger log = Logger.getLogger(CreatePolicyFileCommandTest.class.getName());
    private ExecutionEnv env;
    private File policyFile;
    private File securityDir;
    private File policyFileTemplate;

    public CreatePolicyFileCommandTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        env = DummyExecutionEnvFactory.newInstance();
        policyFile = PathUtil.getSecurityJavaPolicy(env);
        securityDir = policyFile.getParentFile();
        if (!securityDir.mkdirs()) {
            throw new Exception("Could not create directory " + securityDir);
        }
        policyFileTemplate = new File(PathUtil.getTemplatePath(env), "java.policy.template");
        if (!policyFileTemplate.getParentFile().exists() && !policyFileTemplate.getParentFile().mkdirs()) {
            throw new Exception("Could not create directory " + policyFileTemplate.getParentFile());
        }
    }

    @Override
    protected void tearDown() throws Exception {
        Platform.getPlatform().removeDir(env.getLocalSpoolDir(), true);
        Platform.getPlatform().removeDir(env.getDistDir(), true);
    }

    public void testUndoFileDeleted() throws Exception {
        String templateContent = "Template content for java policy file";
        FileUtil.write(templateContent, policyFileTemplate);

        CreatePolicyFileCommand cmd = new CreatePolicyFileCommand(System.getProperty("user.name"));
        cmd.execute(env);
        assertTrue("java.policy file must exist", policyFile.exists());
        assertEquals("Content of java.policy must be equals to template", templateContent, FileUtil.read(policyFile));

        cmd.undo(env);
        assertFalse("java.policy file must not exist", policyFile.exists());
    }

    public void testUndoRestoreOldContent() throws Exception {
        String oldContent = "old content for java policy file";
        String templateContent = "Template content for java policy file";

        FileUtil.write(oldContent, policyFile);
        FileUtil.write(templateContent, policyFileTemplate);

        CreatePolicyFileCommand cmd = new CreatePolicyFileCommand(System.getProperty("user.name"));
        cmd.execute(env);
        assertTrue("java.policy file must exist", policyFile.exists());
        assertEquals("Content of java.policy must be equals to template", templateContent, FileUtil.read(policyFile));

        cmd.undo(env);
        assertTrue("java.policy file must still exist", policyFile.exists());
        assertEquals("Content of java.policy file has not been restored", oldContent, FileUtil.read(policyFile));
    }

    public void testTemplateNotExist() throws Exception {
        policyFileTemplate.delete();
        try {
            CreatePolicyFileCommand cmd = new CreatePolicyFileCommand(System.getProperty("user.name"));
            cmd.execute(env);
            fail("if template file does not exist CreatePolicyFileCommand must fail");
        } catch (GrmException ex) {
            log.log(Level.FINE, "Got expected exception", ex);
        }
    }

    public void testPolicyFileNotWritable() throws Exception {

        String oldContent = "old content for java policy file";
        FileUtil.write(oldContent, policyFile);

        Platform.getPlatform().chmod(policyFile, "400");
        try {
            String templateContent = "Template content for java policy file";
            FileUtil.write(templateContent, policyFileTemplate);
            try {
                CreatePolicyFileCommand cmd = new CreatePolicyFileCommand(System.getProperty("user.name"));
                cmd.execute(env);
                fail("if policy file is not writeable CreatePolicyFileCommand must fail");
            } catch (GrmException ex) {
                log.log(Level.FINE, "Got expected exception", ex);
            }
        } finally {
            Platform.getPlatform().chmod(policyFile, "600");
        }
    }
}
